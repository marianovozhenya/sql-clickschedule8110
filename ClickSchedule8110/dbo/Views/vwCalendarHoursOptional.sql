﻿

CREATE VIEW [dbo].[vwCalendarHoursOptional]
AS
SELECT     TOP (100) PERCENT dbo.W6CALENDARS.W6Key AS CalendarKey, 
                      SUM(dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Finish - dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Start) AS Duration, 
                      ROUND((dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Start + 86400) / 86400, 0, 1) AS DOW, dbo.W6CALENDAR_WEEKLY_INTERVALS.Status, 
                      dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Start, dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Finish, 
                      dbo.W6ENGINEERS.W6Key AS EngineerKey
FROM         dbo.W6CALENDARS INNER JOIN
                      dbo.W6CALENDAR_WEEKLY_INTERVALS ON dbo.W6CALENDARS.W6Key = dbo.W6CALENDAR_WEEKLY_INTERVALS.W6Key INNER JOIN
                      dbo.W6ENGINEERS ON dbo.W6CALENDARS.W6Key = dbo.W6ENGINEERS.Calendar
GROUP BY dbo.W6CALENDARS.W6Key, ROUND((dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Start + 86400) / 86400, 0, 1), 
                      dbo.W6CALENDAR_WEEKLY_INTERVALS.Status, dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Start, 
                      dbo.W6CALENDAR_WEEKLY_INTERVALS.Seconds_To_Finish, dbo.W6ENGINEERS.W6Key
HAVING      (dbo.W6CALENDAR_WEEKLY_INTERVALS.Status = 2)
ORDER BY CalendarKey, DOW