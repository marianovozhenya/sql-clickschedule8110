﻿CREATE VIEW dbo.[vwTechnicianTime_201110007]
AS
SELECT     TOP (100) PERCENT dbo.vwScheduledHours.EngineerKey, dbo.vwScheduledHours.Name, dbo.vwScheduledHours.WorkDate, 
                      dbo.vwCalendarHours.Duration AS [Available Duration], dbo.vwCalendarHoursOptional.Duration AS [Optional Duration], 
                      dbo.vwTravelDurationByDay.TravelDayDuration, ISNULL(dbo.vwNAsSameDay.Duration, 0) AS [NA Duration], 
                      dbo.vwScheduledHours.[Total Duration] AS [Work Duration], 
                      dbo.vwCalendarHours.Duration - dbo.vwScheduledHours.[Total Duration] - ISNULL(dbo.vwTravelDurationByDay.TravelDayDuration, 0) 
                      - ISNULL(dbo.vwNAsSameDay.Duration, 0) AS [Open Duration]
FROM         dbo.vwCalendarHours INNER JOIN
                      dbo.vwScheduledHours ON dbo.vwCalendarHours.DOW = dbo.vwScheduledHours.DOW AND 
                      dbo.vwCalendarHours.EngineerKey = dbo.vwScheduledHours.EngineerKey LEFT OUTER JOIN
                      dbo.vwNAsSameDay ON dbo.vwScheduledHours.EngineerKey = dbo.vwNAsSameDay.EngineerKey AND 
                      dbo.vwScheduledHours.WorkDate = dbo.vwNAsSameDay.NAStartDate LEFT OUTER JOIN
                      dbo.vwCalendarHoursOptional ON dbo.vwScheduledHours.DOW = dbo.vwCalendarHoursOptional.DOW AND 
                      dbo.vwScheduledHours.EngineerKey = dbo.vwCalendarHoursOptional.EngineerKey LEFT OUTER JOIN
                      dbo.vwTravelDurationByDay ON dbo.vwScheduledHours.EngineerKey = dbo.vwTravelDurationByDay.EngineerKey AND 
                      dbo.vwScheduledHours.WorkDate = dbo.vwTravelDurationByDay.WorkDay
ORDER BY dbo.vwScheduledHours.EngineerKey, dbo.vwScheduledHours.WorkDate
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTechnicianTime_201110007';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTechnicianTime_201110007';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwCalendarHours"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwCalendarHoursOptional"
            Begin Extent = 
               Top = 114
               Left = 634
               Bottom = 222
               Right = 805
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwTravelDurationByDay"
            Begin Extent = 
               Top = 6
               Left = 645
               Bottom = 99
               Right = 815
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwNAsSameDay"
            Begin Extent = 
               Top = 251
               Left = 520
               Bottom = 359
               Right = 671
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "vwScheduledHours"
            Begin Extent = 
               Top = 6
               Left = 247
               Bottom = 114
               Right = 398
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTechnicianTime_201110007';

