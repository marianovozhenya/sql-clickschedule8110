﻿CREATE VIEW dbo.[vwJeopardyTickets]
AS
SELECT     TOP (100) PERCENT R.Name AS Region, D.Name AS District, t.CallID AS TicketNumber, TT.Name AS DispatchCode, A.StartTime, A.FinishTime, A.AssignedEngineers, 
                      TS.Name AS TicketStatus, JS.Name AS JeopardyState
FROM         dbo.W6TASKS AS t WITH (nolock) INNER JOIN
                      dbo.W6ASSIGNMENTS AS A WITH (nolock) ON t.W6Key = A.Task INNER JOIN
                      dbo.W6JEOPARDY_STATE AS JS WITH (nolock) ON t.JeopardyState = JS.W6Key INNER JOIN
                      dbo.W6REGIONS AS R WITH (nolock) ON t.Region = R.W6Key INNER JOIN
                      dbo.W6DISTRICTS AS D WITH (nolock) ON t.District = D.W6Key INNER JOIN
                      dbo.W6TASK_STATUSES AS TS WITH (nolock) ON t.Status = TS.W6Key INNER JOIN
                      dbo.W6TASK_TYPES AS TT WITH (nolock) ON t.TaskType = TT.W6Key
WHERE     (JS.Name IN ('Late Start Travel', 'Late Start Work'))
ORDER BY A.StartTime
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwJeopardyTickets';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwJeopardyTickets';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "A"
            Begin Extent = 
               Top = 0
               Left = 256
               Bottom = 119
               Right = 439
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JS"
            Begin Extent = 
               Top = 227
               Left = 390
               Bottom = 331
               Right = 550
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "R"
            Begin Extent = 
               Top = 6
               Left = 723
               Bottom = 110
               Right = 883
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "D"
            Begin Extent = 
               Top = 6
               Left = 921
               Bottom = 125
               Right = 1127
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TS"
            Begin Extent = 
               Top = 114
               Left = 525
               Bottom = 233
               Right = 685
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TT"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwJeopardyTickets';

