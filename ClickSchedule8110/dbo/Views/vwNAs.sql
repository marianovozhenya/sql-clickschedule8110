﻿
CREATE VIEW [dbo].[vwNAs]
AS
SELECT     EngineerKey, Name, CONVERT(VARCHAR, StartTime, 101) AS NAStartDate, CONVERT(VARCHAR, FinishTime, 101) AS NAFinishDate, CommentText, [NA Reason], 
                      Duration
FROM         dbo.vwEngineerAssignments
WHERE     (CallID IS NULL)