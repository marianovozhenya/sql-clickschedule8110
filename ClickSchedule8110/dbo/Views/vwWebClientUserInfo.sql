﻿CREATE VIEW dbo.vwWebClientUserInfo
AS
SELECT        TOP (100) PERCENT REPLACE(Owner, 'APEX\', '') AS 'UserName', 
                         CASE WHEN ParentUserSetting = 790759430 THEN 'APEX\ReadOnly' WHEN ParentUserSetting = 790773763 THEN 'APEX\DispatchetLevel1' WHEN ParentUserSetting = 790773766 THEN 'APEX\ServiceRegionals'
                          WHEN ParentUserSetting = 790784000 THEN 'APEX\DispatchetLevel3' WHEN ParentUserSetting = 790784002 THEN 'APEX\DispatchetLevel2' WHEN ParentUserSetting = 790849539 THEN 'APEX\SpecialOps' WHEN
                          ParentUserSetting = 790857751 THEN 'APEX\FieldServiceAnalysts' WHEN ParentUserSetting = 1059215383 THEN 'APEX\SolarReadOnly' WHEN ParentUserSetting = 1059233820 THEN 'APEX\SolarServiceSupport'
                          WHEN ParentUserSetting = 1059252224 THEN 'APEX\SolarSupervisor' WHEN ParentUserSetting = 1059264515 THEN 'APEX\NISImportant' WHEN parentusersetting = 119732242 THEN 'SpecialOps' WHEN Parentusersetting
                          = 119732230 THEN 'DispatcherLevel1' WHEN ParentUserSetting = 119732234 THEN 'DispatcherLevel3' WHEN ParentUserSetting = 119732232 THEN 'DispatcherLevel2' WHEN ParentUserSetting = 119732237 THEN
                          'NISAdmin' WHEN ParentUserSetting = 119732235 THEN 'FieldServiceAnalysts' WHEN ParentUserSetting = 119740531 THEN 'SolarReadOnly' WHEN ParentUserSetting = 119732238 THEN 'ReadOnly' WHEN ParentUserSetting
                          = 119732239 THEN 'ServiceRegionals' WHEN ParentUserSetting = 119758856 THEN 'SolarSpecial' WHEN ParentUserSetting = 119732236 THEN 'NISScheduling' WHEN ParentUserSetting = 119732225 THEN 'Master Beast'
                          WHEN ParentUserSetting = 119758858 THEN 'SpecialOps' WHEN ParentUserSetting = 119758858 THEN 'SpecialOps' WHEN ParentUserSetting = 119711747 THEN 'Master Beast' WHEN ParentUserSetting = 119715840
                          THEN 'Diabsolut\Admin' WHEN ParentUserSetting = 119769092 THEN 'SSPro' WHEN ParentUserSetting = 253888518 THEN 'Summer 2014' WHEN ParentUserSetting = 253933572 THEN '365 Template' WHEN ParentUserSetting
                          = 253937672 THEN '365 - Read Only' WHEN ParentUserSetting = 253947916 THEN 'Beta Scheduling Leads' WHEN ParentUserSetting = 253947917 THEN 'Beta Scheduling Professionals' WHEN ParentUserSetting
                          = 253952000 THEN 'Beta Solar IA' WHEN ParentUserSetting = 253947954 THEN 'Beta Solar OM' WHEN ParentUserSetting = 253964288 THEN 'SpecialOpsSupreme' END AS 'Template'
FROM            dbo.W6USER_SETTINGS WITH (nolock)
WHERE        (Category = 'ClickSchedule Web Client') AND (Owner NOT IN ('[WOMM Template]', '[Workspace Template]', '[VacationManagementEmployee]', 'Diabsoult\Admin', 'MapTest', 'APEX\SpecialOps', 'Master Beast', 
                         'AssistantDirectors', 'DispatcherLevel1', 'DispatcherLevel2', 'DispatcherLevel3', 'FieldServiceAnalysts', 'NISScheduling', 'NISAdmin', 'ReadOnly', 'ServiceRegionals', 'SpecialOps', 'SolarSupervisor', 
                         'SolarReadOnly', 'Copy of DispatcherLevel1', 'Copy of SpecialOps', 'Domain\AdministratorSettings.IMRSDemo', 'ProductTemplate\Reviewer', 'ProductTemplate\User', 'ProductTemplate\Administrator', 
                         'APEX\SpecialOpsTEST', 'ManualCopyServiceRegionals', 'SolarSpecial', 'SSPro', 'Summer 2014', '365', '365 - Read Only', 'Beta Scheduling Leads', 'Beta Scheduling Professionals', 
                         'Copy of Beta Scheduling Professionals', 'Beta Solar OM', 'Beta Solar IA', 'SpecialOpsSupreme'))
ORDER BY 'Template'
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwWebClientUserInfo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[12] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "W6USER_SETTINGS"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 222
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1230
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwWebClientUserInfo';

