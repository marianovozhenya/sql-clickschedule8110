﻿
CREATE VIEW [dbo].[vwEngineerAssignments]
AS
SELECT     dbo.W6ASSIGNMENTS_ENGINEERS.EngineerKey, dbo.W6ENGINEERS.Name, dbo.W6ASSIGNMENTS.StartTime, dbo.W6ASSIGNMENTS.FinishTime, 
                      dbo.W6ASSIGNMENTS.CommentText, dbo.W6NONAVAILABILITY_TYPES.Name AS [NA Reason], DATEPART(weekday, dbo.W6ASSIGNMENTS.StartTime) AS DOW, 
                      DATEDIFF(second, dbo.W6ASSIGNMENTS.StartTime, dbo.W6ASSIGNMENTS.FinishTime) AS Duration, dbo.W6TASKS.CallID, 
                      dbo.W6RP_DS_TRAVEL.DefaultTravelTime, dbo.W6RP_DS_TRAVEL.DefaultTravelDistance, dbo.W6ASSIGNMENTS.W6Key AS AssignmentKey, 
                      dbo.W6RP_DS_TRAVEL.TravelType
FROM         dbo.W6ASSIGNMENTS with (nolock)
INNER JOIN   dbo.W6ASSIGNMENTS_ENGINEERS with (nolock) ON dbo.W6ASSIGNMENTS.W6Key = dbo.W6ASSIGNMENTS_ENGINEERS.W6Key 
INNER JOIN   dbo.W6ENGINEERS with (nolock) ON dbo.W6ASSIGNMENTS_ENGINEERS.EngineerKey = dbo.W6ENGINEERS.W6Key 
LEFT OUTER JOIN  dbo.W6RP_DS_TRAVEL with (nolock) ON dbo.W6ASSIGNMENTS_ENGINEERS.W6Key = dbo.W6RP_DS_TRAVEL.AssignementKey 
			AND  dbo.W6ASSIGNMENTS_ENGINEERS.EngineerKey = dbo.W6RP_DS_TRAVEL.EngineerKey 
LEFT OUTER JOIN  dbo.W6TASKS with (nolock) ON dbo.W6ASSIGNMENTS.Task = dbo.W6TASKS.W6Key 
LEFT OUTER JOIN  dbo.W6NONAVAILABILITY_TYPES with (nolock) ON dbo.W6ASSIGNMENTS.NonAvailabilityType = dbo.W6NONAVAILABILITY_TYPES.W6Key
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwEngineerAssignments';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'     Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4425
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwEngineerAssignments';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "W6ASSIGNMENTS"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "W6ASSIGNMENTS_ENGINEERS"
            Begin Extent = 
               Top = 6
               Left = 250
               Bottom = 114
               Right = 480
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "W6ENGINEERS"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "W6RP_DS_TRAVEL"
            Begin Extent = 
               Top = 6
               Left = 518
               Bottom = 136
               Right = 704
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "W6TASKS"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "W6NONAVAILABILITY_TYPES"
            Begin Extent = 
               Top = 114
               Left = 280
               Bottom = 222
               Right = 564
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
    ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwEngineerAssignments';

