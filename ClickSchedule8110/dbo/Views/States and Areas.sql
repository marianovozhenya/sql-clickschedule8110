﻿CREATE VIEW dbo.[States and Areas]
AS
SELECT        dbo.W6DISTRICTS.Name AS District, CASE WHEN dbo.W6DISTRICTS.Name IN ('Huntsville, AL', 'Birmingham, AL', 'Enterprise, AL', 'Mobile, AL', 'Montgomery, AL') 
                         THEN 'Alabama' WHEN dbo.W6DISTRICTS.Name IN ('Kenai, AK', 'Anchorage, AK', 'Fairbanks, AK', 'Juneau, AK') 
                         THEN 'Alaska' WHEN dbo.W6DISTRICTS.Name IN ('Kingman, AZ', 'Tucson, AZ', 'Phoenix, AZ', 'Yuma, AZ') 
                         THEN 'Arizona' WHEN dbo.W6DISTRICTS.Name IN ('Fayetteville, AR', 'Jonesboro, AR', 'Little Rock, AR', 'Texarkana, AR') 
                         THEN 'Arkansas' WHEN dbo.W6DISTRICTS.Name IN ('Eureka, CA', 'Northern California', 'Redding, CA', 'Central California', 'Southern California') 
                         THEN 'California' WHEN dbo.W6DISTRICTS.Name IN ('Colorado Springs, CO', 'Denver, CO', 'Grand Junction, CO') 
                         THEN 'Colorado' WHEN dbo.W6DISTRICTS.Name = 'Hartford, CT' THEN 'Connecticut' WHEN dbo.W6DISTRICTS.Name = 'Dover, DE' THEN 'Delaware' WHEN dbo.W6DISTRICTS.Name
                          IN ('Pensacola, FL', 'Fort Myers, FL', 'Jacksonville, FL', 'Miami, FL', 'Orlando, FL', 'Tallahassee, FL', 'Tampa, FL') 
                         THEN 'Florida' WHEN dbo.W6DISTRICTS.Name IN ('Albany, GA', 'Atlanta, GA', 'Augusta, GA', 'Macon, GA', 'Savannah, GA') 
                         THEN 'Georgia' WHEN dbo.W6DISTRICTS.Name IN ('Maui, HI', 'Oahu, HI', 'Big Island, HI', 'Kauai, HI') 
                         THEN 'Hawaii' WHEN dbo.W6DISTRICTS.Name = 'Southern Idaho' THEN 'Idaho' WHEN dbo.W6DISTRICTS.Name IN ('Chicago, IL', 'Peoria, IL', 'Springfield, IL', 
                         'Carbondale, IL') THEN 'Illinois' WHEN dbo.W6DISTRICTS.Name IN ('Evansville, IN', 'Fort Wayne, IN', 'Gary, IN', 'Indianapolis, IN') 
                         THEN 'Indiana' WHEN dbo.W6DISTRICTS.Name IN ('Des Moines, IA', 'Waterloo, IA', 'Quad Cities, IA') 
                         THEN 'Iowa' WHEN dbo.W6DISTRICTS.Name IN ('Dodge City, KS', 'Topeka, KS', 'Wichita, KS') THEN 'Kansas' WHEN dbo.W6DISTRICTS.Name IN ('Lexington, KY', 
                         'Louisville, KY', 'Owensboro, KY') THEN 'Kentucky' WHEN dbo.W6DISTRICTS.Name IN ('Baton Rouge, LA', 'Shreveport, LA') 
                         THEN 'Louisiana' WHEN dbo.W6DISTRICTS.Name = 'Baltimore, MD' THEN 'Maryland' WHEN dbo.W6DISTRICTS.Name IN ('Boston, MA', 'New England North') 
                         THEN 'Massachusetts' WHEN dbo.W6DISTRICTS.Name IN ('Detroit, MI', 'Flint, MI', 'Grand Rapids, MI') 
                         THEN 'Michigan' WHEN dbo.W6DISTRICTS.Name IN ('Minneapolis South, MN', 'Duluth, MN', 'Minneapolis Metro, MN') 
                         THEN 'Minnesota' WHEN dbo.W6DISTRICTS.Name IN ('Gulfport, MS', 'Jackson, MS', 'North Mississippi') 
                         THEN 'Mississippi' WHEN dbo.W6DISTRICTS.Name IN ('Cape Girardeau, MO', 'Columbia, MO', 'Kansas City, MO', 'Springfield, MO', 'St. Louis, MO') 
                         THEN 'Missouri' WHEN dbo.W6DISTRICTS.Name IN ('Sidney, MT', 'Helena, MT', 'Billings, MT') THEN 'Montana' WHEN dbo.W6DISTRICTS.Name IN ('Scottsbluff, NE', 
                         'Grand Island, NE', 'Omaha, NE') THEN 'Nebraska' WHEN dbo.W6DISTRICTS.Name IN ('Reno, NV', 'Wendover Pacific, NV', 'Las Vegas, NV') 
                         THEN 'Nevada' WHEN dbo.W6DISTRICTS.Name IN ('North New Jersey', 'South New Jersey') 
                         THEN 'New Jersey' WHEN dbo.W6DISTRICTS.Name IN ('Albuquerque, NM', 'Clovis, NM', 'Las Cruces, NM', 'Roswell, NM') 
                         THEN 'New Mexico' WHEN dbo.W6DISTRICTS.Name IN ('Staten Island, NY', 'Albany, NY', 'Buffalo, NY', 'Long Island, NY', 'New York, NY') 
                         THEN 'New York' WHEN dbo.W6DISTRICTS.Name IN ('Charlotte, NC', 'Fayetteville, NC', 'Greensboro, NC', 'Raleigh, NC') 
                         THEN 'North Carolina' WHEN dbo.W6DISTRICTS.Name IN ('Dickinson, ND', 'Williston, ND', 'Bismarck, ND', 'Fargo, ND') 
                         THEN 'North Dakota' WHEN dbo.W6DISTRICTS.Name IN ('Cincinnati, OH', 'Cleveland, OH', 'Columbus, OH', 'Toledo, OH') 
                         THEN 'Ohio' WHEN dbo.W6DISTRICTS.Name IN ('Guymon, OK', 'Lawton, OK', 'Oklahoma City, OK', 'Tulsa, OK') 
                         THEN 'Oklahoma' WHEN dbo.W6DISTRICTS.Name IN ('Portland, OR', 'Eastern Oregon', 'Southern Oregon') 
                         THEN 'Oregon' WHEN dbo.W6DISTRICTS.Name IN ('Philadelphia, PA', 'Scranton, PA', 'Erie, PA', 'Pittsburgh, PA') 
                         THEN 'Pennsylvania' WHEN dbo.W6DISTRICTS.Name = 'Rhode Island' THEN 'RhodeIsland' WHEN dbo.W6DISTRICTS.Name IN ('Charleston, SC', 'Columbia, SC', 
                         'Greenville, SC') THEN 'South Carolina' WHEN dbo.W6DISTRICTS.Name IN ('Sioux Falls, SD', 'Rapid City, SD') 
                         THEN 'South Dakota' WHEN dbo.W6DISTRICTS.Name IN ('Chattanooga, TN', 'Knoxville, TN', 'Memphis, TN', 'Nashville, TN') 
                         THEN 'Tennessee' WHEN dbo.W6DISTRICTS.Name IN ('DFW, TX', 'Austin, TX', 'Eagle Pass, TX', 'Laredo, TX', 'Odessa, TX', 'El Paso, TX', 'Amarillo, TX', 'Lufkin, TX', 
                         'San Antonio, TX', 'Tyler, TX', 'Lubbock, TX', 'Waco, TX', 'Wichita Falls, TX', 'Beaumont, TX', 'Corpus Christi, TX', 'Houston West, TX', 'Houston, TX', 'McAllen, TX', 
                         'Abilene, TX') THEN 'Texas' WHEN dbo.W6DISTRICTS.Name IN ('Central Utah', 'Salt Lake, UT', 'St. George, UT') 
                         THEN 'Utah' WHEN dbo.W6DISTRICTS.Name IN ('Fairfax, VA', 'Richmond, VA', 'Roanoke, VA') THEN 'Virginia' WHEN dbo.W6DISTRICTS.Name IN ('Seattle, WA', 
                         'Eastern Washington', 'Spokane, WA', 'Vancouver, WA') 
                         THEN 'Washington' WHEN dbo.W6DISTRICTS.Name = 'Washington DC' THEN 'Washington DC' WHEN dbo.W6DISTRICTS.Name IN ('Morgantown, WV', 
                         'Martinsburg, WV', 'Charleston, WV') THEN 'West Virginia' WHEN dbo.W6DISTRICTS.Name IN ('Eau Claire, WI', 'Green Bay, WI', 'Milwaukee, WI') 
                         THEN 'Wisconsin' WHEN dbo.W6DISTRICTS.Name IN ('Casper, WY', 'Cheyenne, WY', 'Cody, WY', 'Evanston, WY', 'Gillette, WY') 
                         THEN 'Wyoming' WHEN dbo.W6DISTRICTS.Name IN ('Medicine Hat', 'Elk Point', 'Lethbridge', 'Red Deer', 'Edmonton', 'Fort McMurray', 'Calgary', 'Grand Prairie') 
                         THEN 'Alberta' WHEN dbo.W6DISTRICTS.Name = 'Winnipeg' THEN 'Manitoba' WHEN dbo.W6DISTRICTS.Name IN ('Saskatoon', 'Regina') 
                         THEN 'Saskatchewan' WHEN dbo.W6DISTRICTS.Name IN ('Sydney', 'Halifax') 
                         THEN 'Nova Scotia' WHEN dbo.W6DISTRICTS.Name = 'Yellowknife' THEN 'Northwest Territories' WHEN dbo.W6DISTRICTS.Name IN ('North Bay, ON', 'Ottawa', 
                         'Thunder Bay', 'Toronto') 
                         THEN 'Ontario' WHEN dbo.W6DISTRICTS.Name = 'Newfoundland' THEN 'Newfoundland and Labrador' WHEN dbo.W6DISTRICTS.Name IN ('Cranbrook', 
                         'Prince George', 'Fort St. John', 'Terrace', 'Vancouver', 'Victoria', 'Kelowna', 'Kamloops') 
                         THEN 'British Columbia' WHEN dbo.W6DISTRICTS.Name = 'New Brunswick' THEN 'New Brunswick' WHEN dbo.W6DISTRICTS.Name = 'Prince Edward Island' THEN 'Prince Edward Island'
                          WHEN dbo.W6DISTRICTS.Name = 'Whitehorse' THEN 'Yukon' WHEN dbo.W6DISTRICTS.Name = 'Auckland, NZ' THEN 'Auckland' END AS State, 
                         dbo.W6REGIONS.Name AS Region, CASE WHEN dbo.W6REGIONS.Name IN ('Central East', 'Central North East', 'Central North West', 'Central South', 'Central West', 
                         'Western Texas South') THEN 'Central Area' WHEN dbo.W6REGIONS.Name IN ('Canada East', 'Canada West') 
                         THEN 'Canada Area' WHEN dbo.W6REGIONS.Name IN ('Western Mountain', 'Western North West', 'Western South West', 'Western Texas North', 
                         'Western Tex-Mex') THEN 'Western Area' WHEN dbo.W6REGIONS.Name IN ('Eastern East', 'Eastern North East', 'Eastern North West', 'Eastern North', 
                         'Eastern South East', 'Eastern South') THEN 'Eastern Area' END AS Area, dbo.W6DISTRICTS.W6Key AS DistrictKey, dbo.W6REGIONS.W6Key AS RegionKey, 
                         CASE WHEN dbo.W6REGIONS.Name IN ('Canada East', 'Canada West', 'Western Canada') 
                         THEN 'Canada' WHEN dbo.W6REGIONS.Name = 'New Zealand' THEN 'New Zealand' ELSE 'US' END AS Country
FROM            dbo.W6DISTRICTS INNER JOIN
                         dbo.W6REGIONS ON dbo.W6DISTRICTS.RegionParent = dbo.W6REGIONS.W6Key
WHERE        (dbo.W6DISTRICTS.Name NOT LIKE '%Solar%') AND (dbo.W6DISTRICTS.Name <> 'Inactive Techs') AND (dbo.W6REGIONS.Name <> 'SolarUSA')
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'States and Areas';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[9] 2[32] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "W6DISTRICTS"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "W6REGIONS"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 110
               Right = 442
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'States and Areas';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'State and Area', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'States and Areas';

