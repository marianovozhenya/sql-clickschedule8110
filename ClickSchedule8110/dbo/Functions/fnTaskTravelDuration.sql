﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnTaskTravelDuration] 
(
	-- Add the parameters for the function here
	@TaskKey int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar int
	DECLARE @TravelStatus DATETIME
	DECLARE @OnsiteStatus DATETIME
	
	
	SELECT @OnsiteStatus = W6RP_DS_STATUS_CHANGES.UpdateDate 
	FROM W6RP_DS_STATUS_CHANGES INNER JOIN
                      W6TASK_STATUSES ON W6RP_DS_STATUS_CHANGES.Status = W6TASK_STATUSES.W6Key
	WHERE     (W6TASK_STATUSES.Name = N'On-site')

	Select @TravelStatus = W6RP_DS_STATUS_CHANGES.UpdateDate 
	FROM W6RP_DS_STATUS_CHANGES INNER JOIN
                      W6TASK_STATUSES ON W6RP_DS_STATUS_CHANGES.Status = W6TASK_STATUSES.W6Key
	WHERE     (W6TASK_STATUSES.Name = N'Travel')

	-- Return the result of the function
	RETURN DATEDIFF(s,@TravelStatus,@OnsiteStatus)

END