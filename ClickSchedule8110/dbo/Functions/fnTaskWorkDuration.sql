﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnTaskWorkDuration] 
(
	-- Add the parameters for the function here
	@TaskKey int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar int
	DECLARE @WorkStatus DATETIME
	DECLARE @OnsiteStatus DATETIME
	
	
	SELECT @OnsiteStatus = W6RP_DS_STATUS_CHANGES.UpdateDate 
	FROM W6RP_DS_STATUS_CHANGES INNER JOIN
                      W6TASK_STATUSES ON W6RP_DS_STATUS_CHANGES.Status = W6TASK_STATUSES.W6Key
	WHERE     (W6TASK_STATUSES.Name = N'On-site')

	Select @WorkStatus = W6RP_DS_STATUS_CHANGES.UpdateDate 
	FROM W6RP_DS_STATUS_CHANGES INNER JOIN
                      W6TASK_STATUSES ON W6RP_DS_STATUS_CHANGES.Status = W6TASK_STATUSES.W6Key
	WHERE     (W6TASK_STATUSES.Name = N'Complete')

	-- Return the result of the function
	RETURN DATEDIFF(s,@WorkStatus,@OnsiteStatus)

END