﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[jsonDeleteRequiredEngineers]




AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
MESSAGEIN,
MESSAGEOUT,
MessageIn_Time 
into #tmp
from W6IM_INCOMING_LOG (nolock) 
where MESSAGEIN like '%"RequiredEngineers"%'

delete from W6IM_INCOMING_LOG 
from W6IM_INCOMING_LOG loggy (nolock)
inner join #tmp tmp on tmp.MessageIn_Time = loggy.MessageIn_Time
where loggy.MESSAGEIN like '%"RequiredEngineers"%'



END