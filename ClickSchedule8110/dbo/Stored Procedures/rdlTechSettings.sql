﻿
 /******************************************************************************
**		File: rdlTechSettings
**		Name: 
**		Desc: Shows information concerning individual Techs
**			such as their region, district, Working Districts, Max Distance from
**			home base
**
**		Return values:
** 
**		Called by:  Click Reports
**              
**		Parameters:
**		Input							Output
**     ----------						-----------
**		
**
**		Auth: Collin Peterson
**		Date: 12/08/09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:			Description:
**		--------	--------		-------------------------------------------
**		
*******************************************************************************/

CREATE Procedure [dbo].[rdlTechSettings]
	@Region nvarchar(64)
AS

Select     
	W6Regions.Name AS Region, 
	W6Engineers.Name, 
	W6Engineers.ID, 
	W6Districts.Name AS HomeDistrict, 
	W6Calendars.Name AS CalendarName, 
	case W6Engineers.Active
		when -1 Then 'TRUE'
		else 'FALSE'
	end as ActiveTech,
	case W6Engineers.Internal	
		when -1 Then 'TRUE'
		else 'FALSE'
	end as ServiceTech, 
	case W6Engineers.MobileClient 
		when -1 Then 'TRUE'
		else 'FALSE'
	end as MobileClient,
	W6Engineers.MobileClientSettings, 
	W6Engineers.LoginName, 
	W6Engineers.Street AS Address, 
	W6Engineers.City, 
	W6Engineers.State, 
	W6Engineers.PostCode, 
	W6Countries.Name AS CountryName, 
	W6Engineers.MaxDistanceFromHB AS MaxDistanceFromHomeBase, 
	W6Engineers.HomePhone, 
	W6Engineers.MobilePhone,
	WorkingDistricts = dbo.fnWorkingDistrictsForEngineer(W6ENGINEERS.ID)
From W6Engineers with (nolock)
Inner join W6Countries with (nolock)
	on W6Countries.W6Key = W6Engineers.CountryID
Inner join W6REGIONS with (nolock)
	on W6Regions.W6Key = W6Engineers.Region 
Inner join W6Districts with (nolock)
	on W6Districts.W6Key = W6Engineers.District
Left outer join W6Calendars with (nolock)
	on W6Calendars.W6Key = W6Engineers.Calendar
Where (@Region = 'All' or @Region = W6REGIONS.Name)
Order by W6Regions.Name, W6Engineers.Name