﻿
CREATE proc spDefrag10 
as	
declare @Tables table (
pk int identity(1,1),
FK int,
TableName sysname,
IndexName sysname, 
IndexID int)

declare @latest date
select @latest = cast(MAX(CollectionDate) as DATE) from maintenance.dbo.DefragLevels


insert into @Tables (FK, TableName, IndexName, IndexID)
select top 10 PK, tablename, indexname, d.IndexID--, cast(AVG(avg_fragmentation_in_percent) as decimal(10,1)) pct 
from maintenance..DefragLevels d
inner join sys.indexes i on d.IndexID = i.index_id and OBJECT_NAME(i.object_id) = d.TableName 
where IndexID > 0
and Alloc_unit_type_desc = 'IN_ROW_DATA'
and i.allow_page_locks = 1
--and (DATEDIFF(dd,isnull(datelastdefragged,getdate()),getdate()) <=2 or DATEDIFF(dd,isnull(datelastdefragged,getdate()),getdate()) >=5) --only defrag if it's been defragged in the last two days, or if it's been longer than 5
and CollectionDate >= @latest
group by DBName, TableName, IndexName, IndexID, PK
order by cast(AVG(avg_fragmentation_in_percent) as decimal(10,1)) desc


Declare @i int
Declare @tname sysname, @iname sysname
Declare @iID int, @FK int

select @i = 1
while @i <= (select MAX(pk) from @Tables)
begin
	select @tname = tablename, @iname = indexname, @iID = indexid, @FK = FK from @Tables where pk = @i
	if @iID =1
	begin
		--exec ('alter index '+@iname+' on ['+@tname+'] rebuild with (online = on, sort_in_tempdb = on)')
		print 'alter index ['+@iname+'] on ['+@tname+'] rebuild with (online = on, sort_in_tempdb = on)' + ' ' + cast(@iid as varchar(100))
		begin try
			exec ('alter index ['+@iname+'] on ['+@tname+'] rebuild with (online = on, sort_in_tempdb = on)')
		end try
		begin catch
			exec ('alter index ['+@iname+'] on ['+@tname+'] rebuild with (sort_in_tempdb = on)')
		end catch
	end
	else
	if @iID >1
	begin	
		print 'alter index ['+@iname+'] on ['+@tname+'] reorganize ' + ' ' + cast(@iid as varchar(100))
		exec ('alter index ['+@iname+'] on ['+@tname+'] reorganize ')
	end

update Maintenance..DefragLevels set DateLastDefragged = GETDATE(), DefraggedCount = isnull(DefraggedCount,0) + 1
where TableName = @tname and IndexName = @iname and PK = @FK

insert into Maintenance..DefragLevels_Log (CollectionDate, DBName, TableName, IndexID, IndexName, Avg_Fragmentation_in_Percent, Alloc_unit_type_desc, DateLastDefragged, DefraggedCount)
select CollectionDate, DBName, TableName, IndexID, IndexName, Avg_Fragmentation_in_Percent, Alloc_unit_type_desc, DateLastDefragged, DefraggedCount from Maintenance..DefragLevels
where PK = @FK


select @i = @i + 1
end