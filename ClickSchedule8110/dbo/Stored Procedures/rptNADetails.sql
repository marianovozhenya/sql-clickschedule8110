﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================



CREATE PROCEDURE [dbo].[rptNADetails]




AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


select distinct
r.Name as Region,
d.Name as District,
e.Name as Tech,
e.ID as EmployeeID,
a.W6Key as AssignmentKey,
a.CreatedBy,
a.TimeCreated as DateCreated,
a.StartTime as 'NA Start',
a.FinishTime as 'NA End',
cast( DateDiff( mi, a.StartTime, a.FinishTime) / 60   as varchar(20) )+ ' h ' +  cast (DateDiff( mi, a.StartTime, a.FinishTime) % 60   as varchar(20) ) + ' m '   as 'NA Duration',
na.Name as NonAvailabilityType,
a.CommentText,
a.Latitude,
a.Longitude,
Case When DateDiff( hour, a.StartTime, a.FinishTime) > 40 Then 'More than 40 hours' Else '' End as MoreThan40Hours,
a.Street,
a.City,
a.State,
a.PostCode

from W6ASSIGNMENTS a (nolocK)
inner join W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
inner join W6REGIONS r (nolock) on e.Region = r.W6Key
inner join W6DISTRICTS d (nolock) on e.District = d.W6Key
left outer join W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key

Where na.Name <> '2 Week Notice'

END