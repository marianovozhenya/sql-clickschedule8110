﻿
 /******************************************************************************
**		File: rdlCurrentlyAssignedTickets 
**		Name: 
**		Desc: Shows which tickets are currently assigned and there Status
**			and related appointment information.
**
**		Return values:
** 
**		Called by:  Click Reports
**              
**		Parameters:
**		Input							Output
**     ----------						-----------
**		
**
**		Auth: Collin Peterson
**		Date: 12/08/09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:			Description:
**		--------	--------		-------------------------------------------
**		
*******************************************************************************/

CREATE Procedure [dbo].[rdlCurrentlyAssignedTickets] 
	@Region nvarchar(64),
	@District nvarchar(64)
AS

select 
                W6REGIONS.Name AS Region, 
                W6DISTRICTS.Name AS District, 
                W6ASSIGNMENTS.AssignedEngineers AS TechAssigned, 
                W6TASKS.AppointmentStart, 
                W6TASKS.AppointmentFinish, 
                W6TASKS.CallID AS [Ticket #], 
                W6TASK_STATUSES.Name AS TaskStatus, 
                W6TASK_TYPES.Name AS TaskType, 
                W6ASSIGNMENTS.StartTime AS WorkStart, 
                W6ASSIGNMENTS.FinishTime AS WorkFinish, 
                W6TASKS.Street, 
                W6TASKS.City, 
                W6TASKS.State, 
                W6TASKS.PostCode, 
                W6TASKS.CustomerEmail, 
                W6JEOPARDY_STATE.Name AS JeopardyState, 
                W6TASKS.Pinned, 
                W6TASKS.DispatcherComment, 
                W6TASKS.NotifyLongTerm, 
                W6TASKS.NotifyMidTerm, 
                W6TASKS.NotifyShortTerm, 
                W6TASKS.NotifySMS, 
                W6TASKS.NotifyEMail, 
                W6TASKS.NotifyIVR, 
                W6TASKS.TimeCreated

from W6Tasks with (nolock)
Inner join W6regions with (nolock)
	on W6Regions.W6Key = W6Tasks.Region
Inner join W6Districts with (nolock)
	on W6Districts.W6Key = W6Tasks.District
Inner join W6Task_Statuses with (nolock)
	on W6Task_Statuses.W6Key = W6Tasks.[Status]
Inner join W6Task_Types with (nolock)
	on W6Tasks.TaskType = W6Task_types.W6Key
Left outer join W6Assignments with (nolock)
	on W6Tasks.W6Key = W6Assignments.Task  
Left outer join W6Jeopardy_State with (nolock)
	on W6Jeopardy_State.W6Key = W6Tasks.JeopardyState
Where (@Region = 'All' or @Region = W6REGIONS.Name) AND 
		(@District = 'All' or @District = W6DISTRICTS.Name) AND
		W6Task_Statuses.Name IN (N'Acknowledged', N'Dispatched', N'On-Site', N'Scheduled', N'Tentative', N'Travel')
Order by W6Regions.Name, W6Districts.Name, W6Assignments.AssignedEngineers, W6Tasks.AppointmentStart