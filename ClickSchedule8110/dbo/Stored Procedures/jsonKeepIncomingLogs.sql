﻿



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [dbo].[jsonKeepIncomingLogs]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select 
MESSAGEIN,
MESSAGEOUT,
MessageIn_Time

into #tmp2
from W6IM_INCOMING_LOG (nolock) 
where 
(
	MESSAGEIN not like '%GetAvailableAppointments%'
	and 
	MESSAGEIN not like '%CreateOrUpdateTask%'
	and 
	MESSAGEOUT not like '%Dispatch Error%'
	and
	MESSAGEOUT not like '%SXPTaskAssignmentUpdate%'
	and
	MESSAGEOUT not like '%SXPExtendedTasKGetAppointments%'
	and
	MESSAGEOUT not like '%SXPTaskOperations%'
	--and
	--MESSAGEOUT not like '%SXPEngineerGetSchedule%'
)

delete from W6IM_INCOMING_LOG 
from W6IM_INCOMING_LOG loggy (nolock)
inner join #tmp2 tmp on tmp.MessageIn_Time = loggy.MessageIn_Time
where
(
	loggy.MESSAGEIN not like '%GetAvailableAppointments%'
	and 
	loggy.MESSAGEIN not like '%CreateOrUpdateTask%'
	and
	loggy.Messagein not like '%Dispatch Error%'
	and
    loggy.Messagein not like '%SXPTaskAssignmentUpdate%'
	and
	loggy.Messagein not like '%SXPExtendedTasKGetAppointments%'
	and
	loggy.Messagein not like '%SXPTaskOperations%'
	--and
	--loggy.MESSAGEIN not like '%SXPEngineerGetSchedule%'

)
  
  


END