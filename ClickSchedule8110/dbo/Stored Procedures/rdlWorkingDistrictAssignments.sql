﻿
 /******************************************************************************
**		File: rdlWorkingDistrictAssignments 
**		Name: 
**		Desc: Shows information concerning which techs are assigned to which 
**			Districts.  Also shows corresponding WorkingDistricts
**
**		Return values:
** 
**		Called by:  Click Reports
**              
**		Parameters:
**		Input							Output
**     ----------						-----------
**		
**
**		Auth: Collin Peterson
**		Date: 12/08/09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:			Description:
**		--------	--------		-------------------------------------------
**		
*******************************************************************************/

CREATE Procedure [dbo].[rdlWorkingDistrictAssignments] 
	@Region nvarchar(64)
AS

SELECT 
W6ENGINEERS.Name,
W6ENGINEERS.ID, 
W6REGIONS.Name AS HomeRegion, 
W6DISTRICTS.Name AS HomeDistrict,
a.DistrictName AS WorkingDistrict
FROM W6ENGINEERS with (nolock)
inner join W6REGIONS with (nolock)
	on W6Regions.W6Key = W6Engineers.Region 
inner join W6DISTRICTS with (nolock)
	on W6Districts.W6Key = W6Engineers.District
left outer join
	(select W6ENGINEERS.ID as EngineersId, W6DISTRICTS.Name as DistrictName
		from W6ENGINEERS with (nolock)
		inner join W6ENGINEERS_WORKINGDISTRICTS with (nolock)
 			on W6ENGINEERS_WORKINGDISTRICTS.W6Key = W6ENGINEERS.W6Key
		inner join W6DISTRICTS with (nolock)
			on W6Districts.W6Key = W6ENGINEERS_WORKINGDISTRICTS.District
	) a on a.EngineersId = W6ENGINEERS.ID
Where (@Region = 'All' or @Region = W6REGIONS.Name)
order by W6ENGINEERS.Name