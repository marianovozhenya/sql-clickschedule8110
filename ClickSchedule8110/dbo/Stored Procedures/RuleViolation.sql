﻿
CREATE PROCEDURE [dbo].[RuleViolation]
(
	@begindate	datetime,
	@enddate	datetime
)
	
AS
BEGIN
	SET NOCOUNT ON;


select distinct
t.CallID as Ticket,
t.InstallingOffice,
t.CreatedBy,
r.name as Region,
d.name as District,
a.assignedengineers as Techs,
CONVERT(nvarchar,t.TimeCreated,22) as 'Ticket Created Date',
CONVERT(nvarchar,t.AppointmentStart,22)AppointmentStart,
CONVERT(nvarchar,t.AppointmentFinish,22)AppointmentFinish,
t.Priority,
ts.Name as 'Current_Status',
c.Name as Country,
tt.name as TaskType,
t.Panel,
t.RuleViolations,
dept.departmentname as CreatedByDepartment,
schdept.departmentname as ScheduledByDepartment


from
W6TASKS t (nolock)
inner join W6DISTRICTS d (nolock) on t.District = d.W6Key
inner join W6REGIONS r (nolock) on t.Region = r.W6Key
inner join W6TASK_STATUSES ts (nolock) on t.Status = ts.W6Key
inner join W6COUNTRIES c (nolock) on t.CountryID = c.W6Key
inner join W6TASK_TYPES tt (nolock) on t.TaskType = tt.W6Key
inner join W6ASSIGNMENTS a (nolock) on t.W6Key = a.Task
inner join W6ENGINEERS e (nolock) on a.AssignedEngineers = e.Name and e.Internal = -1
left outer join mssql05.hssalesdaily.dbo.srvticket st (nolock) on t.callid = st.ticketnum
left outer join mssql05.hssalesdaily.dbo.departments dept (nolock) on st.createdbydepartmentid = dept.departmentid
left outer join mssql05.hssalesdaily.dbo.srvticketstatus stat (nolock) on st.ticketid = stat.ticketid and stat.statustypeid = 6
left outer join mssql05.hssalesdaily.dbo.employees emp (nolock) on stat.createdbyid = emp.personid
left outer join mssql05.hssalesdaily.dbo.departments schdept (nolock) on emp.departmentid = schdept.departmentid

where
t.RuleViolations is not null
and ts.Name not in ('Scheduled','Cancelled')
and tt.Name <> 'TEST-DO NOT USE'
and t.AppointmentStart > = '2012-04-03'
and stat.createddate >= @begindate and stat.createddate < DateAdd("d", 1, @enddate)

order by AppointmentStart,r.Name,D.Name

END