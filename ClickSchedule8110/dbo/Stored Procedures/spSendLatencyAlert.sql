﻿
CREATE proc [dbo].[spSendLatencyAlert] as 
declare @table nvarchar(max)

SET @table = 
'<H1>Salesforce latency alert</H1> <table border = "1">' +
'<tr><th>Server Name</th><th>Latency</th><th>MessageIN_Time</th>' +
cast ((SELECT
		td = [SERVERNAME], '',
		td = datediff(SECOND,messagein_time,messageout_time),'',
		td = [MessageIn_Time]
    FROM W6IM_INCOMING_LOG (nolock) 
	where 
		MessageIn_Time >= dateadd(MINUTE,-60,getdate()) 
		and (messagein like '%getavailable%') 
		and datediff(ss,messagein_time,messageout_time) > 10
	order by MessageIn_Time desc
for xml path ('tr'), TYPE) as nvarchar(MAX)) + 
'</table>';

--select @table

exec msdb.dbo.sp_send_dbmail @recipients = 'FAravena@vivint.com; grodriguez@vivint.com; vrodriguez@vivint.com; tom.beasley@vivint.com',
	@subject = 'Click latency alert',
	@profile_name = 'sqlalerts',
	@body = @table,
	@body_format = 'html';