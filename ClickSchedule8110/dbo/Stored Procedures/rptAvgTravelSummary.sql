﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [dbo].[rptAvgTravelSummary]
(
@BeginDate	datetime,
@EndDate	datetime
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



select 
COUNT(distinct vea.CallID) as 'Number Of Tickets',
(sum(vea.DefaultTravelDistance) / count(distinct vea.CallID)) / 1609.344 as 'Average Travel Miles',
(sum(vea.DefaultTravelTime) / count(distinct vea.CallID)) / 60 as 'Average Travel Minutes',
(MAX(vea.DefaultTravelDistance) / 1609.344) as 'Max Travel Miles',					--convert meters to miles
(MAX(vea.defaulttraveltime) / 60)as 'Max Travel Minutes',
MAX(vea.StartTime) as 'Most Recent Ticket',
MIN(vea.FinishTime) as 'Oldest Ticket',
SUM(case when vea.DefaultTravelDistance <= 321.8688 and vea.CallID is not null then 1 else 0 end) as 'Tickets Below .2 Miles',
SUM(case when vea.DefaultTravelDistance >= 160934.4 and vea.CallID is not null then 1 else 0 end) as 'Tickets Above 100 Miles'

from
vwEngineerAssignments vea (nolock)

where 
vea.CallID is not null        --Removes all Non-Availabilities
and vea.DefaultTravelDistance is not null       --Removes rows of data that haven't had travel calculated by the morning task
and starttime >= @BeginDate
and starttime < DateAdd(day, 1, @EndDate)




END