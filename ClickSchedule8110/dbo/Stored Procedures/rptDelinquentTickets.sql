﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[rptDelinquentTickets]




AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
r.Name as Region,
d.Name as District,
t.CallID,
t.AppointmentStart,
ts.Name as ClickStatus,
sst.name as CMSStatus,
STS.CREATEDdATE,
A.AssignedEngineers,
u.Username + '@vivint.com' as TechEmail,
src.name as ResultCode,
stc.CorrectiveAction
            
from W6TASKS T (nolock)  
inner join W6TASK_STATUSES TS (nolock) on t.Status = ts.W6Key
inner join W6ASSIGNMENTS A (nolock) on a.Task = t.W6Key
inner join W6DISTRICTS d (nolock) on t.District = d.W6Key
inner join W6REGIONS R (nolock) on t.Region = r.W6Key
left join W6ENGINEERS e (nolock) on a.AssignedEngineers = e.Name
inner join    
(
select
st.ticketnum,
st.ticketid,
max(sts.TicketStatusID) as MaxStatusID

       from mssql05.hssalesdaily.dbo.SrvTicketStatus sts (nolock)
       inner join  mssql05.hssalesdaily.dbo.SrvTicket st (nolock) on sts.ticketid = st.ticketid
      group by st.ticketnum,st.ticketid)  StatusTable on statustable.ticketnum = t.CallID  
      
inner join mssql05.hssalesdaily.dbo.srvTicketStatus sts  (nolock) on sts.TicketStatusID = StatusTable.MaxStatusID
inner join mssql05.hssalesdaily.dbo.srvTicket st  (nolock) on st.ticketID = StatusTable.ticketID
inner join mssql05.hssalesdaily.dbo.srvStatusType sst  (nolock) on sst.StatusTypeID = sts.StatusTypeID
left join mssql05.hssalesdaily.dbo.people p1 (nolock) on sts.createdbyid = p1.personid
left join mssql05.hssalesdaily.dbo.employees E1 (nolock) on p1.personid = e1.personid
left join mssql05.hssalesdaily.dbo.departments d1 (nolock) on e1.departmentid = d1.departmentid
left join mssql05.hssalesdaily.dbo.srvticketclose STC (nolock) on st.ticketid = stc.ticketid
left join mssql05.hssalesdaily.dbo.srvreasoncode SRC (nolock) on stc.resultcodeid = src.codevalue
left join mssql05.hssalesdaily.dbo.employees E2 (nolock) on e.ID = e2.employeeid
left join mssql05.hssalesdaily.dbo.users U (nolock)on e2.personid = U.personid


where ts.Name not in (
'Cancelled',
'Completed')
and
t.AppointmentStart < convert( varchar(20), GetDate(), 101)
and
sst.name = ts.Name

 order by
TS.Name




END