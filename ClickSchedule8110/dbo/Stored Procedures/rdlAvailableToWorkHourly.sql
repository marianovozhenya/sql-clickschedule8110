﻿
/******************************************************************************
**		File: rdlAvailableToWorkHourly 
**		Name: 
**		Desc: Shows when an Engineer is available to work.  Takes into account
**			weekly schedule and yearly schedule (days off each year)
**
**		DOCUMENTATION ON HOW CLICK HANDLES ENGINEERS AVAILABLE TIME IS INCLUDED 
**		BELOW THE STORED PROCEDURE DEFINITION
** 
**		Called by:  Click Reports
**              
**		Parameters:
**		Input							Output
**     ----------						-----------
**		Engineer, Start Date, End Date
**
**		Auth: Collin Peterson
**		Date: 12/14/09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:			Description:
**		--------	--------		-------------------------------------------
**		12/17/09	Collin			Rewrote and took out 2 cursors
*******************************************************************************/

CREATE Procedure [dbo].[rdlAvailableToWorkHourly] 
	@EngineerID int,
	@StartDate DateTime,
	@EndDate DateTime
AS

--Temp table serves as matrix to show Engineers (techs) availability
--Matrix is by Date (TheDay) and Hour (TheHour)
--The hours from 12 midnight to 6AM are intentionally left out.  Hour 1 equals 6AM
create table #tblDateRange
(
	TheDay DateTime, -- Input Range
	TheHour int, -- 0 through 17
	FirstDayOfWeek DateTime,
	StartingSeconds int,
	EndingSeconds int,
	HourStatus tinyint 
)

Declare @DateIncrement int, @HourIncrement int, @DaysToIncrementBy int, @AvailStart int, @AvailFinish int, @AvailStatus int,
	@AvailStartDate DateTime, @AvailFinishDate DateTime, @TechName varchar(50)

Set @TechName = (select [name] from w6engineers where W6Key = @EngineerID)

Set @DateIncrement = 0
Set @DaysToIncrementBy = 0

--Fill the Matrix
While (Select DateAdd(day, @DateIncrement, @StartDate)) <= @EndDate
Begin
	Set @HourIncrement = 0
	Set @DaysToIncrementBy = DateDiff(day, dbo.F_START_OF_WEEK(DateAdd(day, @DateIncrement, @StartDate),1), DateAdd(day, @DateIncrement, @StartDate))
	While @HourIncrement <= 17
	Begin
		insert into #tblDateRange(TheDay, TheHour, FirstDayOfWeek, StartingSeconds,	EndingSeconds, HourStatus) 
			select DateAdd(day, @DateIncrement, @StartDate), @HourIncrement, dbo.F_START_OF_WEEK(DateAdd(day, @DateIncrement, @StartDate),1),
				(@DaysToIncrementBy*(24*3600))+(@HourIncrement*3600)+(6*3600), (@DaysToIncrementBy*(24*3600))+(@HourIncrement*3600)+(6*3600)+3600, 10
		Set @HourIncrement = @HourIncrement + 1
	End
	Set @DateIncrement = @DateIncrement + 1
End

update #tblDateRange
set HourStatus = (
	select cwi.[Status] 
	from W6Engineers e with (nolock)
	inner join W6Calendars c with (nolock)
		on c.w6key = e.Calendar
	inner join W6Calendar_Weekly_Intervals cwi with (nolock)
		on cwi.w6key = c.w6key
	where e.w6key = @EngineerID
		and cwi.Seconds_To_Finish > #tblDateRange.StartingSeconds
		and cwi.Seconds_To_Start < #tblDateRange.EndingSeconds
	)
Where Exists (
	select cwi.[Status] 
	from W6Engineers e with (nolock)
	inner join W6Calendars c with (nolock)
		on c.w6key = e.Calendar
	inner join W6Calendar_Weekly_Intervals cwi with (nolock)
		on cwi.w6key = c.w6key
	where e.w6key = @EngineerID
		and cwi.Seconds_To_Finish > #tblDateRange.StartingSeconds
		and cwi.Seconds_To_Start < #tblDateRange.EndingSeconds
	)

--select * from #tblDateRange 

update #tblDateRange
set HourStatus = (
	select cyi.[Status] 
	from W6Engineers e with (nolock)
	inner join W6Calendars c with (nolock)
		on c.w6key = e.Calendar
	inner join W6Calendar_Yearly_Intervals cyi with (nolock)
		on cyi.w6key = c.w6key
	Where e.w6key = @EngineerID
		and cyi.Start_Time <= DateAdd(second, #tblDateRange.StartingSeconds, #tblDateRange.FirstDayOfWeek)
		and cyi.Finish_Time >= DateAdd(second, #tblDateRange.EndingSeconds, #tblDateRange.FirstDayOfWeek)
	)
Where Exists (
	select cyi.[Status] 
	from W6Engineers e with (nolock)
	inner join W6Calendars c with (nolock)
		on c.w6key = e.Calendar
	inner join W6Calendar_Yearly_Intervals cyi with (nolock)
		on cyi.w6key = c.w6key
	Where e.w6key = @EngineerID
		and cyi.Start_Time <= DateAdd(second, #tblDateRange.StartingSeconds, #tblDateRange.FirstDayOfWeek)
		and cyi.Finish_Time >= DateAdd(second, #tblDateRange.EndingSeconds, #tblDateRange.FirstDayOfWeek)
	)
	
select *, @TechName as TechName from #tblDateRange 
--select *, DateAdd(second, StartingSeconds, FirstDayOfWeek) as CellStartDate, DateAdd(second, EndingSeconds, FirstDayOfWeek) as CellFinishDate from #tblDateRange 

drop table #tblDateRange

/*
How the Weekly + Yearly Calendar tables work in Click

The W6Calendar_Yearly_Intervals: 
Allows administrators to setup a yearly calendar for the technician(s). Administrators will be able to create
working/non-working/optional hours for a given date for a given technician(s). For a given date, a separate entry
will need to be created. These are one-time, non-repeating entries. Typically this is used for a company holiday calendar.

For example: if planners know ahead of time that the technicians will not work on December 24, then an entry can be made for
this non-working day. 

W6Calendar_Weekly_Intervals: 
Represents the weekly repeating intervals for a specific week(s), that a technician has either working/non working/optional hours. 
For example: if a technician named John Smith’s working hours were defined as:  Monday: 9am-7pm and a 7pm-8pm optional time. 

Then the seconds_to_start and seconds_to_finish would be as follows:
Time period	Seconds to Start	Seconds to Finish	Status (0=non working, 1 = working, 2 =optional)
Monday	118800	154800	1
Monday (optional)	154800	158400	2

The calculation works as follows:
Both the Seconds_To_Start and Seconds_To_Finish are calculated as follows:
number of hours from start of week (which is Sunday at 12am) until start/finish time (in this case 9am and 7pm respectively, and or
7pm and 8pm (for optional)) converted into seconds.

For Monday the calculation is as follows:
Seconds_To_Start=  [24hrs (Sunday) + 9hrs (12am-9am)]*3600 = 118,800

Start_To_Finish = [24hrs (Sunday) + 19hrs (12am-7pm)]*3600 = 154,800

Seconds_To_Start (optional) = [24hrs (Sunday) + 19hrs (12am-7pm)]*3600 = 154,800
Seconds_To_Finish (optional) = [24hrs (Sunday) + 20hrs (12am-8pm)]*3600 = 158,400

As the week progresses (Tuesday, Wednesday and so forth), the numbers highlighted, are the ones that will increase, as the time from
the start of the week increases.

*/