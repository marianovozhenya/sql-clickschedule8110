﻿
CREATE Procedure [dbo].[rptNoShowCustomer_daterange]
( 
	@BeginDate DateTime,
	@EndDate DateTime
)

AS

SELECT     
a.W6Key AS AssignmentKey, 
t.W6Key AS TaskKey, 
ts.Name AS Status,  
e.W6Key AS EngineerKey, 
e.Name,
a.AssignedEngineers, 
r.Name AS Region, 
d.Name AS District, 
t.CallID, 
t.AppointmentStart, 
t.AppointmentFinish, 
t.DispatcherComment, 
a.CommentText,
rc.NAME AS ResultCode

FROM         
W6REGIONS r (nolock) INNER JOIN
W6ASSIGNMENTS a (nolock)
INNER JOIN W6ASSIGNMENTS_ENGINEERS ae (nolock) ON a.W6Key = ae.W6Key 
INNER JOIN W6ENGINEERS e (nolock) ON ae.EngineerKey = e.W6Key ON r.W6Key = e.Region 
INNER JOIN W6DISTRICTS d (nolock) ON e.District = d.W6Key 
FULL OUTER JOIN W6TASK_STATUSES ts (nolock)
INNER JOIN W6TASKS t (nolock) ON ts.W6Key = t.Status ON a.Task = t.W6Key
INNER JOIN W6RESULTCODES rc (NOLOCK) ON t.RESULTCODE = rc.W6KEY

WHERE     
(rc.name = 'No Show-Customer')
and t.appointmentstart >= @begindate and t.appointmentstart < DateAdd("d", 1, @EndDate)

ORDER BY AppointmentStart