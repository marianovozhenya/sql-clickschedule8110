﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================




CREATE PROCEDURE [dbo].[rptNASummary] 



AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	


		
	

Declare @Yesterday				date
Declare @FirstDayOf_ThisWeek	date
Declare @FirstDayOf_LastWeek	date
Declare @FirstDayOf_NextWeek	date
Declare @FirstDay_AfterNextWeek	date
Declare @NextMonth				int

Set @Yesterday = DateAdd( day, -1, convert(date, GetDate()) )
Set @FirstDayOf_ThisWeek = convert( date, DateAdd( day, - DatePart( dw, GetDate() ) + 2 , GetDate()  )  ) 
Set @FirstDayOf_LastWeek = convert( date, DateAdd( day, - DatePart( dw, GetDate() ) -5 , GetDate() ) ) 
Set @FirstDayOf_NextWeek = convert( date, DateAdd( day, - DatePart( dw, GetDate() ) + 9 , GetDate()  )  ) 
Set @FirstDay_AfterNextWeek = convert( date, DateAdd( day, - DatePart( dw, GetDate() ) + 16 , GetDate()  )  ) 
Set @NextMonth = Month ( GetDate() ) + 1

-----------------NAs:

IF OBJECT_ID('tempdb..#NAs') IS NOT NULL
	drop table #NAs
	
Create table #NAs 
(
NN							varchar(10),
Region						varchar(100),
Criteria					varchar(50),
TotalQTY					varchar(10),
DONOTUSE					varchar(10),
OneHRHoldPendingAppointment	varchar(10),
OneonOneTraining			varchar(10),
EquipmentExchangePickup		varchar(10),
FinishedForDay				varchar(10),
HelpingAnotherTech			varchar(10),
Sick						varchar(10),
TeamMeeting					varchar(10),
TimeOffRequest				varchar(10),
VehicleMaintenance			varchar(10),
CustomerCancelEnRoute               varchar(10),
FleetPickup                         varchar(10),
Projects                            varchar(10),
[Training:Drop-off/Pick-upTech]       varchar(10),
UPS                                  varchar(10),
[Verizon:PhoneIssues]                  varchar(10),
WrongAddressonTicket                 varchar(10),




Nulls						varchar(10)
)


--NAs Yesterday:
Insert Into #NAs
select
'A' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'NAs Yesterday' as Criteria,
sum( Case When													convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as TotalQTY,  
sum( Case When na.Name = '***DO NOT USE***H'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'DO NOT USE',
sum( Case When na.Name = '1HR Hold - Pending Appointment'	and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as '1HR Hold - Pending Appointment',
sum( Case When na.Name = '1-on-1 Training'					and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as '1-on-1 Training',
sum( Case When na.Name = 'Equipment Exchange/Pickup'		and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Equipment Exchange/Pickup',
sum( Case When na.Name = 'Finished for day'					and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Finished for day',
sum( Case When na.Name = 'Helping Another Tech'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Helping Another Tech',
sum( Case When na.Name = 'Sick'								and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Sick',
sum( Case When na.Name = 'Team Meeting'						and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Team Meeting',
sum( Case When na.Name = 'Time Off Request'					and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Time Off Request',
sum( Case When na.Name = 'Vehicle Maintenance'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Vehicle Maintenance',


sum( Case When na.Name = 'Customer Cancel En Route'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Customer Cancel En Route',

sum( Case When na.Name = 'Fleet Pickup'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Fleet Pickup',

sum( Case When na.Name = 'Projects'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Projects',

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Training: Drop-off or Pick-up a Tech',

sum( Case When na.Name = 'UPS'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'UPS',

sum( Case When na.Name = 'Verizon: Phone Issues'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Verizon: Phone Issues',

sum( Case When na.Name = 'Wrong Address on Ticket'				and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Wrong Address on Ticket',


sum( Case When na.Name is null								and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as 'Nulls'




from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%' 
and na.Name <> '2 Week Notice' 

Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1


--NAs This Week:
Insert Into #NAs
select
'B' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'NAs This Week' as Criteria,
sum( Case When													a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as TotalQTY,  

sum( Case When na.Name = '***DO NOT USE***H'				and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'DO NOT USE',

sum( Case When na.Name = '1HR Hold - Pending Appointment'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as '1HR Hold - Pending Appointment',

sum( Case When na.Name = '1-on-1 Training'					and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as '1-on-1 Training',
sum( Case When na.Name = 'Equipment Exchange/Pickup'		and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Equipment Exchange/Pickup',
sum( Case When na.Name = 'Finished for day'					and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Finished for day',
sum( Case When na.Name = 'Helping Another Tech'				and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Helping Another Tech',
sum( Case When na.Name = 'Sick'								and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Sick',
sum( Case When na.Name = 'Team Meeting'						and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Team Meeting',
sum( Case When na.Name = 'Time Off Request'					and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Time Off Request',
sum( Case When na.Name = 'Vehicle Maintenance'				and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Vehicle Maintenance',


sum( Case When na.Name = 'Customer Cancel En Route'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Customer Cancel En Route',


sum( Case When na.Name = 'Fleet Pickup'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Fleet Pickup',

sum( Case When na.Name = 'Projects'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Projects',

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Training: Drop-off or Pick-up a Tech',

sum( Case When na.Name = 'UPS'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'UPS',

sum( Case When na.Name = 'Verizon: Phone Issues'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Verizon: Phone Issues',

sum( Case When na.Name = 'Wrong Address on Ticket'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Wrong Address on Ticket',





sum( Case When na.Name is null								and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as 'Nulls'








from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%' 
and na.Name <> '2 Week Notice'   

Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1


--NAs Last Week:
Insert Into #NAs
select
'C' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'NAs Last Week' as Criteria,
sum( Case When												    a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as TotalQTY,  

sum( Case When na.Name = '***DO NOT USE***H'				and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'DO NOT USE',

sum( Case When na.Name = '1HR Hold - Pending Appointment'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as '1HR Hold - Pending Appointment',


sum( Case When na.Name = '1-on-1 Training'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as '1-on-1 Training',

sum( Case When na.Name = 'Equipment Exchange/Pickup'		and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Equipment Exchange/Pickup',
sum( Case When na.Name = 'Finished for day'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Finished for day',
sum( Case When na.Name = 'Helping Another Tech'				and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Helping Another Tech',
sum( Case When na.Name = 'Sick'								and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Sick',
sum( Case When na.Name = 'Team Meeting'						and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Team Meeting',
sum( Case When na.Name = 'Time Off Request'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Time Off Request',
sum( Case When na.Name = 'Vehicle Maintenance'				and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Vehicle Maintenance',




sum( Case When na.Name = 'Customer Cancel En Route'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Customer Cancel En Route',

sum( Case When na.Name = 'Fleet Pickup'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Fleet Pickup',

sum( Case When na.Name = 'Projects'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Projects',

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Training: Drop-off or Pick-up a Tech',

sum( Case When na.Name = 'UPS'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'UPS',

sum( Case When na.Name = 'Verizon: Phone Issues'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Verizon: Phone Issues',

sum( Case When na.Name = 'Wrong Address on Ticket'					and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Wrong Address on Ticket',


sum( Case When na.Name is null								and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek   Then 1 Else 0 End ) as 'Nulls'

from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%'
and na.Name <> '2 Week Notice'   

Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1



--NAs Assigned for next week:
Insert Into #NAs
select
'D' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'NAs Next Week' as Criteria,
sum( Case When                                                  a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as TotalQTY,  
sum( Case When na.Name = '***DO NOT USE***H'				and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'DO NOT USE',
sum( Case When na.Name = '1HR Hold - Pending Appointment'	and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as '1HR Hold - Pending Appointment',

sum( Case When na.Name = '1-on-1 Training'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as '1-on-1 Training',

sum( Case When na.Name = 'Equipment Exchange/Pickup'		and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Equipment Exchange/Pickup',
sum( Case When na.Name = 'Finished for day'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Finished for day',
sum( Case When na.Name = 'Helping Another Tech'				and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Helping Another Tech',
sum( Case When na.Name = 'Sick'								and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Sick',
sum( Case When na.Name = 'Team Meeting'						and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Team Meeting',
sum( Case When na.Name = 'Time Off Request'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Time Off Request',
sum( Case When na.Name = 'Vehicle Maintenance'				and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Vehicle Maintenance',


sum( Case When na.Name = 'Customer Cancel En Route'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Customer Cancel En Route',

sum( Case When na.Name = 'Fleet Pickup'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Fleet Pickup',

sum( Case When na.Name = 'Projects'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Projects',

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Training: Drop-off or Pick-up a Tech',

sum( Case When na.Name = 'UPS'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'UPS',

sum( Case When na.Name = 'Verizon: Phone Issues'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Verizon: Phone Issues',

sum( Case When na.Name = 'Wrong Address on Ticket'					and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Wrong Address on Ticket',


sum( Case When na.Name is null								and a.StartTime >= @FirstDayOf_NextWeek   and a.StartTime < @FirstDay_AfterNextWeek   Then 1 Else 0 End ) as 'Nulls'

from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%' 
and na.Name <> '2 Week Notice'  

Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1


--NAs Assigned for the following month:
Insert Into #NAs
select
'E' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'NAs Next Month' as Criteria,
sum( Case When													Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as TotalQTY,  
sum( Case When na.Name = '***DO NOT USE***H'				and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'DO NOT USE',
sum( Case When na.Name = '1HR Hold - Pending Appointment'	and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as '1HR Hold - Pending Appointment',

sum( Case When na.Name = '1-on-1 Training'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as '1-on-1 Training',



sum( Case When na.Name = 'Equipment Exchange/Pickup'		and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Equipment Exchange/Pickup',
sum( Case When na.Name = 'Finished for day'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Finished for day',
sum( Case When na.Name = 'Helping Another Tech'				and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Helping Another Tech',
sum( Case When na.Name = 'Sick'								and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Sick',
sum( Case When na.Name = 'Team Meeting'						and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Team Meeting',
sum( Case When na.Name = 'Time Off Request'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Time Off Request',
sum( Case When na.Name = 'Vehicle Maintenance'				and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Vehicle Maintenance',


sum( Case When na.Name = 'Customer Cancel En Route'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Customer Cancel En Route',

sum( Case When na.Name = 'Fleet Pickup'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Fleet Pickup',

sum( Case When na.Name = 'Projects'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Projects',

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Training: Drop-off or Pick-up a Tech',

sum( Case When na.Name = 'UPS'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'UPS',

sum( Case When na.Name = 'Verizon: Phone Issues'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Verizon: Phone Issues',
sum( Case When na.Name = 'Wrong Address on Ticket'					and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Wrong Address on Ticket',

sum( Case When na.Name is null								and Year(a.StartTime) = Year(GetDate()) and Month(a.StartTime) = @NextMonth   Then 1 Else 0 End ) as 'Nulls'

from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%'  
and na.Name <> '2 Week Notice' 

Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1



--Select * from #NAs 


------------------------Duration:

IF OBJECT_ID('tempdb..#Duration') IS NOT NULL
	drop table #Duration
	
Create table #Duration 
(
NN										varchar(10),
Region									varchar(100),
Criteria								varchar(50),
TotalQTY								int,
TotalQTY_Duration						int,
TotalQTY_AvgDuration					varchar(20),

DONOTUSE_QTY							int,
DONOTUSE_Duration						int,
DONOTUSE_AvgDuration					varchar(20),

OneHRHoldPendingAppointment_QTY			int,
OneHRHoldPendingAppointment_Duration	int,
OneHRHoldPendingAppointment_AvgDuration	varchar(20),
OneonOneTraining_QTY					int,
OneonOneTraining_Duration				int,
OneonOneTraining_AvgDuration			varchar(20),
EquipmentExchangePickup_QTY				int,
EquipmentExchangePickup_Duration		int,
EquipmentExchangePickup_AvgDuration		varchar(20),
FinishedForDay_QTY						int,
FinishedForDay_Duration					int,
FinishedForDay_AvgDuration				varchar(20),
HelpingAnotherTech_QTY					int,
HelpingAnotherTech_Duration				int,
HelpingAnotherTech_AvgDuration			varchar(20),
Sick_QTY								int,
Sick_Duration							int,
Sick_AvgDuration						varchar(20),
TeamMeeting_QTY							int,
TeamMeeting_Duration					int,
TeamMeeting_AvgDuration					varchar(20),
TimeOffRequest_QTY						int,
TimeOffRequest_Duration					int,
TimeOffRequest_AvgDuration				varchar(20),
VehicleMaintenance_QTY					int,
VehicleMaintenance_Duration				int,
VehicleMaintenance_AvgDuration			varchar(20),


CustomerCancelEnRoute_QTY				int,
CustomerCancelEnRoute_Duration			int,
CustomerCancelEnRoute_AvgDuration			varchar(20),

FleetPickup_QTY		   				int,
FleetPickup_Duration					int,
FleetPickup_AvgDuration				varchar(20),

Projects_QTY		   				int,
Projects_Duration						int,
Projects_AvgDuration				varchar(20),

[Training:Drop-off/Pick-upTech_QTY]			int,
[Training:Drop-off/Pick-upTech_Duration]		int,
[Training:Drop-off/Pick-upTech_AvgDuration]		varchar(20),

UPS_QTY			   				int,
UPS_Duration						int,
UPS_AvgDuration					varchar(20),

[Verizon:PhoneIssues_QTY]	   				int,
[Verizon:PhoneIssues_Duration]				int,
[Verizon:PhoneIssues_AvgDuration]			varchar(20),

WrongAddressonTicket_QTY   				int,
WrongAddressonTicket_Duration				int,
WrongAddressonTicket_AvgDuration			varchar(20),

Nulls_QTY								int,
Nulls_Duration							int,
Nulls_AvgDuration						varchar(20)
)

-- Duration Yesterday:
Insert Into #Duration
Select
'F' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'Avg Duration Yesterday' as Criteria,
sum( Case When na.Name is not null and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End )   as TotalQTY,  
sum( Case When na.Name is not null and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TotalQTY_Duration,
null,
sum( Case When na.Name = '***DO NOT USE***H' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End )  as DONOTUSE_QTY,
sum( Case When na.Name = '***DO NOT USE***H' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as DONOTUSE_Duration,				
null,
sum( Case When na.Name = '1HR Hold - Pending Appointment' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneHRHoldPendingAppointment_QTY,
sum( Case When na.Name = '1HR Hold - Pending Appointment' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneHRHoldPendingAppointment_Duration, 
null,
sum( Case When na.Name = '1-on-1 Training' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = '1-on-1 Training' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,
sum( Case When na.Name = 'Equipment Exchange/Pickup' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End )as EquipmentExchangePickup_QTY,
sum( Case When na.Name = 'Equipment Exchange/Pickup' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as EquipmentExchangePickup_Duration,		
null,
sum( Case When na.Name = 'Finished for day'	and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Finished for day'	and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,
sum( Case When na.Name = 'Helping Another Tech'	and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as HelpingAnotherTech_QTY,
sum( Case When na.Name = 'Helping Another Tech'	and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as HelpingAnotherTech_Duration,				
null,
sum( Case When na.Name = 'Sick' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as Sick_QTY,
sum( Case When na.Name = 'Sick' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as Sick_Duration,							
null,
sum( Case When na.Name = 'Team Meeting' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End )as TeamMeeting_QTY,
sum( Case When na.Name = 'Team Meeting' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TeamMeeting_Duration,					
null,
sum( Case When na.Name = 'Time Off Request' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End )  as TimeOffRequest_QTY,
sum( Case When na.Name = 'Time Off Request' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TimeOffRequest_Duration,				
null,
sum( Case When na.Name = 'Vehicle Maintenance' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as VehicleMaintenance_QTY,
sum( Case When na.Name = 'Vehicle Maintenance' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End )	as VehicleMaintenance_Duration,
null,



sum( Case When na.Name = 'Customer Cancel En Route'and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Customer Cancel En Route'and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Fleet Pickup' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Fleet Pickup' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Projects' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Projects' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'UPS' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'UPS' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Verizon: Phone Issues' and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Verizon: Phone Issues' and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Wrong Address on Ticket'and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Wrong Address on Ticket'and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name is null   and convert(date, a.StartTime) = @Yesterday Then 1 Else 0 End ) as Nulls_QTY,
sum( Case When na.Name is null   and convert(date, a.StartTime) = @Yesterday Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End )	as Nulls_Duration,
null				
				

from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%' 
and na.Name <> '2 Week Notice'   

Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1



--Duration This Week:
Insert Into #Duration
Select
'G' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'Avg Duration This Week' as Criteria,
sum( Case When na.Name is not null and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End )   as TotalQTY,  
sum( Case When na.Name is not null and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TotalQTY_Duration,
null,
sum( Case When na.Name = '***DO NOT USE***H' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End )  as DONOTUSE_QTY,
sum( Case When na.Name = '***DO NOT USE***H' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as DONOTUSE_Duration,				
null,
sum( Case When na.Name = '1HR Hold - Pending Appointment' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneHRHoldPendingAppointment_QTY,
sum( Case When na.Name = '1HR Hold - Pending Appointment' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneHRHoldPendingAppointment_Duration, 
null,
sum( Case When na.Name = '1-on-1 Training' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = '1-on-1 Training' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,
sum( Case When na.Name = 'Equipment Exchange/Pickup' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End )as EquipmentExchangePickup_QTY,
sum( Case When na.Name = 'Equipment Exchange/Pickup' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as EquipmentExchangePickup_Duration,		
null,
sum( Case When na.Name = 'Finished for day'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Finished for day'	and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,
sum( Case When na.Name = 'Helping Another Tech'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as HelpingAnotherTech_QTY,
sum( Case When na.Name = 'Helping Another Tech'	and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as HelpingAnotherTech_Duration,				
null,
sum( Case When na.Name = 'Sick'	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as Sick_QTY,
sum( Case When na.Name = 'Sick'	and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as Sick_Duration,							
null,
sum( Case When na.Name = 'Team Meeting' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End )as TeamMeeting_QTY,
sum( Case When na.Name = 'Team Meeting' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TeamMeeting_Duration,					
null,
sum( Case When na.Name = 'Time Off Request' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End )  as TimeOffRequest_QTY,
sum( Case When na.Name = 'Time Off Request' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TimeOffRequest_Duration,				
null,
sum( Case When na.Name = 'Vehicle Maintenance' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as VehicleMaintenance_QTY,
sum( Case When na.Name = 'Vehicle Maintenance' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End )	as VehicleMaintenance_Duration,
null,

sum( Case When na.Name = 'Customer Cancel En Route'and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Customer Cancel En Route'and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Fleet Pickup' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Fleet Pickup' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Projects' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Projects' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'UPS' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'UPS' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Verizon: Phone Issues' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Verizon: Phone Issues' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,

sum( Case When na.Name = 'Wrong Address on Ticket' and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = 'Wrong Address on Ticket' and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,


sum( Case When na.Name is null	and a.StartTime >= @FirstDayOf_ThisWeek Then 1 Else 0 End ) as Nulls_QTY,
sum( Case When na.Name is null  and a.StartTime >= @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End )	as Nulls_Duration,
null				
				

from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%' 
and na.Name <> '2 Week Notice' 
 
Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1


--Duration Last Week:
Insert Into #Duration
Select
'H' as NN,
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End as Region,
'Avg Duration Last Week' as Criteria,
sum( Case When na.Name is not null and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )   as TotalQTY,  
sum( Case When na.Name is not null and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TotalQTY_Duration,
null,
sum( Case When na.Name = '***DO NOT USE***H' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )  as DONOTUSE_QTY,
sum( Case When na.Name = '***DO NOT USE***H' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as DONOTUSE_Duration,				
null,
sum( Case When na.Name = '1HR Hold - Pending Appointment' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneHRHoldPendingAppointment_QTY,
sum( Case When na.Name = '1HR Hold - Pending Appointment' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneHRHoldPendingAppointment_Duration, 
null,
sum( Case When na.Name = '1-on-1 Training' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End ) as OneonOneTraining_QTY,
sum( Case When na.Name = '1-on-1 Training' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as OneonOneTraining_Duration,					
null,
sum( Case When na.Name = 'Equipment Exchange/Pickup' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as EquipmentExchangePickup_QTY,
sum( Case When na.Name = 'Equipment Exchange/Pickup' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as EquipmentExchangePickup_Duration,		
null,
sum( Case When na.Name = 'Finished for day'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Finished for day'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,
sum( Case When na.Name = 'Helping Another Tech'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End ) as HelpingAnotherTech_QTY,
sum( Case When na.Name = 'Helping Another Tech'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as HelpingAnotherTech_Duration,				
null,
sum( Case When na.Name = 'Sick' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End ) as Sick_QTY,
sum( Case When na.Name = 'Sick' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as Sick_Duration,							
null,
sum( Case When na.Name = 'Team Meeting' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as TeamMeeting_QTY,
sum( Case When na.Name = 'Team Meeting' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TeamMeeting_Duration,					
null,
sum( Case When na.Name = 'Time Off Request' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )  as TimeOffRequest_QTY,
sum( Case When na.Name = 'Time Off Request' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as TimeOffRequest_Duration,				
null,
sum( Case When na.Name = 'Vehicle Maintenance' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End ) as VehicleMaintenance_QTY,
sum( Case When na.Name = 'Vehicle Maintenance' and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End )	as VehicleMaintenance_Duration,
null,



sum( Case When na.Name = 'Customer Cancel En Route'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Customer Cancel En Route'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,

sum( Case When na.Name = 'Fleet Pickup'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Fleet Pickup'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,

sum( Case When na.Name = 'Projects'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Projects'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,

sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Training: Drop-off or Pick-up a Tech'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,

sum( Case When na.Name = 'UPS'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'UPS'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,

sum( Case When na.Name = 'Verizon: Phone Issues'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Verizon: Phone Issues'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,

sum( Case When na.Name = 'Wrong Address on Ticket'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End )as FinishedForDay_QTY	,
sum( Case When na.Name = 'Wrong Address on Ticket'	and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End ) as FinishedForDay_Duration,				
null,


sum( Case When na.Name is null and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then 1 Else 0 End ) as Nulls_QTY,
sum( Case When na.Name is null and a.StartTime >= @FirstDayOf_LastWeek    and a.StartTime < @FirstDayOf_ThisWeek Then DateDiff( minute, a.StartTime, a.FinishTime) Else 0 End )	as Nulls_Duration,
null								

from MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS a (nolocK)
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ASSIGNMENTS_ENGINEERS ae (nolock) on a.W6Key = ae.W6Key and a.Task is null
inner join MSSQLClickVir1.ClickSchedule.dbo.W6ENGINEERS e (nolock) on ae.EngineerKey = e.W6Key
left outer join MSSQLClickVir1.ClickSchedule.dbo.W6NONAVAILABILITY_TYPES na (nolock) on a.NonAvailabilityType = na.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6REGIONS r (nolock) on e.Region = r.W6Key
left join MSSQLClickVir1.ClickSchedule.dbo.W6DISTRICTS d (nolock) on e.District = d.W6Key

Where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%' 
and na.Name <> '2 Week Notice'   

Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     End
Order By 1



Update #Duration Set TotalQTY_AvgDuration = TotalQTY_Duration / TotalQTY Where TotalQTY <> 0
Update #Duration Set DONOTUSE_AvgDuration = DONOTUSE_Duration / DONOTUSE_QTY Where DONOTUSE_QTY <> 0
Update #Duration Set OneHRHoldPendingAppointment_AvgDuration = OneHRHoldPendingAppointment_Duration / OneHRHoldPendingAppointment_QTY Where OneHRHoldPendingAppointment_QTY <> 0
Update #Duration Set OneonOneTraining_AvgDuration = OneonOneTraining_Duration / OneonOneTraining_QTY Where OneonOneTraining_QTY <> 0
Update #Duration Set EquipmentExchangePickup_AvgDuration = EquipmentExchangePickup_Duration / EquipmentExchangePickup_QTY Where EquipmentExchangePickup_QTY <> 0
Update #Duration Set FinishedForDay_AvgDuration = FinishedForDay_Duration / FinishedForDay_QTY Where FinishedForDay_QTY <> 0
Update #Duration Set HelpingAnotherTech_AvgDuration = HelpingAnotherTech_Duration / HelpingAnotherTech_QTY Where HelpingAnotherTech_QTY <> 0
Update #Duration Set Sick_AvgDuration = Sick_Duration / Sick_QTY Where Sick_QTY <> 0
Update #Duration Set TeamMeeting_AvgDuration = TeamMeeting_Duration / TeamMeeting_QTY Where TeamMeeting_QTY <> 0
Update #Duration Set TimeOffRequest_AvgDuration = TimeOffRequest_Duration / TimeOffRequest_QTY Where TimeOffRequest_QTY <> 0
Update #Duration Set VehicleMaintenance_AvgDuration = VehicleMaintenance_Duration / VehicleMaintenance_QTY Where VehicleMaintenance_QTY <> 0
Update #Duration Set OneonOneTraining_AvgDuration = OneonOneTraining_Duration / OneonOneTraining_QTY Where OneonOneTraining_QTY <> 0
Update #Duration Set CustomerCancelEnRoute_AvgDuration = CustomerCancelEnRoute_Duration / CustomerCancelEnRoute_QTY Where CustomerCancelEnRoute_QTY <> 0
Update #Duration Set FleetPickup_AvgDuration = FleetPickup_Duration / FleetPickup_QTY Where FleetPickup_QTY <> 0
Update #Duration Set Projects_AvgDuration = Projects_Duration / Projects_QTY Where Projects_QTY <> 0
Update #Duration Set [Training:Drop-off/Pick-upTech_AvgDuration] = [Training:Drop-off/Pick-upTech_Duration] / [Training:Drop-off/Pick-upTech_QTY] Where [Training:Drop-off/Pick-upTech_QTY] <> 0
Update #Duration Set UPS_AvgDuration = UPS_Duration / UPS_QTY Where UPS_QTY <> 0
Update #Duration Set [Verizon:PhoneIssues_AvgDuration] = [Verizon:PhoneIssues_Duration] / [Verizon:PhoneIssues_QTY] Where [Verizon:PhoneIssues_QTY] <> 0
Update #Duration Set WrongAddressonTicket_AvgDuration = WrongAddressonTicket_Duration / WrongAddressonTicket_QTY Where WrongAddressonTicket_QTY <> 0
Update #Duration Set Nulls_AvgDuration = Nulls_Duration / Nulls_QTY Where Nulls_QTY <> 0


Update #Duration Set TotalQTY_AvgDuration = 0 Where TotalQTY_AvgDuration is null
Update #Duration Set DONOTUSE_AvgDuration = 0 Where DONOTUSE_AvgDuration is null
Update #Duration Set OneHRHoldPendingAppointment_AvgDuration = 0 Where OneHRHoldPendingAppointment_AvgDuration is null
Update #Duration Set OneonOneTraining_AvgDuration = 0 Where OneonOneTraining_AvgDuration is null
Update #Duration Set EquipmentExchangePickup_AvgDuration = 0 Where EquipmentExchangePickup_AvgDuration is null
Update #Duration Set FinishedForDay_AvgDuration = 0 Where FinishedForDay_AvgDuration is null
Update #Duration Set HelpingAnotherTech_AvgDuration = 0 Where HelpingAnotherTech_AvgDuration is null
Update #Duration Set Sick_AvgDuration = 0 Where Sick_AvgDuration is null
Update #Duration Set TeamMeeting_AvgDuration = 0 Where TeamMeeting_AvgDuration is null
Update #Duration Set TimeOffRequest_AvgDuration = 0 Where TimeOffRequest_AvgDuration is null
Update #Duration Set VehicleMaintenance_AvgDuration = 0 Where VehicleMaintenance_AvgDuration is null
Update #Duration Set CustomerCancelEnRoute_AvgDuration = 0 Where CustomerCancelEnRoute_AvgDuration is null
Update #Duration Set FleetPickup_AvgDuration = 0 Where FleetPickup_AvgDuration is null
Update #Duration Set Projects_AvgDuration = 0 Where Projects_AvgDuration is null
Update #Duration Set [Training:Drop-off/Pick-upTech_AvgDuration] = 0 Where [Training:Drop-off/Pick-upTech_AvgDuration] is null
Update #Duration Set UPS_AvgDuration = 0 Where UPS_AvgDuration is null
Update #Duration Set [Verizon:PhoneIssues_AvgDuration] = 0 Where [Verizon:PhoneIssues_AvgDuration] is null
Update #Duration Set WrongAddressonTicket_AvgDuration = 0 Where WrongAddressonTicket_AvgDuration is null
Update #Duration Set Nulls_AvgDuration = 0 Where Nulls_AvgDuration is null


Update #Duration Set TotalQTY_AvgDuration = cast ( cast( TotalQTY_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((TotalQTY_AvgDuration % 60) as varchar(20)) + ' m' Where TotalQTY_AvgDuration <> 0
Update #Duration Set DONOTUSE_AvgDuration = cast ( cast( DONOTUSE_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((DONOTUSE_AvgDuration % 60) as varchar(20)) + ' m' Where DONOTUSE_AvgDuration <> 0
Update #Duration Set OneHRHoldPendingAppointment_AvgDuration = cast ( cast( OneHRHoldPendingAppointment_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((OneHRHoldPendingAppointment_AvgDuration % 60) as varchar(20)) + ' m' Where OneHRHoldPendingAppointment_AvgDuration <> 0
Update #Duration Set OneonOneTraining_AvgDuration = cast ( cast( OneonOneTraining_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((OneonOneTraining_AvgDuration % 60) as varchar(20)) + ' m' Where OneonOneTraining_AvgDuration <> 0
Update #Duration Set EquipmentExchangePickup_AvgDuration = cast ( cast( EquipmentExchangePickup_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((EquipmentExchangePickup_AvgDuration % 60) as varchar(20)) + ' m' Where EquipmentExchangePickup_AvgDuration <> 0
Update #Duration Set FinishedForDay_AvgDuration = cast ( cast( FinishedForDay_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((FinishedForDay_AvgDuration % 60) as varchar(20)) + ' m' Where FinishedForDay_AvgDuration <> 0
Update #Duration Set HelpingAnotherTech_AvgDuration = cast ( cast( HelpingAnotherTech_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((HelpingAnotherTech_AvgDuration % 60) as varchar(20)) + ' m' Where HelpingAnotherTech_AvgDuration <> 0
Update #Duration Set Sick_AvgDuration = cast ( cast( Sick_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((Sick_AvgDuration % 60) as varchar(20)) + ' m' Where Sick_AvgDuration <> 0
Update #Duration Set TeamMeeting_AvgDuration = cast ( cast( TeamMeeting_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((TeamMeeting_AvgDuration % 60) as varchar(20)) + ' m' Where TeamMeeting_AvgDuration <> 0
Update #Duration Set TimeOffRequest_AvgDuration = cast ( cast( TimeOffRequest_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((TimeOffRequest_AvgDuration % 60) as varchar(20)) + ' m' Where TimeOffRequest_AvgDuration <> 0
Update #Duration Set VehicleMaintenance_AvgDuration = cast ( cast( VehicleMaintenance_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((VehicleMaintenance_AvgDuration % 60) as varchar(20)) + ' m' Where VehicleMaintenance_AvgDuration <> 0

Update #Duration Set CustomerCancelEnRoute_AvgDuration = cast ( cast(CustomerCancelEnRoute_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((CustomerCancelEnRoute_AvgDuration % 60) as varchar(20)) + ' m' Where CustomerCancelEnRoute_AvgDuration <> 0

Update #Duration Set FleetPickup_AvgDuration = cast ( cast(FleetPickup_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((FleetPickup_AvgDuration % 60) as varchar(20)) + ' m' Where FleetPickup_AvgDuration <> 0

Update #Duration Set Projects_AvgDuration = cast ( cast(Projects_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((Projects_AvgDuration % 60) as varchar(20)) + ' m' Where Projects_AvgDuration <> 0

Update #Duration Set [Training:Drop-off/Pick-upTech_AvgDuration] = cast ( cast([Training:Drop-off/Pick-upTech_AvgDuration]/60 as int) as varchar(10)) + ' h ' +  cast(([Training:Drop-off/Pick-upTech_AvgDuration] % 60) as varchar(20)) + ' m' Where [Training:Drop-off/Pick-upTech_AvgDuration] <> 0

Update #Duration Set UPS_AvgDuration = cast ( cast(UPS_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((UPS_AvgDuration % 60) as varchar(20)) + ' m' Where UPS_AvgDuration <> 0

Update #Duration Set [Verizon:PhoneIssues_AvgDuration] = cast ( cast([Verizon:PhoneIssues_AvgDuration]/60 as int) as varchar(10)) + ' h ' +  cast(([Verizon:PhoneIssues_AvgDuration] % 60) as varchar(20)) + ' m' Where [Verizon:PhoneIssues_AvgDuration] <> 0

Update #Duration Set WrongAddressonTicket_AvgDuration = cast ( cast(WrongAddressonTicket_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((WrongAddressonTicket_AvgDuration % 60) as varchar(20)) + ' m' Where WrongAddressonTicket_AvgDuration <> 0

Update #Duration Set Nulls_AvgDuration = cast ( cast( Nulls_AvgDuration/60 as int) as varchar(10)) + ' h ' +  cast((Nulls_AvgDuration % 60) as varchar(20)) + ' m' Where Nulls_AvgDuration <> 0


--Select * from #Duration 


Select * from #NAs
Union 
Select NN, Region, Criteria, 
	   TotalQTY_AvgDuration, DONOTUSE_AvgDuration, OneHRHoldPendingAppointment_AvgDuration, OneonOneTraining_AvgDuration, EquipmentExchangePickup_AvgDuration, 
	   FinishedForDay_AvgDuration, HelpingAnotherTech_AvgDuration, Sick_AvgDuration, TeamMeeting_AvgDuration, TimeOffRequest_AvgDuration, VehicleMaintenance_AvgDuration, CustomerCancelEnRoute_AvgDuration, FleetPickup_AvgDuration, Projects_AvgDuration, [Training:Drop-off/Pick-upTech_AvgDuration], UPS_AvgDuration, [Verizon:PhoneIssues_AvgDuration], WrongAddressonTicket_AvgDuration, Nulls_AvgDuration
	   
From #Duration
	   Order By 2,1	   





END