﻿


/*
Author:							Tatiana Petrov
Create date:					03/12/2013                 
Description:					Combined 26 queries from Victor in one report.
Requested By:                   By Victor Rodriguez
Used By Crystal Report:			Yes
Crystal Report Name:			NextAvailableAppointment.rpt

Modifications(Date/By Name/Requested By/Description):
09/30/2013	/	By TP		/ Requested By Victor Rodriguez		/ Added condition: and ssc.ServiceCategory = 'Install Primary' 
10/15/2013  /   By TP       / Requested By Victor Rodriguez		/ Modified the @SrvCodeOption conditions


*/

--		rptNextAvailableAppointment '03/01/2013', '03/12/2013', 'US', 'DayWeek Report Summary', 'INS No Pulls'

CREATE PROCEDURE [dbo].[rptNextAvailableAppointment]
(
@BeginDate datetime,
@EndDate datetime,
@Country varchar(20),
@ReportType varchar(200),
@SrvCodeOption varchar(200)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select
	il.EntryID,
	SUBSTRING(il.messagein,74,13) as TicketNum,
	SUBSTRING(il.messagein,charindex('<Start>',il.messagein)+7,10) as PeriodStart,
	SUBSTRING(il.messagein,charindex('<Finish>',il.messagein)+8,10) as PeriodFinish,
	convert(nvarchar,il.MessageIn_Time,23) as RequestDate,
	il.MessageIn_Time
into #TicketIncoming
From W6IM_INCOMING_LOG il (nolock)
where il.MESSAGEIN like '%<SXPExtendedTaskGetAppointments Revision="7.5.0">%'
order by TicketNum

--CREATE NONCLUSTERED INDEX entryid_ind ON #TicketIncoming (entryid)
--CREATE NONCLUSTERED INDEX TicketNum_ind ON #TicketIncoming (TicketNum)



select
	replace(ti.ticketnum,'/','')as TicketNum,
	ti.RequestDate,
	ti.PeriodStart,
	ti.PeriodFinish,
	substring (il.messageout,82,19)as FirstOfferedAppointment
into #IncomingSlash
from #TicketIncoming ti (nolock)
inner join W6IM_INCOMING_LOG il (nolock)  on il.entryid = ti.entryid and il.MessageIn_Time = ti.MessageIn_Time
where il.MESSAGEIN like '%<SXPExtendedTaskGetAppointments Revision="7.5.0">%'
and il.MESSAGEOUT like '%<SXPExtendedTaskGetAppointmentsResult><OptionalAppointments><TimeInterval><Start>%'
order by TicketNum

--CREATE NONCLUSTERED INDEX TicketNum_ind ON #IncomingSlash (TicketNum)



Update #IncomingSlash			-- was #FirstOffered
Set TicketNum = replace(ticketnum,'<','')



Select
	il.entryid,
	SUBSTRING(il.messageout,charindex('<CallID>',il.messageout)+8,13) as TicketNum,
	il.MessageOut_Time
into #TicketOutgoing
From W6IM_INCOMING_LOG il (nolock)
where il.messageout like '%SXPTaskOperationsResult><ReturnCode>Success</ReturnCode>%'

CREATE NONCLUSTERED INDEX entryid_ind ON #TicketOutgoing (entryid)



select 
il.EntryID,
replace (tot.ticketnum,'/','')as TicketNum,
il.MessageOut_Time
into #OUTGOINGSLASH
from #TicketOutgoing tot (nolock)
inner join W6IM_INCOMING_LOG il (nolock) on il.EntryID = tot.EntryID and il.MessageOut_Time = tot.MessageOut_Time
where il.messageout like '%SXPTaskOperationsResult><ReturnCode>Success</ReturnCode>%'

--CREATE NONCLUSTERED INDEX TicketNum_ind ON #OUTGOINGSLASH (TicketNum)



Update #OUTGOINGSLASH			--was #Chosen
Set TicketNum = replace (ticketnum,'<','')



Select distinct
	c.Name as Country,
	r.Name as Region,
	d.Name as District,
	t.InstallingOffice,
	ch.TicketNum,
	fo.RequestDate,
	ass.TimeCreated as ScheduledOn,
	DATEPART(dw,ass.TimeCreated) as DayWeek,
	fo.PeriodStart,
	fo.PeriodFinish, 
	cast((DateDIFF(hh,ass.timecreated,fo.FirstOfferedAppointment)/24.0) as decimal (8,1)) as SoonestApptDayDiff,
	FO.FirstOfferedAppointment as SoonestAppt,
	cast((DateDIFF(hh,ass.timecreated,t.AppointmentStart)/24.0) as decimal (8,1)) as ChosenApptDayDIff,
	t.AppointmentStart as ChosenAppointment,
	tt.name as Dispatch,
	ssc.ServiceCode,
	ssc.ServiceCategory,
	ass.AssignedEngineers as AssignedTech
Into #FinalSummary
from #IncomingSlash fo (nolock)  --was #FirstOffered 
inner join #OUTGOINGSLASH ch (nolock) on FO.TicketNum = ch.TicketNum  -- was #Chosen
inner Join #TicketOutgoing tot (nolock)on ch.EntryID = tot.EntryID
inner join W6TASKS t (nolock)on fo.TicketNum = t.CallID
inner join W6TASK_TYPES tt (nolock) on t.TaskType = tt.W6Key 
inner join mssql05.hssalesdaily.dbo.srvservicecode ssc (nolock) on tt.Name = ssc.servicedesc
inner join W6REGIONS r (nolock) on t.Region = r.W6Key
inner join W6DISTRICTS d (nolock) on t.District = d.W6Key
inner join W6ASSIGNMENTS ass (nolock) on t.W6Key = ass.Task
Inner Join W6COUNTRIES C (nolock) on t.CountryID = c.W6Key
inner join W6ENGINEERS E (nolock) on ass.AssignedEngineers = e.Name
Where
ass.TimeCreated >= @BeginDate 
and ass.TimeCreated < DateAdd( day, 1, @EndDate)  
and RequestDate = PeriodStart
and 
	(
		(@Country = 'Canada' and d.CountryID = '126107649')       
		or
		(@Country = 'US' and d.CountryID = '126107648') 
		or 
		(@Country = 'New Zealand' and d.CountryID = '260335616')--10/15/2013  '126107650')
		or
		(@Country = 'US-Canada' and d.CountryID in ('126107649', '126107648'))
		or
		(@Country = 'US-Canada-New Zealand' and d.CountryID in ('126107649', '126107648', '260335616')) --10/15/2013  '126107650'))
	) 
and e.Internal = -1
and r.name not like 'Solar%'
--and ssc.ServiceCategory = 'Install Primary'  --10/15/2013 TP commented per Victor R
order by c.Name, r.Name, d.Name, ch.TicketNum, periodstart, RequestDate, ScheduledOn



Create table #temp 
(
Column1				varchar(100),
Column2				varchar(100),
Column3				varchar(100),
SoonestOffered		decimal(10,2),
ActualChosenAppt	decimal(10,2),
CountOfTickets		int
)

If @ReportType = 'Vivint Summary'
	Begin 
	        Insert Into #temp
			Select
				'' as Column1,
				'' as Column2,
				'Vivint Summary' as Column3, 
				AVG(FS.SoonestApptDayDiff)as SoonestOffered,
				AVG(FS.ChosenApptDayDiff)as ActualChosenAppt,
				Count(FS.TicketNum) as CountOfTickets
			from #FinalSummary FS (nolock)
			Where FS.SoonestApptDayDiff < '21.1'
			and fs.soonestapptdaydiff > '-1.0'
			and fs.ChosenApptDayDiff < '21.1'
			and 
				( 
					(@SrvCodeOption = 'NAAI' and fs.servicecode = 'INS' and fs.servicecategory = 'Install Primary' and fs.installingoffice like 'NIS%') --10/15/2013 (@SrvCodeOption = 'INS No Pulls' and fs.servicecode = 'INS' and fs.servicecategory <> 'Pull')
					or
					(@SrvCodeOption = 'NAAS' and fs.servicecode <> 'INS')-- 10/15/2013 (@SrvCodeOption = 'No INS' and fs.servicecode <> 'INS')
				)
	End


If @ReportType = 'District Report Summary'
	Begin
	        Insert Into #temp
			Select 
				FS.Country as Column1, 
				FS.Region as Column2,
				FS.District as Column3,
				AVG(FS.SoonestApptDayDiff)as SoonestOffered,
				AVG(FS.ChosenApptDayDiff)as ActualChosenAppt,
				Count(FS.TicketNum) as CountOfTickets
			from #FinalSummary FS (nolock)
			Where FS.SoonestApptDayDiff < '21.1'
			and fs.soonestapptdaydiff > '-1.0'
			and FS.ChosenApptDayDiff < '21.1'
			and 
				( 
					(@SrvCodeOption = 'NAAI' and fs.servicecode = 'INS' and fs.servicecategory = 'Install Primary' and fs.installingoffice like 'NIS%') --(@SrvCodeOption = 'INS No Pulls' and fs.servicecode = 'INS' and fs.servicecategory <> 'Pull')
					or
					(@SrvCodeOption = 'NAAS' and fs.servicecode <> 'INS')--(@SrvCodeOption = 'No INS' and fs.servicecode <> 'INS')
				)
			Group by FS.Country, FS.Region, FS.District
			Order by Fs.Region,Fs.District
	End


If @ReportType = 'Summary By Regions'
	Begin 
	        Insert Into #temp
			Select
				'' as Column1,
				'Summary By Regions' as Column2,
				FS.Region as Column3,
				AVG(FS.SoonestApptDayDiff)as SoonestOffered,
				AVG(FS.ChosenApptDayDiff)as ActualChosenAppt,
				Count(FS.TicketNum) as CountOfTickets
			from #FinalSummary FS (nolock)
			Where FS.SoonestApptDayDiff < '21.1'
			and fs.soonestapptdaydiff > '-1.0'
			and FS.ChosenApptDayDiff < '21.1'
			and 
				( 
					(@SrvCodeOption = 'NAAI' and fs.servicecode = 'INS' and fs.servicecategory = 'Install Primary' and fs.installingoffice like 'NIS%') --(@SrvCodeOption = 'INS No Pulls' and fs.servicecode = 'INS' and fs.servicecategory <> 'Pull')
					or
					(@SrvCodeOption = 'NAAS' and fs.servicecode <> 'INS')--(@SrvCodeOption = 'No INS' and fs.servicecode <> 'INS')
				)
			Group by FS.Region
			order by fs.Region
	End


If @ReportType = 'Summary by Installing Office'
	Begin 
	        Insert Into #temp
			Select 
				'' as Column1,
				'Summary by Installing Office' as Column2,
				FS.InstallingOffice as Column3,
				AVG(FS.SoonestApptDayDiff)as SoonestOffered,
				AVG(FS.ChosenApptDayDiff)as ActualChosenAppt,
				Count(FS.TicketNum) as CountOfTickets
			from #FinalSummary FS (nolock)
			Where FS.SoonestApptDayDiff < '21.1'
			and fs.soonestapptdaydiff > '-1.0'
			and FS.ChosenApptDayDiff < '21.1'
			and FS.InstallingOffice like 'NIS%'
			and 
				( 
					(@SrvCodeOption = 'NAAI' and fs.servicecode = 'INS' and fs.servicecategory = 'Install Primary' and fs.installingoffice like 'NIS%') --(@SrvCodeOption = 'INS No Pulls' and fs.servicecode = 'INS' and fs.servicecategory <> 'Pull')
					or
					(@SrvCodeOption = 'NAAS' and fs.servicecode <> 'INS')--(@SrvCodeOption = 'No INS' and fs.servicecode <> 'INS')
				)
			Group by FS.InstallingOffice
			order by fs.InstallingOffice
	End


If @ReportType = 'Summary by ServiceCode'
	Begin 
	        Insert Into #temp
			Select 
				'' as Column1,
				'Summary by ServiceCode' as Column2,
				ssc.ServiceCode as Column3,
				AVG(fs.SoonestApptDayDiff)as SoonestOffered,
				AVG(fs.ChosenApptDayDiff)as ActualChosenAppt,
				Count(fs.TicketNum) as CountOfTickets
			from #FinalSummary fs (nolock)
			inner join mssql05.hssalesdaily.dbo.srvServiceCode ssc (nolock) on fs.Dispatch = ssc.Servicedesc
			Where FS.SoonestApptDayDiff < '21.1'
			and fs.soonestapptdaydiff > '-1.0'
			and FS.ChosenApptDayDiff < '21.1'
			and 
				( 
					(@SrvCodeOption = 'NAAI' and fs.servicecode = 'INS' and fs.servicecategory = 'Install Primary' and fs.installingoffice like 'NIS%') --(@SrvCodeOption = 'INS No Pulls' and fs.servicecode = 'INS' and fs.servicecategory <> 'Pull')
					or
					(@SrvCodeOption = 'NAAS' and fs.servicecode <> 'INS')--(@SrvCodeOption = 'No INS' and fs.servicecode <> 'INS')
				)
			Group by Ssc.ServiceCode
			order by Ssc.ServiceCode
	End


If @ReportType = 'DayWeek Report Summary'
	Begin 
	        Insert Into #temp
			Select 
				'' as Column1,
				'DayWeek Report Summary' as Column2,
				Case 
					when fs.DayWeek = 1 then 'Sunday'
					when fs.DayWeek = 2 then 'Monday'
					when fs.DayWeek = 3 then 'Tuesday'
					when fs.DayWeek = 4 then 'Wednesday'
					when fs.DayWeek = 5 then 'Thursday'
					when fs.DayWeek = 6 then 'Friday'
					when fs.DayWeek = 7 then 'Saturday'
					end as  Column3,		--RequestedOn,
				AVG(FS.SoonestApptDayDiff)as SoonestOffered,
				AVG(FS.ChosenApptDayDiff)as ActualChosenAppt,
				Count(FS.TicketNum) as CountOfTickets
			from #FinalSummary fs (nolock)
			Where fs.SoonestApptDayDiff < '21.1'
			and fs.soonestapptdaydiff > '-1.0'
			and fs.ChosenApptDayDiff < '21.1'
			and 
				( 
					(@SrvCodeOption = 'NAAI' and fs.servicecode = 'INS' and fs.servicecategory = 'Install Primary' and fs.installingoffice like 'NIS%') --(@SrvCodeOption = 'INS No Pulls' and fs.servicecode = 'INS' and fs.servicecategory <> 'Pull')
					or
					(@SrvCodeOption = 'NAAS' and fs.servicecode <> 'INS')--(@SrvCodeOption = 'No INS' and fs.servicecode <> 'INS')
				)
			Group by FS.DayWeek
			Order by Fs.DayWeek
	End


Select * from #temp



END