﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [dbo].[rptDailyErrorTickets]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;




select 
t.CreatedBy,
a.CreatedBy,
t.ModifiedBy,
a.W6Key as AssignmentKey,
A.Task as NA,
T.W6Key as TaskKey,
t.Pinned,
R.Name as Region,
d.Name as District,
A.AssignedEngineers,
a.StartTime,
t.CallID,
t.street,
A.Street as street,
t.City,
a.City,
t.State,
a.State,
t.Postcode,
a.Postcode,
a.Latitude,
a.Longitude,
t.Latitude,
t.Longitude,
A.GISDataSource,
tt.Name as Dispatch,
ts.Name as Status,
na.Name as NAType,
c.Name as Country,
a.CountryID,
t.CountryID


 from w6assignments a (nolock)
 left join W6TASKS t (nolock) on a.Task = t.W6Key
 left join W6TASK_STATUSES TS (nolock) on t.Status = ts.W6Key
 left join W6REGIONS R (nolock) on t.Region = r.W6Key
 left join W6DISTRICTS D (nolock) on t.District = d.W6Key
 left join W6COUNTRIES C (nolock) on t.CountryID = c.W6Key
 left join W6TASK_TYPES TT (nolock) on t.TaskType = tt.W6Key
 left join W6NONAVAILABILITY_TYPES NA (nolock) on a.NonAvailabilityType = na.W6Key
  where 
  
  t.GISDataSource not in (
  
  '1',
  '4'
  )
  or
  a.GISDataSource not in (
  '1',
  '4'
  )
  or
  t.Latitude = '0'
  or
  t.Longitude ='0'
  or
  a.Latitude = '0'
  or 
  a.Longitude = '0'
  --or
  ----(t.CountryID is null
  ----and na.Name is null)
  --or
  --t.Pinned = '-1'
  --or
  --d.TimeZone is null
  or
  ts.Name = 'Open'
   or 
  ( t.GISDataSource= '4'
  and
  d.Name = 'Abilene, TX')
  or
  a.CountryID is null
  --or 
  --a.task is null
  or
  d.Name like 'San Juan%'
  or 
  d.Name like'Ponce%'
  order by GISDataSource, ts.name, tt.name,t.City
  
  


END