﻿/*
Author:                       Tatiana Petrov
Create date:                  10/11/2013
Description:                  The logic was provided by Danny Baranowski
Requested By:                 Danny Baranowski 
Used By Crystal Report:		  Yes
Crystal Report Name:		  ServiceSupport_DailyTickets.rpt

Modifications(Date/By Name/Requested By/Description):

*/



CREATE PROCEDURE rptServiceSupport_DailyTickets

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
t.CallID
,ts.name as Status
,t.appointmentstart as AppointmentDay
,tt.name as DispatchCode
,R.Name as Region
,d.name as District
,A.AssignedEngineers

from
w6tasks t (nolock)
Inner Join W6ASSIGNMENTS A (Nolock) on T.W6Key = A.Task
inner join w6task_statuses ts (nolock) on t.status = ts.w6key
inner join w6task_types tt (nolock) on t.tasktype = tt.w6key
inner join w6districts d (nolock) on t.district = d.w6key
inner join W6REGIONS R (nolock) on t.Region = R.W6Key

Where
R.Name not like 'solarUSA'
and ts.Name in ('scheduled','tentative','Acknowledged','Dispatched')
and T.AppointmentStart > GetDate()
and A.AssignedEngineers not like '%2013%'

Order by AppointmentDay



END