﻿-- =============================================
-- Author:		Gabriel Rodriguez
-- Create date: 09/03/2014
-- Description:	Purge incoming log older than 35 days
-- =============================================
CREATE PROCEDURE [dbo].[DeleteIncomingLogsOlderThan35Days] 
	-- Add the parameters for the stored procedure here
	--@p1 int = 0, 
	--@p2 int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
delete W6IM_INCOMING_LOG
where
messagein_time < dateadd(day,datediff(day,35,GETDATE()),0)

    -- Insert statements for procedure here
	--SELECT @p1, @p2
END