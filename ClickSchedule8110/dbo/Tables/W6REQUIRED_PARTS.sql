﻿CREATE TABLE [dbo].[W6REQUIRED_PARTS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [MobileKey]        NVARCHAR (128) NULL,
    [PartsStock]       INT            NULL,
    [Quantity]         INT            NULL,
    [QuantityUnit]     NVARCHAR (64)  NULL,
    [RequiredPartID]   NVARCHAR (256) NULL,
    CONSTRAINT [W6PK_30000016] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000016_4_30000000_] FOREIGN KEY ([PartsStock]) REFERENCES [dbo].[W6PARTS_STOCKS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000016_2__1_0]
    ON [dbo].[W6REQUIRED_PARTS]([TimeCreated] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX30000016_7]
    ON [dbo].[W6REQUIRED_PARTS]([RequiredPartID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000016_4_30000000]
    ON [dbo].[W6REQUIRED_PARTS]([PartsStock] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000016_3]
    ON [dbo].[W6REQUIRED_PARTS]([MobileKey] ASC) WITH (FILLFACTOR = 20);

