﻿CREATE TABLE [dbo].[W6INDEXES] (
    [Collection_ID]          INT            NOT NULL,
    [Index_ID]               INT            NOT NULL,
    [Name]                   VARCHAR (128)  NOT NULL,
    [Context_String]         VARCHAR (1024) NULL,
    [Product_Context_String] VARCHAR (1024) NULL,
    [Is_Product]             INT            NULL,
    PRIMARY KEY CLUSTERED ([Collection_ID] ASC, [Index_ID] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC)
);

