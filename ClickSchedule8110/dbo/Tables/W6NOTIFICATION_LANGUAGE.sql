﻿CREATE TABLE [dbo].[W6NOTIFICATION_LANGUAGE] (
    [W6Key]                  INT            NOT NULL,
    [Revision]               INT            NOT NULL,
    [Name]                   NVARCHAR (64)  NULL,
    [IVR_LongTerm]           NVARCHAR (400) NULL,
    [IVR_MidTerm]            NVARCHAR (400) NULL,
    [IVR_ShortTerm]          NVARCHAR (400) NULL,
    [SMS_LongTerm]           NVARCHAR (400) NULL,
    [SMS_MidTerm]            NVARCHAR (400) NULL,
    [SMS_ShortTerm]          NVARCHAR (400) NULL,
    [Email_LongTerm]         NVARCHAR (400) NULL,
    [Email_MidTerm]          NVARCHAR (400) NULL,
    [Email_ShortTerm]        NVARCHAR (400) NULL,
    [IVRCampaign_LongTerm]   INT            NULL,
    [IVRCampaign_MidTerm]    INT            NULL,
    [IVRCampaign_ShortTerm]  INT            NULL,
    [TokenTag]               NVARCHAR (5)   NULL,
    [EmailSubject_ShortTerm] NVARCHAR (64)  NULL,
    [EmailSubject_MidTerm]   NVARCHAR (64)  NULL,
    [EmailSubject_LongTerm]  NVARCHAR (64)  NULL,
    [EmailFrom]              NVARCHAR (64)  NULL,
    [Stamp_TimeModified]     DATETIME       NULL,
    CONSTRAINT [W6PK_85] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX85_2]
    ON [dbo].[W6NOTIFICATION_LANGUAGE]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W685_UPDATE_CACHED on W6NOTIFICATION_LANGUAGE for insert,update as if update(revision) insert into W6OPERATION_LOG select 85,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W685_DELETE_CACHED on W6NOTIFICATION_LANGUAGE for delete as insert into W6OPERATION_LOG select 85, W6Key, 1, getdate() from deleted