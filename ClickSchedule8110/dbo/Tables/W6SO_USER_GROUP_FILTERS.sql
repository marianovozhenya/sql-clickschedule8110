﻿CREATE TABLE [dbo].[W6SO_USER_GROUP_FILTERS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [OQLFilter]        NVARCHAR (MAX) NULL,
    [FilterID]         NVARCHAR (80)  NULL,
    CONSTRAINT [W6PK_30000204] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000204_4]
    ON [dbo].[W6SO_USER_GROUP_FILTERS]([FilterID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000204_2__4_0]
    ON [dbo].[W6SO_USER_GROUP_FILTERS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W630000204_UPDATE_CACHED on W6SO_USER_GROUP_FILTERS for insert,update as if update(revision) insert into W6OPERATION_LOG select 30000204,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W630000204_DELETE_CACHED on W6SO_USER_GROUP_FILTERS for delete as insert into W6OPERATION_LOG select 30000204, W6Key, 1, getdate() from deleted