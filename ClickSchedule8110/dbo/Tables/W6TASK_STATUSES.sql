﻿CREATE TABLE [dbo].[W6TASK_STATUSES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [UsedInMobile]       INT           NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX51_2]
    ON [dbo].[W6TASK_STATUSES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W651_UPDATE_CACHED on W6TASK_STATUSES for insert,update as if update(revision) insert into W6OPERATION_LOG select 51,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W651_DELETE_CACHED on W6TASK_STATUSES for delete as insert into W6OPERATION_LOG select 51, W6Key, 1, getdate() from deleted