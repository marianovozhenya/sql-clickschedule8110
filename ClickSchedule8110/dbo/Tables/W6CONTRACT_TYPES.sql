﻿CREATE TABLE [dbo].[W6CONTRACT_TYPES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Profile]            NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX52_2]
    ON [dbo].[W6CONTRACT_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W652_UPDATE_CACHED on W6CONTRACT_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 52,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W652_DELETE_CACHED on W6CONTRACT_TYPES for delete as insert into W6OPERATION_LOG select 52, W6Key, 1, getdate() from deleted