﻿CREATE TABLE [dbo].[W6ADDRESSES] (
    [W6Key]               INT            NOT NULL,
    [Revision]            INT            NOT NULL,
    [CreatedBy]           NVARCHAR (128) NOT NULL,
    [TimeCreated]         DATETIME       NOT NULL,
    [CreatingProcess]     INT            NOT NULL,
    [ModifiedBy]          NVARCHAR (128) NOT NULL,
    [TimeModified]        DATETIME       NOT NULL,
    [ModifyingProcess]    INT            NOT NULL,
    [Name]                NVARCHAR (256) NULL,
    [Region]              INT            NULL,
    [District]            INT            NULL,
    [GISDataSource]       INT            NULL,
    [Latitude]            INT            NULL,
    [Longitude]           INT            NULL,
    [Postcode]            NVARCHAR (256) NULL,
    [Country]             INT            NULL,
    [State]               NVARCHAR (256) NULL,
    [City]                NVARCHAR (256) NULL,
    [Street]              NVARCHAR (256) NULL,
    [LocationDescription] NVARCHAR (512) NULL,
    [AddressType]         INT            NULL,
    CONSTRAINT [W6PK_30000007] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000007_10_66_] FOREIGN KEY ([Country]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK30000007_15_10000000_] FOREIGN KEY ([AddressType]) REFERENCES [dbo].[W6ADDRESS_TYPES] ([W6Key]),
    CONSTRAINT [W6FK30000007_4_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK30000007_5_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000007_2__4_0]
    ON [dbo].[W6ADDRESSES]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000007_15_10000000]
    ON [dbo].[W6ADDRESSES]([AddressType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000007_10_66]
    ON [dbo].[W6ADDRESSES]([Country] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000007_5_53]
    ON [dbo].[W6ADDRESSES]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000007_4_50]
    ON [dbo].[W6ADDRESSES]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000007_3]
    ON [dbo].[W6ADDRESSES]([Name] ASC) WITH (FILLFACTOR = 20);

