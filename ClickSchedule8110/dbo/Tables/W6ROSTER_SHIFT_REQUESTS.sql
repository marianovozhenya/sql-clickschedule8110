﻿CREATE TABLE [dbo].[W6ROSTER_SHIFT_REQUESTS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [RosterDemand]     INT             NULL,
    [RosterResource]   INT             NULL,
    [RosterShift]      INT             NULL,
    [ShiftStart]       DATETIME        NULL,
    [ShiftFinish]      DATETIME        NULL,
    [CandidatePolicy]  NVARCHAR (128)  NULL,
    [AllocationData]   NVARCHAR (MAX)  NULL,
    [OpenDate]         DATETIME        NULL,
    [InitiatorLoginID] NVARCHAR (128)  NULL,
    [Status]           INT             NULL,
    [RequestResult]    INT             NULL,
    [ResultReason]     NVARCHAR (1024) NULL,
    CONSTRAINT [W6PK_30000105] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000105_12_10000104_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6SHIFT_REQUEST_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK30000105_4_8_] FOREIGN KEY ([RosterResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000105_4_3]
    ON [dbo].[W6ROSTER_SHIFT_REQUESTS]([RosterResource] ASC, [RosterDemand] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000105_7_0]
    ON [dbo].[W6ROSTER_SHIFT_REQUESTS]([ShiftFinish] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000105_12_10000104]
    ON [dbo].[W6ROSTER_SHIFT_REQUESTS]([Status] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000105_5]
    ON [dbo].[W6ROSTER_SHIFT_REQUESTS]([RosterShift] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000105_4_8]
    ON [dbo].[W6ROSTER_SHIFT_REQUESTS]([RosterResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000105_3]
    ON [dbo].[W6ROSTER_SHIFT_REQUESTS]([RosterDemand] ASC) WITH (FILLFACTOR = 20);

