﻿CREATE TABLE [dbo].[W6ASSIGNMENTS] (
    [W6Key]                 INT             NOT NULL,
    [Revision]              INT             NOT NULL,
    [CreatedBy]             NVARCHAR (128)  NOT NULL,
    [TimeCreated]           DATETIME        NOT NULL,
    [CreatingProcess]       INT             NOT NULL,
    [ModifiedBy]            NVARCHAR (128)  NOT NULL,
    [TimeModified]          DATETIME        NOT NULL,
    [ModifyingProcess]      INT             NOT NULL,
    [Task]                  INT             NULL,
    [StartTime]             DATETIME        NULL,
    [FinishTime]            DATETIME        NULL,
    [CommentText]           NVARCHAR (255)  NULL,
    [Location]              NVARCHAR (64)   NULL,
    [BinaryData]            NVARCHAR (1536) NULL,
    [Latitude]              INT             NULL,
    [Longitude]             INT             NULL,
    [GISDataSource]         INT             NULL,
    [AssignedEngineers]     NVARCHAR (1024) NULL,
    [LogicPolicy]           NVARCHAR (128)  NULL,
    [IsCrewAssignment]      INT             NULL,
    [CountryID]             INT             NULL,
    [Street]                NVARCHAR (64)   NULL,
    [City]                  NVARCHAR (64)   NULL,
    [State]                 NVARCHAR (64)   NULL,
    [Postcode]              NVARCHAR (64)   NULL,
    [NonAvailabilityType]   INT             NULL,
    [IgnoreInRoster]        INT             NULL,
    [ContractorIndex]       INT             NULL,
    [StateSubdivision]      NVARCHAR (64)   NULL,
    [CitySubdivision]       NVARCHAR (64)   NULL,
    [ID]                    NVARCHAR (128)  NULL,
    [WorkAgreementID]       NVARCHAR (128)  NULL,
    [ExternalRefID]         NVARCHAR (128)  NULL,
    [IsBreakIncluded]       INT             NULL,
    [IncludedBreakDuration] INT             NULL,
    [AbsenceRequest]        INT             NULL,
    [ExternalComment]       NVARCHAR (255)  NULL,
    CONSTRAINT [W6PK_0] PRIMARY KEY NONCLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6CNST_MANDATORY_ASSIGNED_ENGS] CHECK ([AssignedEngineers]<>'' AND [AssignedEngineers] IS NOT NULL),
    CONSTRAINT [W6FK0_16_66_] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK0_21_252_] FOREIGN KEY ([NonAvailabilityType]) REFERENCES [dbo].[W6NONAVAILABILITY_TYPES] ([W6Key]),
    CONSTRAINT [W6FK0_3_2_] FOREIGN KEY ([Task]) REFERENCES [dbo].[W6TASKS] ([W6Key]),
    CONSTRAINT [W6FK0_31_30000035_] FOREIGN KEY ([AbsenceRequest]) REFERENCES [dbo].[W6ABSENCE_REQUESTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK0_21_252]
    ON [dbo].[W6ASSIGNMENTS]([NonAvailabilityType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK0_3_2]
    ON [dbo].[W6ASSIGNMENTS]([Task] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX0_5_4_3_0]
    ON [dbo].[W6ASSIGNMENTS]([FinishTime] ASC, [StartTime] ASC, [Task] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX0_3_2__4]
    ON [dbo].[W6ASSIGNMENTS]([Task] ASC, [TimeModified] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX0_2__4]
    ON [dbo].[W6ASSIGNMENTS]([TimeModified] ASC) WITH (FILLFACTOR = 20);


GO
CREATE CLUSTERED INDEX [W6IX0_4]
    ON [dbo].[W6ASSIGNMENTS]([StartTime] ASC);


GO
 CREATE TRIGGER W6TRIGGER_0 ON W6ASSIGNMENTS FOR DELETE AS  DELETE W6ASSIGNMENTS_ENGINEERS FROM W6ASSIGNMENTS_ENGINEERS, deleted WHERE  W6ASSIGNMENTS_ENGINEERS.W6Key = deleted.W6Key