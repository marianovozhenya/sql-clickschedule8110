﻿CREATE TABLE [dbo].[W6CLOCK_REPORTS] (
    [W6Key]           INT            NOT NULL,
    [W6SubKey_1]      INT            NOT NULL,
    [StartTime]       DATETIME       NULL,
    [FinishTime]      DATETIME       NULL,
    [ClockReportType] INT            NULL,
    [CommentText]     NVARCHAR (512) NULL,
    CONSTRAINT [W6PK_270001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK270001_2_258_] FOREIGN KEY ([ClockReportType]) REFERENCES [dbo].[W6CLOCK_REPORT_TYPE] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK270001_2_258]
    ON [dbo].[W6CLOCK_REPORTS]([ClockReportType] ASC) WITH (FILLFACTOR = 50);

