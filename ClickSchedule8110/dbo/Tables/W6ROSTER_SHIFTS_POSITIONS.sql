﻿CREATE TABLE [dbo].[W6ROSTER_SHIFTS_POSITIONS] (
    [W6Key]               INT      NOT NULL,
    [W6SubKey_1]          INT      NOT NULL,
    [StartTime]           DATETIME NULL,
    [FinishTime]          DATETIME NULL,
    [PeriodicType]        INT      NULL,
    [Active]              INT      NULL,
    [PeriodicStart]       INT      NULL,
    [PeriodicFinish]      INT      NULL,
    [OperationalPosition] INT      NULL,
    CONSTRAINT [W6PK_2000001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK2000001_6_209_] FOREIGN KEY ([OperationalPosition]) REFERENCES [dbo].[W6OPERATIONAL_POSITIONS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2000001_6_209]
    ON [dbo].[W6ROSTER_SHIFTS_POSITIONS]([OperationalPosition] ASC) WITH (FILLFACTOR = 50);

