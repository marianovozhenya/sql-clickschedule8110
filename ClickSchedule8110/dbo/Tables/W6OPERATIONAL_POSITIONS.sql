﻿CREATE TABLE [dbo].[W6OPERATIONAL_POSITIONS] (
    [W6Key]                   INT            NOT NULL,
    [Revision]                INT            NOT NULL,
    [CreatedBy]               NVARCHAR (128) NOT NULL,
    [TimeCreated]             DATETIME       NOT NULL,
    [CreatingProcess]         INT            NOT NULL,
    [ModifiedBy]              NVARCHAR (128) NOT NULL,
    [TimeModified]            DATETIME       NOT NULL,
    [ModifyingProcess]        INT            NOT NULL,
    [Name]                    NVARCHAR (64)  NULL,
    [RelatedActivity]         INT            NULL,
    [OperationalPositionType] INT            NULL,
    [Region]                  INT            NULL,
    [District]                INT            NULL,
    CONSTRAINT [W6PK_209] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK209_4_208_] FOREIGN KEY ([RelatedActivity]) REFERENCES [dbo].[W6OPERATIONAL_ACTIVITIES] ([W6Key]),
    CONSTRAINT [W6FK209_5_262_] FOREIGN KEY ([OperationalPositionType]) REFERENCES [dbo].[W6OPERATIONAL_POSITION_TYPES] ([W6Key]),
    CONSTRAINT [W6FK209_6_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK209_7_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK209_7_53]
    ON [dbo].[W6OPERATIONAL_POSITIONS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK209_6_50]
    ON [dbo].[W6OPERATIONAL_POSITIONS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK209_5_262]
    ON [dbo].[W6OPERATIONAL_POSITIONS]([OperationalPositionType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK209_4_208]
    ON [dbo].[W6OPERATIONAL_POSITIONS]([RelatedActivity] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_209 ON W6OPERATIONAL_POSITIONS FOR DELETE AS  DELETE W6OPERATIONAL_POSITIONS_SKILLS FROM W6OPERATIONAL_POSITIONS_SKILLS, deleted WHERE  W6OPERATIONAL_POSITIONS_SKILLS.W6Key = deleted.W6Key