﻿CREATE TABLE [dbo].[W6VACATION_CALENDARS] (
    [W6Key]              INT            NOT NULL,
    [Revision]           INT            NOT NULL,
    [CreatedBy]          NVARCHAR (128) NOT NULL,
    [TimeCreated]        DATETIME       NOT NULL,
    [CreatingProcess]    INT            NOT NULL,
    [ModifiedBy]         NVARCHAR (128) NOT NULL,
    [TimeModified]       DATETIME       NOT NULL,
    [ModifyingProcess]   INT            NOT NULL,
    [Name]               NVARCHAR (256) NULL,
    [Status]             INT            NULL,
    [CalendarStart]      DATETIME       NULL,
    [CalendarFinish]     DATETIME       NULL,
    [TimeUnit]           INT            NULL,
    [TimeUnitsCount]     INT            NULL,
    [Owner]              INT            NULL,
    [IsAutomaticProcess] INT            NULL,
    CONSTRAINT [W6PK_30000210] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000210_11_30000201_] FOREIGN KEY ([Owner]) REFERENCES [dbo].[W6SO_USERS] ([W6Key]),
    CONSTRAINT [W6FK30000210_4_10000210_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6VACATION_CAL_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK30000210_9_10000211_] FOREIGN KEY ([TimeUnit]) REFERENCES [dbo].[W6VACATION_CAL_TIME_UNITS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000210_11_30000201]
    ON [dbo].[W6VACATION_CALENDARS]([Owner] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000210_8_0]
    ON [dbo].[W6VACATION_CALENDARS]([CalendarFinish] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000210_9_10000211]
    ON [dbo].[W6VACATION_CALENDARS]([TimeUnit] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000210_4_10000210]
    ON [dbo].[W6VACATION_CALENDARS]([Status] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX30000210_3]
    ON [dbo].[W6VACATION_CALENDARS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W630000210_UPDATE_CACHED on W6VACATION_CALENDARS for insert,update as if update(revision) insert into W6OPERATION_LOG select 30000210,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W630000210_DELETE_CACHED on W6VACATION_CALENDARS for delete as insert into W6OPERATION_LOG select 30000210, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_30000210 ON W6VACATION_CALENDARS FOR DELETE AS  DELETE W6SO_VACATION_CALS_COUNTS FROM W6SO_VACATION_CALS_COUNTS, deleted WHERE  W6SO_VACATION_CALS_COUNTS.W6Key = deleted.W6Key  DELETE W6SO_VACATION_EMP_SELECTIONS FROM W6SO_VACATION_EMP_SELECTIONS, deleted WHERE  W6SO_VACATION_EMP_SELECTIONS.W6Key = deleted.W6Key