﻿CREATE TABLE [dbo].[W6GIS_CACHE_OLD2] (
    [Origin_Lat]       INT NOT NULL,
    [Origin_Long]      INT NOT NULL,
    [Destination_Lat]  INT NOT NULL,
    [Destination_Long] INT NOT NULL,
    [TravelTime]       INT NULL,
    [Distance]         INT NULL,
    [GISDataSource]    INT NULL,
    [UpdateStatus]     INT NULL,
    PRIMARY KEY CLUSTERED ([Origin_Lat] ASC, [Origin_Long] ASC, [Destination_Lat] ASC, [Destination_Long] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_W6GISCACHENEW_Traveltime]
    ON [dbo].[W6GIS_CACHE_OLD2]([TravelTime] ASC);

