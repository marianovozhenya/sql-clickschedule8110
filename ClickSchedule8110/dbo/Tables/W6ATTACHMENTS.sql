﻿CREATE TABLE [dbo].[W6ATTACHMENTS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [FileName]         NVARCHAR (512) NULL,
    [FileIndex]        INT            NULL,
    [FileSize]         INT            NULL,
    [FileType]         INT            NULL,
    [TimeAttached]     DATETIME       NULL,
    [MobileKey]        NVARCHAR (128) NULL,
    [UploadedBy]       NVARCHAR (128) NULL,
    CONSTRAINT [W6PK_30000200] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000200_6_10000201_] FOREIGN KEY ([FileType]) REFERENCES [dbo].[W6ATTACHMENT_FILE_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000200_7_0]
    ON [dbo].[W6ATTACHMENTS]([TimeAttached] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000200_8]
    ON [dbo].[W6ATTACHMENTS]([MobileKey] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000200_6_10000201]
    ON [dbo].[W6ATTACHMENTS]([FileType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000200_3_4]
    ON [dbo].[W6ATTACHMENTS]([FileName] ASC, [FileIndex] ASC) WITH (FILLFACTOR = 20);

