﻿CREATE TABLE [dbo].[W6CONTRACTS_INFO] (
    [W6Key]                         INT            NOT NULL,
    [Revision]                      INT            NOT NULL,
    [CreatedBy]                     NVARCHAR (128) NOT NULL,
    [TimeCreated]                   DATETIME       NOT NULL,
    [CreatingProcess]               INT            NOT NULL,
    [ModifiedBy]                    NVARCHAR (128) NOT NULL,
    [TimeModified]                  DATETIME       NOT NULL,
    [ModifyingProcess]              INT            NOT NULL,
    [Description]                   NVARCHAR (512) NULL,
    [MaxWrkDay]                     INT            NULL,
    [MaxWrkDayDef]                  INT            NULL,
    [MaxWrkWeek]                    INT            NULL,
    [MaxWrkWeekDef]                 INT            NULL,
    [MinRollRestTime]               INT            NULL,
    [MinRollRestTimeDef]            INT            NULL,
    [MinRestBtwnShifts]             INT            NULL,
    [MinRestBtwnShiftsDef]          INT            NULL,
    [MinRestPrd]                    INT            NULL,
    [MinRestPrdDef]                 INT            NULL,
    [MaxPainShift]                  INT            NULL,
    [MaxPainShiftDef]               INT            NULL,
    [MaxCstveWrkTime]               INT            NULL,
    [MaxCstveWrkTimeDef]            INT            NULL,
    [WrkRestrictDef]                INT            NULL,
    [RestPrd]                       INT            NULL,
    [RestPrdDef]                    INT            NULL,
    [MaximumWorkingDays]            INT            NULL,
    [MaximumWorkingDaysDefined]     INT            NULL,
    [MaximumConsecutiveDays]        INT            NULL,
    [MaximumConsecutiveDaysDefined] INT            NULL,
    [WorkingHoursPerDay]            INT            NULL,
    [WorkingHoursPerDayDefined]     INT            NULL,
    CONSTRAINT [W6PK_113] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
 CREATE TRIGGER W6TRIGGER_113 ON W6CONTRACTS_INFO FOR DELETE AS  DELETE W6CONTRACTS_INFO_RESTRICTIONS FROM W6CONTRACTS_INFO_RESTRICTIONS, deleted WHERE  W6CONTRACTS_INFO_RESTRICTIONS.W6Key = deleted.W6Key