﻿CREATE TABLE [dbo].[W6ROSTER_DEMAND_TEMPLATES] (
    [W6Key]                         INT             NOT NULL,
    [Revision]                      INT             NOT NULL,
    [CreatedBy]                     NVARCHAR (128)  NOT NULL,
    [TimeCreated]                   DATETIME        NOT NULL,
    [CreatingProcess]               INT             NOT NULL,
    [ModifiedBy]                    NVARCHAR (128)  NOT NULL,
    [TimeModified]                  DATETIME        NOT NULL,
    [ModifyingProcess]              INT             NOT NULL,
    [Name]                          NVARCHAR (64)   NULL,
    [Description]                   NVARCHAR (1024) NULL,
    [Region]                        INT             NULL,
    [District]                      INT             NULL,
    [Team]                          INT             NULL,
    [RosterDemandType]              INT             NULL,
    [DemandCategory]                INT             NULL,
    [NumberOfRequiredResources]     INT             NULL,
    [MaxNumberOfRequiredResources]  INT             NULL,
    [OrigNumberOfRequiredResources] INT             NULL,
    [OrigMaxNumOfRequiredResources] INT             NULL,
    [Priority]                      INT             NULL,
    [DisplayColor]                  INT             NULL,
    [StartOffset]                   INT             NULL,
    [Duration]                      INT             NULL,
    [BinaryData]                    NVARCHAR (4000) NULL,
    CONSTRAINT [W6PK_30000100] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000100_5_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK30000100_6_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK30000100_7_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key]),
    CONSTRAINT [W6FK30000100_8_266_] FOREIGN KEY ([RosterDemandType]) REFERENCES [dbo].[W6ROSTER_DEMAND_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000100_7_259]
    ON [dbo].[W6ROSTER_DEMAND_TEMPLATES]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000100_8_266]
    ON [dbo].[W6ROSTER_DEMAND_TEMPLATES]([RosterDemandType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000100_5_50]
    ON [dbo].[W6ROSTER_DEMAND_TEMPLATES]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000100_6_53]
    ON [dbo].[W6ROSTER_DEMAND_TEMPLATES]([District] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_30000100 ON W6ROSTER_DEMAND_TEMPLATES FOR DELETE AS  DELETE W6ROSTER_DMND_TEMPLATES_SKILLS FROM W6ROSTER_DMND_TEMPLATES_SKILLS, deleted WHERE  W6ROSTER_DMND_TEMPLATES_SKILLS.W6Key = deleted.W6Key