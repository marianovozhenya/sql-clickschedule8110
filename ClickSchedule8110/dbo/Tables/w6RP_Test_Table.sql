﻿CREATE TABLE [dbo].[w6RP_Test_Table] (
    [EngineerKey]           INT             NULL,
    [Name]                  NVARCHAR (255)  NULL,
    [StartTime]             DATETIME        NULL,
    [FinishTime]            DATETIME        NULL,
    [CommentText]           NVARCHAR (255)  NULL,
    [NA Reason]             NVARCHAR (64)   NULL,
    [DOW]                   INT             NULL,
    [Duration]              INT             NULL,
    [CallID]                NVARCHAR (64)   NULL,
    [DefaultTravelTime]     INT             NULL,
    [DefaultTravelDistance] DECIMAL (18, 2) NULL,
    [AssignmentKey]         INT             NULL,
    [District]              NVARCHAR (64)   NULL,
    [TravelType]            NCHAR (3)       NULL
);

