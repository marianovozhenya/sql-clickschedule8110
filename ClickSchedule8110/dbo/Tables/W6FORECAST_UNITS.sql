﻿CREATE TABLE [dbo].[W6FORECAST_UNITS] (
    [W6Key]              INT        NOT NULL,
    [Revision]           INT        NOT NULL,
    [RelatedPlan]        INT        NULL,
    [PlanScenario]       INT        NULL,
    [Region]             INT        NULL,
    [District]           INT        NULL,
    [JobType]            INT        NULL,
    [StartTime]          DATETIME   NULL,
    [FinishTime]         DATETIME   NULL,
    [History]            INT        NULL,
    [TotalUnit]          INT        NULL,
    [ParentUnit]         INT        NULL,
    [M1P1]               FLOAT (53) NULL,
    [M1P2]               FLOAT (53) NULL,
    [M1P3]               FLOAT (53) NULL,
    [M1P4]               FLOAT (53) NULL,
    [M1P5]               FLOAT (53) NULL,
    [M1P6]               FLOAT (53) NULL,
    [M2P1]               FLOAT (53) NULL,
    [M2P2]               FLOAT (53) NULL,
    [M2P3]               FLOAT (53) NULL,
    [M2P4]               FLOAT (53) NULL,
    [M2P5]               FLOAT (53) NULL,
    [M2P6]               FLOAT (53) NULL,
    [M3P1]               FLOAT (53) NULL,
    [M3P2]               FLOAT (53) NULL,
    [M3P3]               FLOAT (53) NULL,
    [M3P4]               FLOAT (53) NULL,
    [M3P5]               FLOAT (53) NULL,
    [M3P6]               FLOAT (53) NULL,
    [M4P1]               FLOAT (53) NULL,
    [M4P2]               FLOAT (53) NULL,
    [M4P3]               FLOAT (53) NULL,
    [M4P4]               FLOAT (53) NULL,
    [M4P5]               FLOAT (53) NULL,
    [M4P6]               FLOAT (53) NULL,
    [M5P1]               FLOAT (53) NULL,
    [M5P2]               FLOAT (53) NULL,
    [M5P3]               FLOAT (53) NULL,
    [M5P4]               FLOAT (53) NULL,
    [M5P5]               FLOAT (53) NULL,
    [M5P6]               FLOAT (53) NULL,
    [M6P1]               FLOAT (53) NULL,
    [M6P2]               FLOAT (53) NULL,
    [M6P3]               FLOAT (53) NULL,
    [M6P4]               FLOAT (53) NULL,
    [M6P5]               FLOAT (53) NULL,
    [M6P6]               FLOAT (53) NULL,
    [M7P1]               FLOAT (53) NULL,
    [M7P2]               FLOAT (53) NULL,
    [M7P3]               FLOAT (53) NULL,
    [M7P4]               FLOAT (53) NULL,
    [M7P5]               FLOAT (53) NULL,
    [M7P6]               FLOAT (53) NULL,
    [Stamp_TimeModified] DATETIME   NULL,
    CONSTRAINT [W6PK_5] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK5_2_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK5_3_101_] FOREIGN KEY ([PlanScenario]) REFERENCES [dbo].[W6PLAN_SCENARIOS] ([W6Key]),
    CONSTRAINT [W6FK5_4_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK5_5_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK5_6_56_] FOREIGN KEY ([JobType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX5_2_3_9_0]
    ON [dbo].[W6FORECAST_UNITS]([RelatedPlan] ASC, [PlanScenario] ASC, [History] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK5_11_5]
    ON [dbo].[W6FORECAST_UNITS]([ParentUnit] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK5_6_56]
    ON [dbo].[W6FORECAST_UNITS]([JobType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK5_5_53]
    ON [dbo].[W6FORECAST_UNITS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK5_4_50]
    ON [dbo].[W6FORECAST_UNITS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK5_3_101]
    ON [dbo].[W6FORECAST_UNITS]([PlanScenario] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK5_2_100]
    ON [dbo].[W6FORECAST_UNITS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);

