﻿CREATE TABLE [dbo].[W6SO_USERS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [UserName]         NVARCHAR (64)  NULL,
    [SOUserGroup]      INT            NULL,
    [FullName]         NVARCHAR (64)  NULL,
    [EmailAddress]     NVARCHAR (128) NULL,
    [PhoneNumber]      NVARCHAR (64)  NULL,
    [SORole]           INT            NULL,
    [ReportsTo]        INT            NULL,
    [IsAdministrator]  INT            NULL,
    CONSTRAINT [W6PK_30000201] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000201_10_30000201_] FOREIGN KEY ([ReportsTo]) REFERENCES [dbo].[W6SO_USERS] ([W6Key]),
    CONSTRAINT [W6FK30000201_4_30000203_] FOREIGN KEY ([SOUserGroup]) REFERENCES [dbo].[W6SO_USER_GROUPS] ([W6Key]),
    CONSTRAINT [W6FK30000201_9_10000204_] FOREIGN KEY ([SORole]) REFERENCES [dbo].[W6SO_ROLES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000201_10_30000201]
    ON [dbo].[W6SO_USERS]([ReportsTo] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX30000201_3]
    ON [dbo].[W6SO_USERS]([UserName] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000201_4_30000203]
    ON [dbo].[W6SO_USERS]([SOUserGroup] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000201_9_10000204]
    ON [dbo].[W6SO_USERS]([SORole] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000201_6]
    ON [dbo].[W6SO_USERS]([FullName] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W630000201_UPDATE_CACHED on W6SO_USERS for insert,update as if update(revision) insert into W6OPERATION_LOG select 30000201,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W630000201_DELETE_CACHED on W6SO_USERS for delete as insert into W6OPERATION_LOG select 30000201, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_30000201 ON W6SO_USERS FOR DELETE AS  DELETE W6SO_USER_SO_CRUDS FROM W6SO_USER_SO_CRUDS, deleted WHERE  W6SO_USER_SO_CRUDS.W6Key = deleted.W6Key