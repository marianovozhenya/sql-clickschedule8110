﻿CREATE TABLE [dbo].[W6BUNDLER_POLICIES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_10000220] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX10000220_2]
    ON [dbo].[W6BUNDLER_POLICIES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W610000220_UPDATE_CACHED on W6BUNDLER_POLICIES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000220,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000220_DELETE_CACHED on W6BUNDLER_POLICIES for delete as insert into W6OPERATION_LOG select 10000220, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_10000220 ON W6BUNDLER_POLICIES FOR DELETE AS  DELETE W6BUNDLER_POLICIES_BPPARAMS FROM W6BUNDLER_POLICIES_BPPARAMS, deleted WHERE  W6BUNDLER_POLICIES_BPPARAMS.W6Key = deleted.W6Key