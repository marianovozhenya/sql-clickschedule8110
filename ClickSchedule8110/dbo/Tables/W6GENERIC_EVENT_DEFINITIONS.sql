﻿CREATE TABLE [dbo].[W6GENERIC_EVENT_DEFINITIONS] (
    [W6Key]                INT             NOT NULL,
    [Revision]             INT             NOT NULL,
    [Name]                 NVARCHAR (64)   NULL,
    [Stamp_TimeModified]   DATETIME        NULL,
    [CollectionName]       NVARCHAR (128)  NULL,
    [PreConditionOQL]      NVARCHAR (MAX)  NULL,
    [PostConditionOQL]     NVARCHAR (MAX)  NULL,
    [PreConditionGroup]    INT             NULL,
    [PostConditionGroup]   INT             NULL,
    [Description]          NVARCHAR (1024) NULL,
    [Bundle]               INT             NULL,
    [EvaluationPrecedence] INT             NULL,
    [EventOperation]       INT             NULL,
    [Active]               INT             NULL,
    [UsageType]            INT             NULL,
    CONSTRAINT [W6PK_10000200] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10000200_7_14_] FOREIGN KEY ([PreConditionGroup]) REFERENCES [dbo].[W6GROUPS] ([W6Key]),
    CONSTRAINT [W6FK10000200_8_14_] FOREIGN KEY ([PostConditionGroup]) REFERENCES [dbo].[W6GROUPS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000200_3_11_13_15_12_0_1]
    ON [dbo].[W6GENERIC_EVENT_DEFINITIONS]([CollectionName] ASC, [Bundle] ASC, [EventOperation] ASC, [Active] ASC, [EvaluationPrecedence] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000200_8_14]
    ON [dbo].[W6GENERIC_EVENT_DEFINITIONS]([PostConditionGroup] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000200_7_14]
    ON [dbo].[W6GENERIC_EVENT_DEFINITIONS]([PreConditionGroup] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX10000200_2]
    ON [dbo].[W6GENERIC_EVENT_DEFINITIONS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_10000200 ON W6GENERIC_EVENT_DEFINITIONS FOR DELETE AS  DELETE W6GENERIC_EVENT_DEFS_PROPS FROM W6GENERIC_EVENT_DEFS_PROPS, deleted WHERE  W6GENERIC_EVENT_DEFS_PROPS.W6Key = deleted.W6Key  DELETE W6GENERIC_EVENT_DEFS_USERS FROM W6GENERIC_EVENT_DEFS_USERS, deleted WHERE  W6GENERIC_EVENT_DEFS_USERS.W6Key = deleted.W6Key  DELETE W6GENERIC_EVENT_DEFS_ACTIONS FROM W6GENERIC_EVENT_DEFS_ACTIONS, deleted WHERE  W6GENERIC_EVENT_DEFS_ACTIONS.W6Key = deleted.W6Key