﻿CREATE TABLE [dbo].[W6TIMESHEET] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Engineer]         INT            NULL,
    [StartTime]        DATETIME       NULL,
    [FinishTime]       DATETIME       NULL,
    [Status]           INT            NULL,
    CONSTRAINT [W6PK_27] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK27_3_8_] FOREIGN KEY ([Engineer]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK27_6_257_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6TIMESHEET_STATUS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX27_4_0]
    ON [dbo].[W6TIMESHEET]([StartTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK27_6_257]
    ON [dbo].[W6TIMESHEET]([Status] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK27_3_8]
    ON [dbo].[W6TIMESHEET]([Engineer] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_27 ON W6TIMESHEET FOR DELETE AS  DELETE W6CLOCK_REPORTS FROM W6CLOCK_REPORTS, deleted WHERE  W6CLOCK_REPORTS.W6Key = deleted.W6Key