﻿CREATE TABLE [dbo].[W6RP_REVENUES_AND_EXPENSES] (
    [W6InstanceID]                INT            NOT NULL,
    [W6EntryID]                   INT            NOT NULL,
    [TimeResolution_Start]        DATETIME       NULL,
    [TimeResolution_Finish]       DATETIME       NULL,
    [Region_Key]                  INT            NULL,
    [Region_Name]                 NVARCHAR (256) NULL,
    [District_Key]                INT            NULL,
    [District_Name]               NVARCHAR (256) NULL,
    [Resource_Contractor]         INT            NULL,
    [Resource_CrewForExternalUse] INT            NULL,
    [Resource_ID]                 NVARCHAR (256) NULL,
    [Resource_Internal]           INT            NULL,
    [Resource_Key]                INT            NULL,
    [Resource_Name]               NVARCHAR (256) NULL,
    [ResourceType_Name]           NVARCHAR (256) NULL,
    [Work_Revenue_Value]          FLOAT (53)     NULL,
    [Operational_Cost_Value]      FLOAT (53)     NULL,
    [Labor_Cost_Value]            FLOAT (53)     NULL,
    [Penalties_Cost_Value]        FLOAT (53)     NULL,
    CONSTRAINT [PKRP_REVENUES_AND_EXPENSES] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);

