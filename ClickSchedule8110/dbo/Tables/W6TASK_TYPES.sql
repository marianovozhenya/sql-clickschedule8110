﻿CREATE TABLE [dbo].[W6TASK_TYPES] (
    [W6Key]                  INT           NOT NULL,
    [Revision]               INT           NOT NULL,
    [Name]                   NVARCHAR (64) NULL,
    [Duration]               INT           NULL,
    [TaskRevenue]            FLOAT (53)    NULL,
    [TimeUnit]               INT           NULL,
    [CostPerTimeUnit]        FLOAT (53)    NULL,
    [Category]               INT           NULL,
    [Priority]               INT           NULL,
    [EarlyStartDiff]         INT           NULL,
    [DueDateDiff]            INT           NULL,
    [ScheduleInOptionalTime] INT           NULL,
    [Pinned]                 INT           NULL,
    [Stamp_TimeModified]     DATETIME      NULL,
    [Org]                    INT           NULL,
    [ServiceCode]            INT           NULL,
    CONSTRAINT [W6PK_56] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK56_14_88_] FOREIGN KEY ([Org]) REFERENCES [dbo].[W6ORGANIZATION] ([W6Key]),
    CONSTRAINT [W6FK56_15_89_] FOREIGN KEY ([ServiceCode]) REFERENCES [dbo].[W6SERVICECODE] ([W6Key]),
    CONSTRAINT [W6FK56_8_81_] FOREIGN KEY ([Category]) REFERENCES [dbo].[W6TASKTYPECATEGORY] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK56_15_89]
    ON [dbo].[W6TASK_TYPES]([ServiceCode] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK56_14_88]
    ON [dbo].[W6TASK_TYPES]([Org] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX56_2]
    ON [dbo].[W6TASK_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK56_8_81]
    ON [dbo].[W6TASK_TYPES]([Category] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W656_UPDATE_CACHED on W6TASK_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 56,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W656_DELETE_CACHED on W6TASK_TYPES for delete as insert into W6OPERATION_LOG select 56, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_56 ON W6TASK_TYPES FOR DELETE AS  DELETE W6TASK_TYPES_SKILLS FROM W6TASK_TYPES_SKILLS, deleted WHERE  W6TASK_TYPES_SKILLS.W6Key = deleted.W6Key