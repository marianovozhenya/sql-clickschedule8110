﻿CREATE TABLE [dbo].[W6STATUS_TRANSITIONS] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [StatusBefore]       INT           NULL,
    [StatusAfter]        INT           NULL,
    [StatusContext]      INT           NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_64] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK64_3_51_] FOREIGN KEY ([StatusBefore]) REFERENCES [dbo].[W6TASK_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK64_4_51_] FOREIGN KEY ([StatusAfter]) REFERENCES [dbo].[W6TASK_STATUSES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX64_2]
    ON [dbo].[W6STATUS_TRANSITIONS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK64_4_51]
    ON [dbo].[W6STATUS_TRANSITIONS]([StatusAfter] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK64_3_51]
    ON [dbo].[W6STATUS_TRANSITIONS]([StatusBefore] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_64 ON W6STATUS_TRANSITIONS FOR DELETE AS  DELETE W6STATUS_TRANSITIONS_USERS FROM W6STATUS_TRANSITIONS_USERS, deleted WHERE  W6STATUS_TRANSITIONS_USERS.W6Key = deleted.W6Key
GO
create trigger W664_UPDATE_CACHED on W6STATUS_TRANSITIONS for insert,update as if update(revision) insert into W6OPERATION_LOG select 64,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W664_DELETE_CACHED on W6STATUS_TRANSITIONS for delete as insert into W6OPERATION_LOG select 64, W6Key, 1, getdate() from deleted