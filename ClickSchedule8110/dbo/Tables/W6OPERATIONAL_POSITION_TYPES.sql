﻿CREATE TABLE [dbo].[W6OPERATIONAL_POSITION_TYPES] (
    [W6Key]              INT             NOT NULL,
    [Revision]           INT             NOT NULL,
    [Name]               NVARCHAR (64)   NULL,
    [Description]        NVARCHAR (1024) NULL,
    [Stamp_TimeModified] DATETIME        NULL,
    CONSTRAINT [W6PK_262] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX262_2]
    ON [dbo].[W6OPERATIONAL_POSITION_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_262 ON W6OPERATIONAL_POSITION_TYPES FOR DELETE AS  DELETE W6OPERATIONAL_POS_TYPES_SKILLS FROM W6OPERATIONAL_POS_TYPES_SKILLS, deleted WHERE  W6OPERATIONAL_POS_TYPES_SKILLS.W6Key = deleted.W6Key
GO
create trigger W6262_UPDATE_CACHED on W6OPERATIONAL_POSITION_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 262,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W6262_DELETE_CACHED on W6OPERATIONAL_POSITION_TYPES for delete as insert into W6OPERATION_LOG select 262, W6Key, 1, getdate() from deleted