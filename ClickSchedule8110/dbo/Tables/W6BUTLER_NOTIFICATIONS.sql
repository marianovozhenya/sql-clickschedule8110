﻿CREATE TABLE [dbo].[W6BUTLER_NOTIFICATIONS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [MobileKey]        NVARCHAR (64)  NULL,
    [ButlerType]       INT            NULL,
    [EngineerLoginID]  NVARCHAR (64)  NULL,
    [OpenDate]         DATETIME       NULL,
    [Description]      NVARCHAR (MAX) NULL,
    CONSTRAINT [W6PK_30000033] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000033_4_10000030_] FOREIGN KEY ([ButlerType]) REFERENCES [dbo].[W6BUTLER_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000033_6_0]
    ON [dbo].[W6BUTLER_NOTIFICATIONS]([OpenDate] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000033_3]
    ON [dbo].[W6BUTLER_NOTIFICATIONS]([MobileKey] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000033_5]
    ON [dbo].[W6BUTLER_NOTIFICATIONS]([EngineerLoginID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000033_4_10000030]
    ON [dbo].[W6BUTLER_NOTIFICATIONS]([ButlerType] ASC) WITH (FILLFACTOR = 50);

