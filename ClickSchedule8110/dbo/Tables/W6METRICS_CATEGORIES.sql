﻿CREATE TABLE [dbo].[W6METRICS_CATEGORIES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_67] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX67_2]
    ON [dbo].[W6METRICS_CATEGORIES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W667_UPDATE_CACHED on W6METRICS_CATEGORIES for insert,update as if update(revision) insert into W6OPERATION_LOG select 67,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W667_DELETE_CACHED on W6METRICS_CATEGORIES for delete as insert into W6OPERATION_LOG select 67, W6Key, 1, getdate() from deleted