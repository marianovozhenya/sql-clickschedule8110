﻿CREATE TABLE [dbo].[W6SPECIAL_DAYS_TYPES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Calendar]           INT           NULL,
    [Region]             INT           NULL,
    [District]           INT           NULL,
    [Team]               INT           NULL,
    [ResourcesGroup]     INT           NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_10000105] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10000105_3_3_] FOREIGN KEY ([Calendar]) REFERENCES [dbo].[W6CALENDARS] ([W6Key]),
    CONSTRAINT [W6FK10000105_4_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK10000105_5_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK10000105_6_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key]),
    CONSTRAINT [W6FK10000105_7_14_] FOREIGN KEY ([ResourcesGroup]) REFERENCES [dbo].[W6GROUPS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000105_6_259]
    ON [dbo].[W6SPECIAL_DAYS_TYPES]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000105_7_14]
    ON [dbo].[W6SPECIAL_DAYS_TYPES]([ResourcesGroup] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000105_4_50]
    ON [dbo].[W6SPECIAL_DAYS_TYPES]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX10000105_2]
    ON [dbo].[W6SPECIAL_DAYS_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000105_5_53]
    ON [dbo].[W6SPECIAL_DAYS_TYPES]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000105_3_3]
    ON [dbo].[W6SPECIAL_DAYS_TYPES]([Calendar] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W610000105_UPDATE_CACHED on W6SPECIAL_DAYS_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000105,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000105_DELETE_CACHED on W6SPECIAL_DAYS_TYPES for delete as insert into W6OPERATION_LOG select 10000105, W6Key, 1, getdate() from deleted