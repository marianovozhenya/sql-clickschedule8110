﻿CREATE TABLE [dbo].[W6PARTS_IN_STOCK] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (256)  NULL,
    [ExternalRefID]    NVARCHAR (128)  NULL,
    [PartType]         INT             NULL,
    [Description]      NVARCHAR (1024) NULL,
    [QuantityUnit]     NVARCHAR (32)   NULL,
    [PartsStock]       INT             NULL,
    [Quantity]         FLOAT (53)      NULL,
    [MobileKey]        NVARCHAR (128)  NULL,
    [Used]             INT             NULL,
    [PartLocation]     INT             NULL,
    CONSTRAINT [W6PK_30000001] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000001_12_30000014_] FOREIGN KEY ([PartLocation]) REFERENCES [dbo].[W6PART_LOCATIONS] ([W6Key]),
    CONSTRAINT [W6FK30000001_5_10000002_] FOREIGN KEY ([PartType]) REFERENCES [dbo].[W6PART_TYPES] ([W6Key]),
    CONSTRAINT [W6FK30000001_8_30000000_] FOREIGN KEY ([PartsStock]) REFERENCES [dbo].[W6PARTS_STOCKS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000001_10]
    ON [dbo].[W6PARTS_IN_STOCK]([MobileKey] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000001_12_30000014]
    ON [dbo].[W6PARTS_IN_STOCK]([PartLocation] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000001_2__4_0]
    ON [dbo].[W6PARTS_IN_STOCK]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000001_8_30000000]
    ON [dbo].[W6PARTS_IN_STOCK]([PartsStock] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000001_5_10000002]
    ON [dbo].[W6PARTS_IN_STOCK]([PartType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000001_4]
    ON [dbo].[W6PARTS_IN_STOCK]([ExternalRefID] ASC) WITH (FILLFACTOR = 20);

