﻿CREATE TABLE [dbo].[W6LOCATIONS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (256)  NULL,
    [LocationID]       NVARCHAR (128)  NULL,
    [BinaryData]       NVARCHAR (4000) NULL,
    [Active]           INT             NULL,
    [TimeZone]         NVARCHAR (64)   NULL,
    [Description]      NVARCHAR (1024) NULL,
    [ExternalRefID]    NVARCHAR (128)  NULL,
    CONSTRAINT [W6PK_30000009] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000009_2__4_0]
    ON [dbo].[W6LOCATIONS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000009_4]
    ON [dbo].[W6LOCATIONS]([LocationID] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_30000009 ON W6LOCATIONS FOR DELETE AS  DELETE W6LOCATIONS_ADDRESSES FROM W6LOCATIONS_ADDRESSES, deleted WHERE  W6LOCATIONS_ADDRESSES.W6Key = deleted.W6Key  DELETE W6LOCATIONS_ASSETS FROM W6LOCATIONS_ASSETS, deleted WHERE  W6LOCATIONS_ASSETS.W6Key = deleted.W6Key