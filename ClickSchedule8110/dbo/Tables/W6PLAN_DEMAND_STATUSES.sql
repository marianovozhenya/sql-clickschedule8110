﻿CREATE TABLE [dbo].[W6PLAN_DEMAND_STATUSES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX61_2]
    ON [dbo].[W6PLAN_DEMAND_STATUSES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W661_UPDATE_CACHED on W6PLAN_DEMAND_STATUSES for insert,update as if update(revision) insert into W6OPERATION_LOG select 61,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W661_DELETE_CACHED on W6PLAN_DEMAND_STATUSES for delete as insert into W6OPERATION_LOG select 61, W6Key, 1, getdate() from deleted