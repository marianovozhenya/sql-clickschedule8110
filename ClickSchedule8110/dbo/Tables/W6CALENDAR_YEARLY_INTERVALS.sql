﻿CREATE TABLE [dbo].[W6CALENDAR_YEARLY_INTERVALS] (
    [W6Key]       INT      NOT NULL,
    [W6SubKey_1]  INT      NOT NULL,
    [Start_Time]  DATETIME NULL,
    [Finish_Time] DATETIME NULL,
    [Status]      INT      NOT NULL,
    CONSTRAINT [W6PK_30001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

