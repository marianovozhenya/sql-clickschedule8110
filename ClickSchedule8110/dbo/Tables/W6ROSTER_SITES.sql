﻿CREATE TABLE [dbo].[W6ROSTER_SITES] (
    [W6Key]                   INT           NOT NULL,
    [Revision]                INT           NOT NULL,
    [Name]                    NVARCHAR (64) NULL,
    [Region]                  INT           NULL,
    [District]                INT           NULL,
    [Team]                    INT           NULL,
    [GeneralSettingsTemplate] INT           NULL,
    [Stamp_TimeModified]      DATETIME      NULL,
    CONSTRAINT [W6PK_10000100] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10000100_3_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK10000100_4_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK10000100_5_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key]),
    CONSTRAINT [W6FK10000100_6_15_] FOREIGN KEY ([GeneralSettingsTemplate]) REFERENCES [dbo].[W6SETTINGS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000100_5_259]
    ON [dbo].[W6ROSTER_SITES]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000100_3_50]
    ON [dbo].[W6ROSTER_SITES]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX10000100_2]
    ON [dbo].[W6ROSTER_SITES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000100_6_15]
    ON [dbo].[W6ROSTER_SITES]([GeneralSettingsTemplate] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000100_4_53]
    ON [dbo].[W6ROSTER_SITES]([District] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W610000100_UPDATE_CACHED on W6ROSTER_SITES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000100,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000100_DELETE_CACHED on W6ROSTER_SITES for delete as insert into W6OPERATION_LOG select 10000100, W6Key, 1, getdate() from deleted