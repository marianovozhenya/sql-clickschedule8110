﻿CREATE TABLE [dbo].[W6ROSTER_RESOURCE_PREFERENCES] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [RelatedResource]  INT            NULL,
    [StartTime]        DATETIME       NULL,
    [FinishTime]       DATETIME       NULL,
    [PreferenceMode]   INT            NULL,
    [ContentType]      INT            NULL,
    [AvailabilityType] INT            NULL,
    [CommentText]      NVARCHAR (256) NULL,
    CONSTRAINT [W6PK_30000102] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000102_3_8_] FOREIGN KEY ([RelatedResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK30000102_8_68_] FOREIGN KEY ([AvailabilityType]) REFERENCES [dbo].[W6AVAILABILITY_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000102_8_68]
    ON [dbo].[W6ROSTER_RESOURCE_PREFERENCES]([AvailabilityType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000102_3_7_4_0_1]
    ON [dbo].[W6ROSTER_RESOURCE_PREFERENCES]([RelatedResource] ASC, [ContentType] ASC, [StartTime] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000102_3_8]
    ON [dbo].[W6ROSTER_RESOURCE_PREFERENCES]([RelatedResource] ASC) WITH (FILLFACTOR = 50);

