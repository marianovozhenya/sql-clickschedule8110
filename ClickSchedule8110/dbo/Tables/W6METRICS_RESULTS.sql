﻿CREATE TABLE [dbo].[W6METRICS_RESULTS] (
    [W6Key]              INT            NOT NULL,
    [Revision]           INT            NOT NULL,
    [SectionPath]        NVARCHAR (256) NULL,
    [HorizonStart]       DATETIME       NULL,
    [HorizonFinish]      DATETIME       NULL,
    [CalculationTime]    DATETIME       NULL,
    [AgentID]            NVARCHAR (256) NULL,
    [MostRecent]         INT            NULL,
    [Results]            NVARCHAR (MAX) NULL,
    [Stamp_TimeModified] DATETIME       NULL,
    CONSTRAINT [W6PK_19] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX19_2_7_6_0_1]
    ON [dbo].[W6METRICS_RESULTS]([SectionPath] ASC, [MostRecent] ASC, [AgentID] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX19_4_0]
    ON [dbo].[W6METRICS_RESULTS]([HorizonFinish] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);

