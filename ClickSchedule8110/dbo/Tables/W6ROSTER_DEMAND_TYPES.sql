﻿CREATE TABLE [dbo].[W6ROSTER_DEMAND_TYPES] (
    [W6Key]              INT             NOT NULL,
    [Revision]           INT             NOT NULL,
    [Name]               NVARCHAR (64)   NULL,
    [Region]             INT             NULL,
    [District]           INT             NULL,
    [Team]               INT             NULL,
    [BinaryData]         NVARCHAR (4000) NULL,
    [Stamp_TimeModified] DATETIME        NULL,
    CONSTRAINT [W6PK_266] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK266_3_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK266_4_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK266_5_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX266_2]
    ON [dbo].[W6ROSTER_DEMAND_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK266_5_259]
    ON [dbo].[W6ROSTER_DEMAND_TYPES]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK266_4_53]
    ON [dbo].[W6ROSTER_DEMAND_TYPES]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK266_3_50]
    ON [dbo].[W6ROSTER_DEMAND_TYPES]([Region] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_266 ON W6ROSTER_DEMAND_TYPES FOR DELETE AS  DELETE W6ROSTER_DEMAND_TYPES_SKILLS FROM W6ROSTER_DEMAND_TYPES_SKILLS, deleted WHERE  W6ROSTER_DEMAND_TYPES_SKILLS.W6Key = deleted.W6Key