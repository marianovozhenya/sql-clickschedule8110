﻿CREATE TABLE [dbo].[W6APP_MODULES] (
    [W6Key]                      INT             NOT NULL,
    [W6SubKey_1]                 INT             NOT NULL,
    [Name]                       NVARCHAR (128)  NULL,
    [MobileItemType]             INT             NULL,
    [AdminFile]                  NVARCHAR (256)  NULL,
    [AdminClass]                 NVARCHAR (256)  NULL,
    [RuntimeFile]                NVARCHAR (256)  NULL,
    [RuntimeClass]               NVARCHAR (256)  NULL,
    [AppSettings]                NVARCHAR (MAX)  NULL,
    [Icon]                       NVARCHAR (1024) NULL,
    [MobileAdditionalProperties] NVARCHAR (MAX)  NULL,
    [MobileDictionaries]         NVARCHAR (MAX)  NULL,
    [AppSpecificProperties]      NVARCHAR (MAX)  NULL,
    CONSTRAINT [W6PK_50000253] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK50000253_1_20000003_] FOREIGN KEY ([MobileItemType]) REFERENCES [dbo].[W6MOBILEAPPTYPE] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50000253_1_20000003]
    ON [dbo].[W6APP_MODULES]([MobileItemType] ASC) WITH (FILLFACTOR = 50);

