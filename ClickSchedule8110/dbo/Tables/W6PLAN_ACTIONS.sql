﻿CREATE TABLE [dbo].[W6PLAN_ACTIONS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [RelatedPlan]      INT             NULL,
    [PlanScenario]     INT             NULL,
    [Implementor]      NVARCHAR (64)   NULL,
    [ObjectType]       INT             NULL,
    [Category]         INT             NULL,
    [ParentResource]   INT             NULL,
    [ParentDemand]     INT             NULL,
    [Body]             NVARCHAR (4000) NULL,
    [VirtualKey]       INT             NULL,
    [Description]      NVARCHAR (512)  NULL,
    [Context]          INT             NULL,
    CONSTRAINT [W6PK_102] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK102_3_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK102_4_101_] FOREIGN KEY ([PlanScenario]) REFERENCES [dbo].[W6PLAN_SCENARIOS] ([W6Key]),
    CONSTRAINT [W6FK102_8_8_] FOREIGN KEY ([ParentResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK102_9_4_] FOREIGN KEY ([ParentDemand]) REFERENCES [dbo].[W6PLAN_DEMANDS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK102_9_4]
    ON [dbo].[W6PLAN_ACTIONS]([ParentDemand] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK102_8_8]
    ON [dbo].[W6PLAN_ACTIONS]([ParentResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK102_4_101]
    ON [dbo].[W6PLAN_ACTIONS]([PlanScenario] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK102_3_100]
    ON [dbo].[W6PLAN_ACTIONS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_102 ON W6PLAN_ACTIONS FOR DELETE AS  DELETE W6PLAN_ACTIONS_EXTRABODY FROM W6PLAN_ACTIONS_EXTRABODY, deleted WHERE  W6PLAN_ACTIONS_EXTRABODY.W6Key = deleted.W6Key