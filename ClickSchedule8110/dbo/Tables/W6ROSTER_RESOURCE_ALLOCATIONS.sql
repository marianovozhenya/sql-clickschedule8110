﻿CREATE TABLE [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS] (
    [W6Key]                       INT             NOT NULL,
    [Revision]                    INT             NOT NULL,
    [CreatedBy]                   NVARCHAR (128)  NOT NULL,
    [TimeCreated]                 DATETIME        NOT NULL,
    [CreatingProcess]             INT             NOT NULL,
    [ModifiedBy]                  NVARCHAR (128)  NOT NULL,
    [TimeModified]                DATETIME        NOT NULL,
    [ModifyingProcess]            INT             NOT NULL,
    [AllocatedResource]           INT             NULL,
    [StartTime]                   DATETIME        NULL,
    [FinishTime]                  DATETIME        NULL,
    [AllocationType]              INT             NULL,
    [Region]                      INT             NULL,
    [District]                    INT             NULL,
    [Team]                        INT             NULL,
    [DestinationResource]         INT             NULL,
    [JobPosition]                 INT             NULL,
    [WorkAgreementID]             NVARCHAR (128)  NULL,
    [OperationalActivity]         INT             NULL,
    [Description]                 NVARCHAR (1024) NULL,
    [ActivityNonAvailabilityType] INT             NULL,
    CONSTRAINT [W6PK_201] PRIMARY KEY NONCLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK201_10_8_] FOREIGN KEY ([DestinationResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK201_11_207_] FOREIGN KEY ([JobPosition]) REFERENCES [dbo].[W6JOB_POSITIONS] ([W6Key]),
    CONSTRAINT [W6FK201_13_208_] FOREIGN KEY ([OperationalActivity]) REFERENCES [dbo].[W6OPERATIONAL_ACTIVITIES] ([W6Key]),
    CONSTRAINT [W6FK201_15_265_] FOREIGN KEY ([ActivityNonAvailabilityType]) REFERENCES [dbo].[W6ACTIVITY_NA_TYPES] ([W6Key]),
    CONSTRAINT [W6FK201_3_8_] FOREIGN KEY ([AllocatedResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK201_7_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK201_8_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK201_9_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_15_265]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([ActivityNonAvailabilityType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_13_208]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([OperationalActivity] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX201_5_0]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([FinishTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_11_207]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([JobPosition] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_10_8]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([DestinationResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_9_259]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_8_53]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_7_50]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK201_3_8]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([AllocatedResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE CLUSTERED INDEX [W6IX201_4_3]
    ON [dbo].[W6ROSTER_RESOURCE_ALLOCATIONS]([StartTime] ASC, [AllocatedResource] ASC);

