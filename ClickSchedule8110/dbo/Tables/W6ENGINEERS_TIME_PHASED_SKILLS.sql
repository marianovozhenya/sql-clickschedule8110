﻿CREATE TABLE [dbo].[W6ENGINEERS_TIME_PHASED_SKILLS] (
    [W6Key]          INT      NOT NULL,
    [W6SubKey_1]     INT      NOT NULL,
    [StartTime]      DATETIME NULL,
    [FinishTime]     DATETIME NULL,
    [PeriodicType]   INT      NULL,
    [Active]         INT      NULL,
    [PeriodicStart]  INT      NULL,
    [PeriodicFinish] INT      NULL,
    [SkillKey]       INT      NULL,
    [SkillLevel]     INT      NULL,
    CONSTRAINT [W6PK_80006] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK80006_6_58_] FOREIGN KEY ([SkillKey]) REFERENCES [dbo].[W6SKILLS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK80006_6_58]
    ON [dbo].[W6ENGINEERS_TIME_PHASED_SKILLS]([SkillKey] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX80006_1_2_K]
    ON [dbo].[W6ENGINEERS_TIME_PHASED_SKILLS]([FinishTime] ASC, [PeriodicType] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);

