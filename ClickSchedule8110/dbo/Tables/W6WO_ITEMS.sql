﻿CREATE TABLE [dbo].[W6WO_ITEMS] (
    [W6Key]               INT             NOT NULL,
    [Revision]            INT             NOT NULL,
    [CreatedBy]           NVARCHAR (128)  NOT NULL,
    [TimeCreated]         DATETIME        NOT NULL,
    [CreatingProcess]     INT             NOT NULL,
    [ModifiedBy]          NVARCHAR (128)  NOT NULL,
    [TimeModified]        DATETIME        NOT NULL,
    [ModifyingProcess]    INT             NOT NULL,
    [Name]                NVARCHAR (256)  NULL,
    [BinaryData]          NVARCHAR (4000) NULL,
    [AppointmentStart]    DATETIME        NULL,
    [AppointmentFinish]   DATETIME        NULL,
    [Comments]            NVARCHAR (256)  NULL,
    [ConfirmationDate]    DATETIME        NULL,
    [ContactDate]         DATETIME        NULL,
    [Critical]            INT             NULL,
    [Description]         NVARCHAR (1024) NULL,
    [DueDate]             DATETIME        NULL,
    [Duration]            INT             NULL,
    [EarlyStart]          DATETIME        NULL,
    [LateStart]           DATETIME        NULL,
    [OpenDate]            DATETIME        NULL,
    [Priority]            INT             NULL,
    [RequiredCrewSize]    INT             NULL,
    [Status]              INT             NULL,
    [WorkOrderItemID]     NVARCHAR (128)  NULL,
    [WorkOrderItemNumber] INT             NULL,
    [WorkOrderItemType]   INT             NULL,
    [IsScheduled]         INT             NULL,
    [TaskCallID]          NVARCHAR (64)   NULL,
    [TaskNumber]          INT             NULL,
    CONSTRAINT [W6PK_30000011] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000011_25_51_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6TASK_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK30000011_29_56_] FOREIGN KEY ([WorkOrderItemType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000011_31_32]
    ON [dbo].[W6WO_ITEMS]([TaskCallID] ASC, [TaskNumber] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000011_7_0]
    ON [dbo].[W6WO_ITEMS]([AppointmentFinish] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000011_29_56]
    ON [dbo].[W6WO_ITEMS]([WorkOrderItemType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000011_25_51]
    ON [dbo].[W6WO_ITEMS]([Status] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000011_27_28]
    ON [dbo].[W6WO_ITEMS]([WorkOrderItemID] ASC, [WorkOrderItemNumber] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_30000011 ON W6WO_ITEMS FOR DELETE AS  DELETE W6WO_ITEMS_ADDRESSES FROM W6WO_ITEMS_ADDRESSES, deleted WHERE  W6WO_ITEMS_ADDRESSES.W6Key = deleted.W6Key  DELETE W6WO_ITEMS_EXCLUDED_ENGINEERS FROM W6WO_ITEMS_EXCLUDED_ENGINEERS, deleted WHERE  W6WO_ITEMS_EXCLUDED_ENGINEERS.W6Key = deleted.W6Key  DELETE W6WO_ITEMS_PREFERRED_ENGINEERS FROM W6WO_ITEMS_PREFERRED_ENGINEERS, deleted WHERE  W6WO_ITEMS_PREFERRED_ENGINEERS.W6Key = deleted.W6Key  DELETE W6WO_ITEMS_REQUIRED_ENGINEERS FROM W6WO_ITEMS_REQUIRED_ENGINEERS, deleted WHERE  W6WO_ITEMS_REQUIRED_ENGINEERS.W6Key = deleted.W6Key  DELETE W6WO_ITEMS_REQUIRED_TOOLS FROM W6WO_ITEMS_REQUIRED_TOOLS, deleted WHERE  W6WO_ITEMS_REQUIRED_TOOLS.W6Key = deleted.W6Key  DELETE W6WO_ITEMS_REQUIRED_SKILLS FROM W6WO_ITEMS_REQUIRED_SKILLS, deleted WHERE  W6WO_ITEMS_REQUIRED_SKILLS.W6Key = deleted.W6Key  DELETE W6WO_ITEMS_STEPS FROM W6WO_ITEMS_STEPS, deleted WHERE  W6WO_ITEMS_STEPS.W6Key = deleted.W6Key