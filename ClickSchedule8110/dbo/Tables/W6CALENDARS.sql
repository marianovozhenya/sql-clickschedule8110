﻿CREATE TABLE [dbo].[W6CALENDARS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Name]             NVARCHAR (128) NULL,
    [Base_Calendar]    INT            NULL,
    [Time_Zone_Name]   NVARCHAR (128) NULL,
    [HasBase]          INT            NOT NULL,
    [SpecialDays]      INT            NULL,
    CONSTRAINT [W6PK_3] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK3_4_3_] FOREIGN KEY ([Base_Calendar]) REFERENCES [dbo].[W6CALENDARS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK3_4_3]
    ON [dbo].[W6CALENDARS]([Base_Calendar] ASC) WITH (FILLFACTOR = 20);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX3_3U]
    ON [dbo].[W6CALENDARS]([Name] ASC);


GO
create trigger W63_UPDATE_CACHED on W6CALENDARS for insert,update as if update(revision) insert into W6OPERATION_LOG select 3,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W63_DELETE_CACHED on W6CALENDARS for delete as insert into W6OPERATION_LOG select 3, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_3 ON W6CALENDARS FOR DELETE AS  DELETE W6CALENDAR_YEARLY_INTERVALS FROM W6CALENDAR_YEARLY_INTERVALS, deleted WHERE  W6CALENDAR_YEARLY_INTERVALS.W6Key = deleted.W6Key  DELETE W6CALENDAR_WEEKLY_INTERVALS FROM W6CALENDAR_WEEKLY_INTERVALS, deleted WHERE  W6CALENDAR_WEEKLY_INTERVALS.W6Key = deleted.W6Key  DELETE W6CALENDAR_YEARLY_SHIFTS FROM W6CALENDAR_YEARLY_SHIFTS, deleted WHERE  W6CALENDAR_YEARLY_SHIFTS.W6Key = deleted.W6Key  DELETE W6CALENDAR_WEEKLY_SHIFTS FROM W6CALENDAR_WEEKLY_SHIFTS, deleted WHERE  W6CALENDAR_WEEKLY_SHIFTS.W6Key = deleted.W6Key  DELETE W6CALENDAR_PHASED_INTERVALS FROM W6CALENDAR_PHASED_INTERVALS, deleted WHERE  W6CALENDAR_PHASED_INTERVALS.W6Key = deleted.W6Key