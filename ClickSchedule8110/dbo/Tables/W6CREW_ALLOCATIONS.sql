﻿CREATE TABLE [dbo].[W6CREW_ALLOCATIONS] (
    [W6Key]                INT            NOT NULL,
    [Revision]             INT            NOT NULL,
    [CreatedBy]            NVARCHAR (128) NOT NULL,
    [TimeCreated]          DATETIME       NOT NULL,
    [CreatingProcess]      INT            NOT NULL,
    [ModifiedBy]           NVARCHAR (128) NOT NULL,
    [TimeModified]         DATETIME       NOT NULL,
    [ModifyingProcess]     INT            NOT NULL,
    [Crew]                 INT            NULL,
    [StartTime]            DATETIME       NULL,
    [FinishTime]           DATETIME       NULL,
    [AllocatedResource]    INT            NULL,
    [ContinueFromHomeBase] INT            NULL,
    [Critical]             INT            NULL,
    [Relocation]           INT            NULL,
    CONSTRAINT [W6PK_23] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK23_3_8_] FOREIGN KEY ([Crew]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK23_6_8_] FOREIGN KEY ([AllocatedResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX23_5_0]
    ON [dbo].[W6CREW_ALLOCATIONS]([FinishTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK23_6_8]
    ON [dbo].[W6CREW_ALLOCATIONS]([AllocatedResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK23_3_8]
    ON [dbo].[W6CREW_ALLOCATIONS]([Crew] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W623_UPDATE_CACHED on W6CREW_ALLOCATIONS for insert,update as if update(revision) insert into W6OPERATION_LOG select 23,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W623_DELETE_CACHED on W6CREW_ALLOCATIONS for delete as insert into W6OPERATION_LOG select 23, W6Key, 1, getdate() from deleted