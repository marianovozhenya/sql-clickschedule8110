﻿CREATE TABLE [dbo].[W6MEASUREMENT_DOCUMENTS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Name]             NVARCHAR (256) NULL,
    [ExternalRefID]    NVARCHAR (128) NULL,
    [Reading]          FLOAT (53)     NULL,
    CONSTRAINT [W6PK_30000004] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000004_2__4_0]
    ON [dbo].[W6MEASUREMENT_DOCUMENTS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000004_4]
    ON [dbo].[W6MEASUREMENT_DOCUMENTS]([ExternalRefID] ASC) WITH (FILLFACTOR = 20);

