﻿CREATE TABLE [dbo].[W6RP_RESOURCE_UTILIZATION] (
    [W6InstanceID]                INT            NOT NULL,
    [W6EntryID]                   INT            NOT NULL,
    [TimeResolution_Start]        DATETIME       NULL,
    [TimeResolution_Finish]       DATETIME       NULL,
    [Region_Key]                  INT            NULL,
    [Region_Name]                 NVARCHAR (256) NULL,
    [District_Key]                INT            NULL,
    [District_Name]               NVARCHAR (256) NULL,
    [Resource_Contractor]         INT            NULL,
    [Resource_CrewForExternalUse] INT            NULL,
    [Resource_ID]                 NVARCHAR (256) NULL,
    [Resource_Internal]           INT            NULL,
    [Resource_Key]                INT            NULL,
    [Resource_Name]               NVARCHAR (256) NULL,
    [ResourceType_Name]           NVARCHAR (256) NULL,
    [Resource_AvailableTime]      INT            NULL,
    [Resource_WorkEffort]         INT            NULL,
    [Resource_NAsDuration]        INT            NULL,
    [Resource_LunchDuration]      INT            NULL,
    [Resource_IdleTime]           INT            NULL,
    [Resource_Utilization]        FLOAT (53)     NULL,
    [Resource_OvertimeWorkeffort] INT            NULL,
    CONSTRAINT [PK_RESOURCE_UTILIZATION] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION1]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION]([TimeResolution_Start] ASC, [W6InstanceID] ASC, [Region_Key] ASC, [District_Key] ASC, [Resource_Key] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION11]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION]([Resource_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION5]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION]([District_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION3]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION]([Region_Key] ASC, [W6InstanceID] ASC);

