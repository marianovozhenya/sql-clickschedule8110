﻿CREATE TABLE [dbo].[W6OFFLINE_OPERATIONS] (
    [W6Key]           INT             NOT NULL,
    [W6SubKey_1]      INT             NOT NULL,
    [ProcessingOrder] INT             NULL,
    [Name]            NVARCHAR (64)   NULL,
    [Description]     NVARCHAR (512)  NULL,
    [Enabled]         INT             NULL,
    [ContinueOnError] INT             NULL,
    [Status]          INT             NULL,
    [Body]            NVARCHAR (MAX)  NULL,
    [Result]          NVARCHAR (4000) NULL,
    [ProcessStart]    DATETIME        NULL,
    [ProcessFinish]   DATETIME        NULL,
    CONSTRAINT [W6PK_260001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

