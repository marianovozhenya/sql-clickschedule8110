﻿CREATE TABLE [dbo].[W6SHIFT_TEMPLATES] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (64)   NULL,
    [Offset]           INT             NULL,
    [Duration]         INT             NULL,
    [BreakDuration]    INT             NULL,
    [Description]      NVARCHAR (1024) NULL,
    [Active]           INT             NULL,
    [DisplayColor]     INT             NULL,
    [AvailabilityType] INT             NULL,
    [Region]           INT             NULL,
    [District]         INT             NULL,
    [Team]             INT             NULL,
    CONSTRAINT [W6PK_114] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK114_10_68_] FOREIGN KEY ([AvailabilityType]) REFERENCES [dbo].[W6AVAILABILITY_TYPES] ([W6Key]),
    CONSTRAINT [W6FK114_11_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK114_12_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK114_13_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK114_13_259]
    ON [dbo].[W6SHIFT_TEMPLATES]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK114_12_53]
    ON [dbo].[W6SHIFT_TEMPLATES]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK114_11_50]
    ON [dbo].[W6SHIFT_TEMPLATES]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK114_10_68]
    ON [dbo].[W6SHIFT_TEMPLATES]([AvailabilityType] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W6114_UPDATE_CACHED on W6SHIFT_TEMPLATES for insert,update as if update(revision) insert into W6OPERATION_LOG select 114,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W6114_DELETE_CACHED on W6SHIFT_TEMPLATES for delete as insert into W6OPERATION_LOG select 114, W6Key, 1, getdate() from deleted