﻿CREATE TABLE [dbo].[W6ASSET_CATEGORIES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_10000012] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000012_2]
    ON [dbo].[W6ASSET_CATEGORIES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W610000012_UPDATE_CACHED on W6ASSET_CATEGORIES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000012,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000012_DELETE_CACHED on W6ASSET_CATEGORIES for delete as insert into W6OPERATION_LOG select 10000012, W6Key, 1, getdate() from deleted