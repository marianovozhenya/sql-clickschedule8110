﻿CREATE TABLE [dbo].[W6ROSTER_ROSTER_ROTATIONS] (
    [W6Key]                INT            NOT NULL,
    [Revision]             INT            NOT NULL,
    [CreatedBy]            NVARCHAR (128) NOT NULL,
    [TimeCreated]          DATETIME       NOT NULL,
    [CreatingProcess]      INT            NOT NULL,
    [ModifiedBy]           NVARCHAR (128) NOT NULL,
    [TimeModified]         DATETIME       NOT NULL,
    [ModifyingProcess]     INT            NOT NULL,
    [RotationContext]      NVARCHAR (128) NULL,
    [Employee]             INT            NULL,
    [Region]               INT            NULL,
    [District]             INT            NULL,
    [Team]                 INT            NULL,
    [ShiftPattern]         INT            NULL,
    [ShiftPatternStartDay] INT            NULL,
    [ReferenceDate]        DATETIME       NULL,
    [RotationPrecedence]   INT            NULL,
    [Active]               INT            NULL,
    CONSTRAINT [W6PK_30000106] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000106_4_8_] FOREIGN KEY ([Employee]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK30000106_5_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK30000106_6_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK30000106_7_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key]),
    CONSTRAINT [W6FK30000106_8_111_] FOREIGN KEY ([ShiftPattern]) REFERENCES [dbo].[W6SHIFT_PATTERNS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000106_7_259]
    ON [dbo].[W6ROSTER_ROSTER_ROTATIONS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000106_8_111]
    ON [dbo].[W6ROSTER_ROSTER_ROTATIONS]([ShiftPattern] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000106_5_50]
    ON [dbo].[W6ROSTER_ROSTER_ROTATIONS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000106_4_8]
    ON [dbo].[W6ROSTER_ROSTER_ROTATIONS]([Employee] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000106_6_53]
    ON [dbo].[W6ROSTER_ROSTER_ROTATIONS]([District] ASC) WITH (FILLFACTOR = 50);

