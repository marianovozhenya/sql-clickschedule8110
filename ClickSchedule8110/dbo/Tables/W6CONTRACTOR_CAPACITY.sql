﻿CREATE TABLE [dbo].[W6CONTRACTOR_CAPACITY] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Engineer]         INT            NULL,
    [StartTime]        DATETIME       NULL,
    [TimeResolution]   NVARCHAR (64)  NULL,
    [Capacity]         INT            NULL,
    [NumberOfTasks]    INT            NULL,
    CONSTRAINT [W6PK_22] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK22_3_8_] FOREIGN KEY ([Engineer]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX22_5_4_0]
    ON [dbo].[W6CONTRACTOR_CAPACITY]([TimeResolution] ASC, [StartTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK22_3_8]
    ON [dbo].[W6CONTRACTOR_CAPACITY]([Engineer] ASC) WITH (FILLFACTOR = 50);

