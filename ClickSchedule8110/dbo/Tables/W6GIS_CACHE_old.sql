﻿CREATE TABLE [dbo].[W6GIS_CACHE_old] (
    [Origin_Lat]       INT NOT NULL,
    [Origin_Long]      INT NOT NULL,
    [Destination_Lat]  INT NOT NULL,
    [Destination_Long] INT NOT NULL,
    [TravelTime]       INT NULL,
    [Distance]         INT NULL,
    [GISDataSource]    INT NULL,
    [UpdateStatus]     INT NULL,
    PRIMARY KEY CLUSTERED ([Origin_Lat] ASC, [Origin_Long] ASC, [Destination_Lat] ASC, [Destination_Long] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX_GIS_TRAVELTIME]
    ON [dbo].[W6GIS_CACHE_old]([TravelTime] ASC);

