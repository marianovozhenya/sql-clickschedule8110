﻿CREATE TABLE [dbo].[W6APP] (
    [W6Key]                     INT             NOT NULL,
    [Revision]                  INT             NOT NULL,
    [CreatedBy]                 NVARCHAR (128)  NOT NULL,
    [TimeCreated]               DATETIME        NOT NULL,
    [CreatingProcess]           INT             NOT NULL,
    [ModifiedBy]                NVARCHAR (128)  NOT NULL,
    [TimeModified]              DATETIME        NOT NULL,
    [ModifyingProcess]          INT             NOT NULL,
    [Name]                      NVARCHAR (128)  NULL,
    [MajorVersion]              INT             NULL,
    [MinorVersion]              INT             NULL,
    [Product]                   INT             NULL,
    [AppState]                  INT             NULL,
    [AppType]                   INT             NULL,
    [StructureEnableScript]     NVARCHAR (MAX)  NULL,
    [BusinessEnableScript]      NVARCHAR (MAX)  NULL,
    [BusinessDisableScript]     NVARCHAR (MAX)  NULL,
    [TestScript]                NVARCHAR (MAX)  NULL,
    [ConfigurationInstructions] NVARCHAR (MAX)  NULL,
    [ShortDescription]          NVARCHAR (1024) NULL,
    [LongDescription]           NVARCHAR (MAX)  NULL,
    [Icon]                      NVARCHAR (1024) NULL,
    CONSTRAINT [W6PK_40000002] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK40000002_6_20000000_] FOREIGN KEY ([Product]) REFERENCES [dbo].[W6APPPRODUCTTYPE] ([W6Key]),
    CONSTRAINT [W6FK40000002_7_20000001_] FOREIGN KEY ([AppState]) REFERENCES [dbo].[W6APPSTATE] ([W6Key]),
    CONSTRAINT [W6FK40000002_8_20000002_] FOREIGN KEY ([AppType]) REFERENCES [dbo].[W6APPTYPE] ([W6Key])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX40000002_3_4_5_6]
    ON [dbo].[W6APP]([Name] ASC, [MajorVersion] ASC, [MinorVersion] ASC, [Product] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK40000002_8_20000002]
    ON [dbo].[W6APP]([AppType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK40000002_7_20000001]
    ON [dbo].[W6APP]([AppState] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK40000002_6_20000000]
    ON [dbo].[W6APP]([Product] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_40000002 ON W6APP FOR DELETE AS  DELETE W6APP_PREREQUISITES FROM W6APP_PREREQUISITES, deleted WHERE  W6APP_PREREQUISITES.W6Key = deleted.W6Key  DELETE W6APP_SUPPORTEDPRODUCTS FROM W6APP_SUPPORTEDPRODUCTS, deleted WHERE  W6APP_SUPPORTEDPRODUCTS.W6Key = deleted.W6Key  DELETE W6APP_COMPLIESWITHSERVERVERSIONS FROM W6APP_COMPLIESWITHSERVERVERSIONS, deleted WHERE  W6APP_COMPLIESWITHSERVERVERSIONS.W6Key = deleted.W6Key  DELETE W6APP_IMAGES FROM W6APP_IMAGES, deleted WHERE  W6APP_IMAGES.W6Key = deleted.W6Key  DELETE W6APP_MODULES FROM W6APP_MODULES, deleted WHERE  W6APP_MODULES.W6Key = deleted.W6Key