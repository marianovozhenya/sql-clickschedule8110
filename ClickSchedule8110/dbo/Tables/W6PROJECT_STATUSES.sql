﻿CREATE TABLE [dbo].[W6PROJECT_STATUSES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_10000011] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000011_2]
    ON [dbo].[W6PROJECT_STATUSES]([Name] ASC) WITH (FILLFACTOR = 20);

