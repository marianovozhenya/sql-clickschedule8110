﻿CREATE TABLE [dbo].[W6BUSINESS_EVENTS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [RelatedPlan]      INT             NULL,
    [PlanScenario]     INT             NULL,
    [Type]             INT             NULL,
    [Probability]      FLOAT (53)      NULL,
    [Template]         INT             NULL,
    [StartTime]        DATETIME        NULL,
    [Active]           INT             NULL,
    [Intensity]        FLOAT (53)      NULL,
    [Description]      NVARCHAR (1024) NULL,
    [Name]             NVARCHAR (128)  NULL,
    [ExtraInfo]        NVARCHAR (MAX)  NULL,
    [OrderTime]        DATETIME        NULL,
    CONSTRAINT [W6PK_10] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10_3_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK10_4_101_] FOREIGN KEY ([PlanScenario]) REFERENCES [dbo].[W6PLAN_SCENARIOS] ([W6Key]),
    CONSTRAINT [W6FK10_5_63_] FOREIGN KEY ([Type]) REFERENCES [dbo].[W6BUSINESS_EVENT_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10_5_63]
    ON [dbo].[W6BUSINESS_EVENTS]([Type] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10_4_101]
    ON [dbo].[W6BUSINESS_EVENTS]([PlanScenario] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10_3_100]
    ON [dbo].[W6BUSINESS_EVENTS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_10 ON W6BUSINESS_EVENTS FOR DELETE AS  DELETE W6BUSINESS_EVENTS_RANGES FROM W6BUSINESS_EVENTS_RANGES, deleted WHERE  W6BUSINESS_EVENTS_RANGES.W6Key = deleted.W6Key