﻿CREATE TABLE [dbo].[W6SHIFT_PATTERNS] (
    [W6Key]                   INT             NOT NULL,
    [Revision]                INT             NOT NULL,
    [CreatedBy]               NVARCHAR (128)  NOT NULL,
    [TimeCreated]             DATETIME        NOT NULL,
    [CreatingProcess]         INT             NOT NULL,
    [ModifiedBy]              NVARCHAR (128)  NOT NULL,
    [TimeModified]            DATETIME        NOT NULL,
    [ModifyingProcess]        INT             NOT NULL,
    [Name]                    NVARCHAR (64)   NULL,
    [PatternLength]           INT             NULL,
    [Description]             NVARCHAR (1024) NULL,
    [Active]                  INT             NULL,
    [ShiftPatternType]        INT             NULL,
    [RecommendedStartWeekDay] INT             NULL,
    [RecommendedNumberOfRows] INT             NULL,
    [RecommendedOffset]       INT             NULL,
    [Region]                  INT             NULL,
    [District]                INT             NULL,
    [Team]                    INT             NULL,
    CONSTRAINT [W6PK_111] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK111_12_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK111_13_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK111_14_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key]),
    CONSTRAINT [W6FK111_8_251_] FOREIGN KEY ([ShiftPatternType]) REFERENCES [dbo].[W6SHIFT_PATTERN_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK111_14_259]
    ON [dbo].[W6SHIFT_PATTERNS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK111_13_53]
    ON [dbo].[W6SHIFT_PATTERNS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK111_12_50]
    ON [dbo].[W6SHIFT_PATTERNS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK111_8_251]
    ON [dbo].[W6SHIFT_PATTERNS]([ShiftPatternType] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_111 ON W6SHIFT_PATTERNS FOR DELETE AS  DELETE W6SHIFT_PATTERNS_TEMPLATES FROM W6SHIFT_PATTERNS_TEMPLATES, deleted WHERE  W6SHIFT_PATTERNS_TEMPLATES.W6Key = deleted.W6Key
GO
create trigger W6111_UPDATE_CACHED on W6SHIFT_PATTERNS for insert,update as if update(revision) insert into W6OPERATION_LOG select 111,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W6111_DELETE_CACHED on W6SHIFT_PATTERNS for delete as insert into W6OPERATION_LOG select 111, W6Key, 1, getdate() from deleted