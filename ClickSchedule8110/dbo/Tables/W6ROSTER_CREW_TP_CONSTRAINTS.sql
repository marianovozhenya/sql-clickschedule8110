﻿CREATE TABLE [dbo].[W6ROSTER_CREW_TP_CONSTRAINTS] (
    [W6Key]           INT           NOT NULL,
    [W6SubKey_1]      INT           NOT NULL,
    [EngineerGroup]   INT           NULL,
    [DemandType]      INT           NULL,
    [ConstraintName]  NVARCHAR (64) NULL,
    [MinimumRequired] INT           NULL,
    CONSTRAINT [W6PK_50000176] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK50000176_0_14_] FOREIGN KEY ([EngineerGroup]) REFERENCES [dbo].[W6GROUPS] ([W6Key]),
    CONSTRAINT [W6FK50000176_1_266_] FOREIGN KEY ([DemandType]) REFERENCES [dbo].[W6ROSTER_DEMAND_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50000176_1_266]
    ON [dbo].[W6ROSTER_CREW_TP_CONSTRAINTS]([DemandType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50000176_0_14]
    ON [dbo].[W6ROSTER_CREW_TP_CONSTRAINTS]([EngineerGroup] ASC) WITH (FILLFACTOR = 50);

