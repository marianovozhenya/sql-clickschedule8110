﻿CREATE TABLE [dbo].[W6USER_SETTINGS] (
    [W6Key]             INT            NOT NULL,
    [Revision]          INT            NOT NULL,
    [CreatedBy]         NVARCHAR (128) NOT NULL,
    [TimeCreated]       DATETIME       NOT NULL,
    [CreatingProcess]   INT            NOT NULL,
    [ModifiedBy]        NVARCHAR (128) NOT NULL,
    [TimeModified]      DATETIME       NOT NULL,
    [ModifyingProcess]  INT            NOT NULL,
    [Owner]             NVARCHAR (128) NULL,
    [Category]          NVARCHAR (128) NULL,
    [SubCategory]       NVARCHAR (128) NULL,
    [Name]              NVARCHAR (128) NULL,
    [Body]              NVARCHAR (MAX) NULL,
    [ParentUserSetting] INT            NULL,
    CONSTRAINT [W6PK_17] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK17_8_17_] FOREIGN KEY ([ParentUserSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK17_8_17]
    ON [dbo].[W6USER_SETTINGS]([ParentUserSetting] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX17_4_5_0_1]
    ON [dbo].[W6USER_SETTINGS]([Category] ASC, [SubCategory] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX17_3_4_5_6_0_1]
    ON [dbo].[W6USER_SETTINGS]([Owner] ASC, [Category] ASC, [SubCategory] ASC, [Name] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);

