﻿CREATE TABLE [dbo].[W6CATALOG_PROFILES] (
    [W6Key]                  INT             NOT NULL,
    [Revision]               INT             NOT NULL,
    [Name]                   NVARCHAR (64)   NULL,
    [Description]            NVARCHAR (1024) NULL,
    [ParentNotificationType] INT             NULL,
    [Stamp_TimeModified]     DATETIME        NULL,
    CONSTRAINT [W6PK_10000004] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10000004_4_10000003_] FOREIGN KEY ([ParentNotificationType]) REFERENCES [dbo].[W6BACKREPORTING_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000004_2]
    ON [dbo].[W6CATALOG_PROFILES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000004_4_10000003]
    ON [dbo].[W6CATALOG_PROFILES]([ParentNotificationType] ASC) WITH (FILLFACTOR = 50);

