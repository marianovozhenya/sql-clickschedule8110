﻿CREATE TABLE [dbo].[W6ATTRIBUTES] (
    [Collection_ID]           INT            NOT NULL,
    [Attribute_ID]            INT            NOT NULL,
    [Name]                    VARCHAR (128)  NOT NULL,
    [Attribute_Def_Type]      INT            NOT NULL,
    [Value_Type]              INT            NULL,
    [Attribute_Collection_ID] INT            NULL,
    [Is_Mandatory]            INT            NOT NULL,
    [Extra_Data]              VARCHAR (1024) NULL,
    [Default_Value]           VARCHAR (512)  NULL,
    [Is_Product]              INT            NULL,
    PRIMARY KEY CLUSTERED ([Collection_ID] ASC, [Attribute_ID] ASC)
);

