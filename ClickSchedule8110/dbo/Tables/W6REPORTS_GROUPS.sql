﻿CREATE TABLE [dbo].[W6REPORTS_GROUPS] (
    [W6Key]        INT            NOT NULL,
    [W6SubKey_1]   INT            NOT NULL,
    [CollectionID] INT            NULL,
    [GroupBody]    NVARCHAR (MAX) NULL,
    CONSTRAINT [W6PK_70002] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

