﻿CREATE TABLE [dbo].[W6TASKS] (
    [W6Key]                     INT             NOT NULL,
    [Revision]                  INT             NOT NULL,
    [CreatedBy]                 NVARCHAR (128)  NOT NULL,
    [TimeCreated]               DATETIME        NOT NULL,
    [CreatingProcess]           INT             NOT NULL,
    [ModifiedBy]                NVARCHAR (128)  NOT NULL,
    [TimeModified]              DATETIME        NOT NULL,
    [ModifyingProcess]          INT             NOT NULL,
    [CallID]                    NVARCHAR (64)   NULL,
    [TaskNumber]                INT             NULL,
    [EarlyStart]                DATETIME        NULL,
    [DueDate]                   DATETIME        NULL,
    [LateStart]                 DATETIME        NULL,
    [Priority]                  INT             NULL,
    [Status]                    INT             NULL,
    [Customer]                  NVARCHAR (255)  NULL,
    [Calendar]                  INT             NULL,
    [Region]                    INT             NULL,
    [District]                  INT             NULL,
    [PostCode]                  NVARCHAR (64)   NULL,
    [ContractType]              INT             NULL,
    [OpenDate]                  DATETIME        NULL,
    [ContactDate]               DATETIME        NULL,
    [ConfirmationDate]          DATETIME        NULL,
    [TaskType]                  INT             NULL,
    [Duration]                  INT             NULL,
    [NumberOfRequiredEngineers] INT             NULL,
    [EngineerType]              INT             NULL,
    [Critical]                  INT             NULL,
    [AppointmentStart]          DATETIME        NULL,
    [AppointmentFinish]         DATETIME        NULL,
    [ContactName]               NVARCHAR (128)  NULL,
    [ContactPhoneNumber]        NVARCHAR (64)   NULL,
    [BinaryData]                NVARCHAR (4000) NULL,
    [Latitude]                  INT             NULL,
    [Longitude]                 INT             NULL,
    [GISDataSource]             INT             NULL,
    [Street]                    NVARCHAR (64)   NULL,
    [City]                      NVARCHAR (64)   NULL,
    [State]                     NVARCHAR (64)   NULL,
    [TaskStatusContext]         INT             NULL,
    [IsCrewTask]                INT             NULL,
    [CountryID]                 INT             NULL,
    [IsScheduled]               INT             NULL,
    [CustomerEmail]             NVARCHAR (128)  NULL,
    [RequiredCrewSize]          INT             NULL,
    [InJeopardy]                INT             NULL,
    [Pinned]                    INT             NULL,
    [JeopardyState]             INT             NULL,
    [DisplayStatus]             INT             NULL,
    [CustomerFax]               NVARCHAR (128)  NULL,
    [DispatchDate]              DATETIME        NULL,
    [ScheduleDate]              DATETIME        NULL,
    [RejectionReason]           INT             NULL,
    [MetricDate]                DATETIME        NULL,
    [IncompletionReason]        NVARCHAR (128)  NULL,
    [CapacityDate]              DATETIME        NULL,
    [CommentText]               NVARCHAR (1000) NULL,
    [LastRejectedEngineer]      NVARCHAR (255)  NULL,
    [CustomerReference]         INT             NULL,
    [LastNotificationSent]      INT             NULL,
    [NotifyLongTerm]            INT             NULL,
    [NotifyMidTerm]             INT             NULL,
    [NotifyShortTerm]           INT             NULL,
    [NotifySMS]                 INT             NULL,
    [NotifyEmail]               INT             NULL,
    [NotifyIVR]                 INT             NULL,
    [WebUser]                   NVARCHAR (64)   NULL,
    [SurveyComment]             NVARCHAR (512)  NULL,
    [InstallingOffice]          NVARCHAR (100)  NULL,
    [AccountNumber]             INT             NULL,
    [AccountOwner]              NVARCHAR (50)   NULL,
    [Panel]                     NVARCHAR (50)   NULL,
    [PanelLocation]             NVARCHAR (50)   NULL,
    [OriginalInstaller]         NVARCHAR (220)  NULL,
    [SignalConfirmation]        NVARCHAR (64)   NULL,
    [ResultCode]                INT             NULL,
    [CSID]                      NVARCHAR (50)   NULL,
    [ReceiverLine]              NVARCHAR (55)   NULL,
    [OriginalInstallDate]       DATETIME        NULL,
    [CentralStation]            NVARCHAR (50)   NULL,
    [DispatcherComment]         NVARCHAR (4000) NULL,
    [CellPhoneNumber]           NVARCHAR (64)   NULL,
    [LocationVerificationError] NVARCHAR (128)  NULL,
    [NotifySpanish]             INT             NULL,
    [LaborBillable]             INT             NULL,
    [TripCharge]                INT             NULL,
    [StateSubdivision]          NVARCHAR (64)   NULL,
    [CitySubdivision]           NVARCHAR (64)   NULL,
    [Team]                      INT             NULL,
    [HasRuleViolations]         INT             NULL,
    [RuleViolations]            NVARCHAR (1000) NULL,
    [ExternalRefID]             NVARCHAR (128)  NULL,
    [NextNotification]          INT             NULL,
    [NotificationCarrier]       INT             NULL,
    [NotificationLanguage]      INT             NULL,
    [WorkOrderItem]             INT             NULL,
    [IsMegatask]                INT             NULL,
    [IsBundled]                 INT             NULL,
    [IsManuallyBundled]         INT             NULL,
    [Megatask]                  INT             NULL,
    [MegataskPureDuration]      INT             NULL,
    [BundlerConfiguration]      INT             NULL,
    [CustomerAccount]           INT             NULL,
    [Org]                       INT             NULL,
    [SFDCServiceID]             NVARCHAR (64)   NULL,
    [SFDCKey]                   NVARCHAR (64)   NULL,
    [SFDCIntegrated]            INT             NULL,
    [ServiceNumber]             NVARCHAR (64)   NULL,
    [SalesRep]                  NVARCHAR (64)   NULL,
    [Case_Summary__c]           NVARCHAR (2000) NULL,
    [Case_Notes__c]             NVARCHAR (4000) NULL,
    [WrongTaskType]             INT             NULL,
    [FSPro_User]                NVARCHAR (100)  NULL,
    [ServiceCode]               INT             NULL,
    CONSTRAINT [W6PK_2] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK2_102_74_] FOREIGN KEY ([NextNotification]) REFERENCES [dbo].[W6NOTIFICATIONS] ([W6Key]),
    CONSTRAINT [W6FK2_103_86_] FOREIGN KEY ([NotificationCarrier]) REFERENCES [dbo].[W6NOTIFICATIONCARRIER] ([W6Key]),
    CONSTRAINT [W6FK2_104_85_] FOREIGN KEY ([NotificationLanguage]) REFERENCES [dbo].[W6NOTIFICATION_LANGUAGE] ([W6Key]),
    CONSTRAINT [W6FK2_105_30000011_] FOREIGN KEY ([WorkOrderItem]) REFERENCES [dbo].[W6WO_ITEMS] ([W6Key]),
    CONSTRAINT [W6FK2_109_2_] FOREIGN KEY ([Megatask]) REFERENCES [dbo].[W6TASKS] ([W6Key]),
    CONSTRAINT [W6FK2_11_3_] FOREIGN KEY ([Calendar]) REFERENCES [dbo].[W6CALENDARS] ([W6Key]),
    CONSTRAINT [W6FK2_111_10000221_] FOREIGN KEY ([BundlerConfiguration]) REFERENCES [dbo].[W6BUNDLER_CONFIGURATIONS] ([W6Key]),
    CONSTRAINT [W6FK2_114_30000013_] FOREIGN KEY ([CustomerAccount]) REFERENCES [dbo].[W6CUSTOMER_ACCOUNTS] ([W6Key]),
    CONSTRAINT [W6FK2_115_88_] FOREIGN KEY ([Org]) REFERENCES [dbo].[W6ORGANIZATION] ([W6Key]),
    CONSTRAINT [W6FK2_12_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK2_125_89_] FOREIGN KEY ([ServiceCode]) REFERENCES [dbo].[W6SERVICECODE] ([W6Key]),
    CONSTRAINT [W6FK2_13_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK2_16_52_] FOREIGN KEY ([ContractType]) REFERENCES [dbo].[W6CONTRACT_TYPES] ([W6Key]),
    CONSTRAINT [W6FK2_20_56_] FOREIGN KEY ([TaskType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key]),
    CONSTRAINT [W6FK2_26_55_] FOREIGN KEY ([EngineerType]) REFERENCES [dbo].[W6ENGINEER_TYPES] ([W6Key]),
    CONSTRAINT [W6FK2_44_66_] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK2_52_254_] FOREIGN KEY ([JeopardyState]) REFERENCES [dbo].[W6JEOPARDY_STATE] ([W6Key]),
    CONSTRAINT [W6FK2_53_255_] FOREIGN KEY ([DisplayStatus]) REFERENCES [dbo].[W6DISPLAY_STATUS] ([W6Key]),
    CONSTRAINT [W6FK2_57_82_] FOREIGN KEY ([RejectionReason]) REFERENCES [dbo].[W6REJECTIONREASON] ([W6Key]),
    CONSTRAINT [W6FK2_63_60_] FOREIGN KEY ([CustomerReference]) REFERENCES [dbo].[W6CUSTOMERS] ([W6Key]),
    CONSTRAINT [W6FK2_64_74_] FOREIGN KEY ([LastNotificationSent]) REFERENCES [dbo].[W6NOTIFICATIONS] ([W6Key]),
    CONSTRAINT [W6FK2_82_78_] FOREIGN KEY ([ResultCode]) REFERENCES [dbo].[W6RESULTCODES] ([W6Key]),
    CONSTRAINT [W6FK2_9_51_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6TASK_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK2_91_80_] FOREIGN KEY ([LaborBillable]) REFERENCES [dbo].[W6LABORBILLABLE] ([W6Key]),
    CONSTRAINT [W6FK2_92_83_] FOREIGN KEY ([TripCharge]) REFERENCES [dbo].[W6TRIPCHARGE] ([W6Key]),
    CONSTRAINT [W6FK2_95_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_105_30000011]
    ON [dbo].[W6TASKS]([WorkOrderItem] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_20_56]
    ON [dbo].[W6TASKS]([TaskType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_9_51]
    ON [dbo].[W6TASKS]([Status] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_82_78]
    ON [dbo].[W6TASKS]([ResultCode] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX2_12_13_6]
    ON [dbo].[W6TASKS]([Region] ASC, [District] ASC, [DueDate] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX2_12_13]
    ON [dbo].[W6TASKS]([Region] ASC, [District] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_12_50]
    ON [dbo].[W6TASKS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_109_2]
    ON [dbo].[W6TASKS]([Megatask] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX2_37_36]
    ON [dbo].[W6TASKS]([Longitude] ASC, [Latitude] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_52_254]
    ON [dbo].[W6TASKS]([JeopardyState] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_13_53]
    ON [dbo].[W6TASKS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_125_89]
    ON [dbo].[W6TASKS]([ServiceCode] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_44_66]
    ON [dbo].[W6TASKS]([CountryID] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX2_3_4]
    ON [dbo].[W6TASKS]([CallID] ASC, [TaskNumber] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2_11_3]
    ON [dbo].[W6TASKS]([Calendar] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX2_31]
    ON [dbo].[W6TASKS]([AppointmentStart] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX2_32]
    ON [dbo].[W6TASKS]([AppointmentFinish] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_2 ON W6TASKS FOR DELETE AS  DELETE W6TASKS_PREFERRED_ENGINEERS FROM W6TASKS_PREFERRED_ENGINEERS, deleted WHERE  W6TASKS_PREFERRED_ENGINEERS.W6Key = deleted.W6Key  DELETE W6TASKS_REQUIRED_ENGINEERS FROM W6TASKS_REQUIRED_ENGINEERS, deleted WHERE  W6TASKS_REQUIRED_ENGINEERS.W6Key = deleted.W6Key  DELETE W6TASKS_REQUIRED_SKILLS1 FROM W6TASKS_REQUIRED_SKILLS1, deleted WHERE  W6TASKS_REQUIRED_SKILLS1.W6Key = deleted.W6Key  DELETE W6TASKS_REQUIRED_SKILLS2 FROM W6TASKS_REQUIRED_SKILLS2, deleted WHERE  W6TASKS_REQUIRED_SKILLS2.W6Key = deleted.W6Key  DELETE W6TASKS_REQUIRED_ENG_TOOLS FROM W6TASKS_REQUIRED_ENG_TOOLS, deleted WHERE  W6TASKS_REQUIRED_ENG_TOOLS.W6Key = deleted.W6Key  DELETE W6TASKS_TIME_DEP FROM W6TASKS_TIME_DEP, deleted WHERE  W6TASKS_TIME_DEP.W6Key = deleted.W6Key  DELETE W6TASKS_ENGINEER_DEP FROM W6TASKS_ENGINEER_DEP, deleted WHERE  W6TASKS_ENGINEER_DEP.W6Key = deleted.W6Key  DELETE W6TASKS_ENG_REQ FROM W6TASKS_ENG_REQ, deleted WHERE  W6TASKS_ENG_REQ.W6Key = deleted.W6Key  DELETE W6TASKS_EXCLUDED_ENGINEERS FROM W6TASKS_EXCLUDED_ENGINEERS, deleted WHERE  W6TASKS_EXCLUDED_ENGINEERS.W6Key = deleted.W6Key  DELETE W6TASKS_SURVEYANSWERS FROM W6TASKS_SURVEYANSWERS, deleted WHERE  W6TASKS_SURVEYANSWERS.W6Key = deleted.W6Key  DELETE W6TASKS_PARTS FROM W6TASKS_PARTS, deleted WHERE  W6TASKS_PARTS.W6Key = deleted.W6Key  DELETE W6TASKS_PARTS_USED FROM W6TASKS_PARTS_USED, deleted WHERE  W6TASKS_PARTS_USED.W6Key = deleted.W6Key  DELETE W6TASKS_ASSETS FROM W6TASKS_ASSETS, deleted WHERE  W6TASKS_ASSETS.W6Key = deleted.W6Key  DELETE W6TASKS_BACKREPORTINGS FROM W6TASKS_BACKREPORTINGS, deleted WHERE  W6TASKS_BACKREPORTINGS.W6Key = deleted.W6Key  DELETE W6TASKS_SUB_TASKS FROM W6TASKS_SUB_TASKS, deleted WHERE  W6TASKS_SUB_TASKS.W6Key = deleted.W6Key  DELETE SKILLSDURATION FROM SKILLSDURATION, deleted WHERE  SKILLSDURATION.W6Key = deleted.W6Key