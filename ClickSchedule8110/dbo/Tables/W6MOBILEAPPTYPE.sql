﻿CREATE TABLE [dbo].[W6MOBILEAPPTYPE] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_20000003] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX20000003_2]
    ON [dbo].[W6MOBILEAPPTYPE]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W620000003_UPDATE_CACHED on W6MOBILEAPPTYPE for insert,update as if update(revision) insert into W6OPERATION_LOG select 20000003,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W620000003_DELETE_CACHED on W6MOBILEAPPTYPE for delete as insert into W6OPERATION_LOG select 20000003, W6Key, 1, getdate() from deleted