﻿CREATE TABLE [dbo].[W6GENERIC_EVENT_DEFS_PROPS] (
    [W6Key]        INT            NOT NULL,
    [W6SubKey_1]   INT            NOT NULL,
    [PropertyName] NVARCHAR (128) NULL,
    CONSTRAINT [W6PK_50000162] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

