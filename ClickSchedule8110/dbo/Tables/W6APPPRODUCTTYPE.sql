﻿CREATE TABLE [dbo].[W6APPPRODUCTTYPE] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_20000000] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX20000000_2]
    ON [dbo].[W6APPPRODUCTTYPE]([Name] ASC) WITH (FILLFACTOR = 20);

