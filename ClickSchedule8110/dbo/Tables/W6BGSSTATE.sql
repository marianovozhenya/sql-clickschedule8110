﻿CREATE TABLE [dbo].[W6BGSSTATE] (
    [BGSInstanceID] INT            NOT NULL,
    [BGSID]         INT            NOT NULL,
    [ServerName]    NVARCHAR (32)  NULL,
    [Properties]    NVARCHAR (128) NOT NULL,
    [IntervalID]    INT            NOT NULL,
    [StartAt]       DATETIME       NOT NULL,
    [FinishAt]      DATETIME       NOT NULL,
    CONSTRAINT [W6BGSSTATE_PK] PRIMARY KEY CLUSTERED ([BGSInstanceID] ASC, [BGSID] ASC)
);

