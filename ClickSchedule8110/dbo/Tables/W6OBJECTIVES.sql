﻿CREATE TABLE [dbo].[W6OBJECTIVES] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Name]             NVARCHAR (128) NULL,
    [Body]             NVARCHAR (MAX) NULL,
    [Prog_ID]          NVARCHAR (256) NULL,
    [Objective_Family] INT            NULL,
    [Minimal_Grade]    FLOAT (53)     NULL,
    [Maximal_Grade]    FLOAT (53)     NULL,
    [Category]         NVARCHAR (255) NULL,
    CONSTRAINT [W6PK_12] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
 CREATE TRIGGER W6TRIGGER_12 ON W6OBJECTIVES FOR DELETE AS  DELETE W6OBJECTIVE_RESOURCES FROM W6OBJECTIVE_RESOURCES, deleted WHERE  W6OBJECTIVE_RESOURCES.W6Key = deleted.W6Key
GO
create trigger W612_UPDATE_CACHED on W6OBJECTIVES for insert,update as if update(revision) insert into W6OPERATION_LOG select 12,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W612_DELETE_CACHED on W6OBJECTIVES for delete as insert into W6OPERATION_LOG select 12, W6Key, 1, getdate() from deleted