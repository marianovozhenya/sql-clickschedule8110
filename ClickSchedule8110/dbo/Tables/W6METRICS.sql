﻿CREATE TABLE [dbo].[W6METRICS] (
    [W6Key]                 INT             NOT NULL,
    [Revision]              INT             NOT NULL,
    [CreatedBy]             NVARCHAR (128)  NOT NULL,
    [TimeCreated]           DATETIME        NOT NULL,
    [CreatingProcess]       INT             NOT NULL,
    [ModifiedBy]            NVARCHAR (128)  NOT NULL,
    [TimeModified]          DATETIME        NOT NULL,
    [ModifyingProcess]      INT             NOT NULL,
    [Name]                  NVARCHAR (128)  NULL,
    [LogicProgID]           NVARCHAR (256)  NULL,
    [SpecificSettings]      NVARCHAR (4000) NULL,
    [Units]                 INT             NULL,
    [MinimumLimit]          FLOAT (53)      NULL,
    [MaximumLimit]          FLOAT (53)      NULL,
    [MinimumThreshold]      FLOAT (53)      NULL,
    [MinimumThresholdAlert] NVARCHAR (512)  NULL,
    [MaximumThreshold]      FLOAT (53)      NULL,
    [MaximumThresholdAlert] NVARCHAR (512)  NULL,
    [Category]              NVARCHAR (128)  NULL,
    [MetricCategory]        INT             NULL,
    CONSTRAINT [W6PK_18] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK18_14_67_] FOREIGN KEY ([MetricCategory]) REFERENCES [dbo].[W6METRICS_CATEGORIES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK18_14_67]
    ON [dbo].[W6METRICS]([MetricCategory] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W618_UPDATE_CACHED on W6METRICS for insert,update as if update(revision) insert into W6OPERATION_LOG select 18,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W618_DELETE_CACHED on W6METRICS for delete as insert into W6OPERATION_LOG select 18, W6Key, 1, getdate() from deleted