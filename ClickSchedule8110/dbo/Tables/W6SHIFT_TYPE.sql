﻿CREATE TABLE [dbo].[W6SHIFT_TYPE] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_90] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX90_2]
    ON [dbo].[W6SHIFT_TYPE]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W690_UPDATE_CACHED on W6SHIFT_TYPE for insert,update as if update(revision) insert into W6OPERATION_LOG select 90,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W690_DELETE_CACHED on W6SHIFT_TYPE for delete as insert into W6OPERATION_LOG select 90, W6Key, 1, getdate() from deleted