﻿CREATE TABLE [dbo].[W6OUTGOING_MESSAGES] (
    [W6Key]            INT            NOT NULL,
    [W6Revision]       INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [SessionID]        INT            NULL,
    [TransactionID]    INT            NULL,
    [Body]             NVARCHAR (MAX) NULL,
    [MessageStatus]    INT            NULL,
    CONSTRAINT [W6PK_6] PRIMARY KEY NONCLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX6_6_2__4_0]
    ON [dbo].[W6OUTGOING_MESSAGES]([MessageStatus] ASC, [TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX6_6_4_2__1]
    ON [dbo].[W6OUTGOING_MESSAGES]([MessageStatus] ASC, [TransactionID] ASC, [TimeCreated] ASC) WITH (FILLFACTOR = 20);


GO
CREATE CLUSTERED INDEX [W6IX6_6]
    ON [dbo].[W6OUTGOING_MESSAGES]([MessageStatus] ASC);


GO
--exec sp_helptext W6OUTGOING_MESSAGES_INSERT

/*
create trigger dbo.W6OUTGOING_MESSAGES_INSERT on W6OUTGOING_MESSAGES 
for insert  as 
declare @dtNow as datetime 
set @dtNow = GetDate()
update W6OUTGOING_MESSAGES  
set TimeCreated=@dtNow,TimeModified=@dtNow where MessageStatus = 0 and W6Key 
in (select W6Key
 from inserted) 
*/

CREATE trigger [dbo].[W6OUTGOING_MESSAGES_INSERT] on [dbo].[W6OUTGOING_MESSAGES] 
instead of insert 
as 
declare @MessageStatus as integer 
select @MessageStatus=MessageStatus from inserted 
if @MessageStatus = 0 
insert into W6OUTGOING_MESSAGES (W6Key,W6Revision,CreatedBy,TimeCreated,CreatingProcess,ModifiedBy,TimeModified,ModifyingProcess,SessionID,TransactionID, 
Body,MessageStatus) 
select W6Key,W6Revision,CreatedBy,GetDate(),CreatingProcess,ModifiedBy,GetDate(),ModifyingProcess,SessionID,TransactionID, 
Body,MessageStatus from inserted 
else 
insert into W6OUTGOING_MESSAGES (W6Key,W6Revision,CreatedBy,TimeCreated,CreatingProcess,ModifiedBy,TimeModified,ModifyingProcess,SessionID,TransactionID, 
Body,MessageStatus) select W6Key,W6Revision,CreatedBy,TimeCreated,CreatingProcess,ModifiedBy,TimeModified,ModifyingProcess,SessionID,TransactionID, 
Body,MessageStatus from inserted