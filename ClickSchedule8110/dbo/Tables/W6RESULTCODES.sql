﻿CREATE TABLE [dbo].[W6RESULTCODES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    [UsedInMobile]       INT           NULL,
    CONSTRAINT [W6PK_78] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX78_2]
    ON [dbo].[W6RESULTCODES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W678_UPDATE_CACHED on W6RESULTCODES for insert,update as if update(revision) insert into W6OPERATION_LOG select 78,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W678_DELETE_CACHED on W6RESULTCODES for delete as insert into W6OPERATION_LOG select 78, W6Key, 1, getdate() from deleted