﻿CREATE TABLE [dbo].[W6RELEVANCE_GROUPS] (
    [W6Key]                   INT            NOT NULL,
    [Revision]                INT            NOT NULL,
    [CreatedBy]               NVARCHAR (128) NOT NULL,
    [TimeCreated]             DATETIME       NOT NULL,
    [CreatingProcess]         INT            NOT NULL,
    [ModifiedBy]              NVARCHAR (128) NOT NULL,
    [TimeModified]            DATETIME       NOT NULL,
    [ModifyingProcess]        INT            NOT NULL,
    [Name]                    NVARCHAR (128) NULL,
    [Time_Check_Policy]       INT            NULL,
    [Optimistic]              INT            NULL,
    [Multiple_Resource_Check] INT            NULL,
    [Demand_Group_Key]        INT            NULL,
    [Resource_Group_Key]      INT            NULL,
    [Rule_Family]             INT            NULL,
    CONSTRAINT [W6PK_16] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK16_7_14_] FOREIGN KEY ([Demand_Group_Key]) REFERENCES [dbo].[W6GROUPS] ([W6Key]),
    CONSTRAINT [W6FK16_8_14_] FOREIGN KEY ([Resource_Group_Key]) REFERENCES [dbo].[W6GROUPS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK16_8_14]
    ON [dbo].[W6RELEVANCE_GROUPS]([Resource_Group_Key] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK16_7_14]
    ON [dbo].[W6RELEVANCE_GROUPS]([Demand_Group_Key] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_16 ON W6RELEVANCE_GROUPS FOR DELETE AS  DELETE W6AG_TIME_INTERVALS FROM W6AG_TIME_INTERVALS, deleted WHERE  W6AG_TIME_INTERVALS.W6Key = deleted.W6Key
GO
create trigger W616_UPDATE_CACHED on W6RELEVANCE_GROUPS for insert,update as if update(revision) insert into W6OPERATION_LOG select 16,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W616_DELETE_CACHED on W6RELEVANCE_GROUPS for delete as insert into W6OPERATION_LOG select 16, W6Key, 1, getdate() from deleted