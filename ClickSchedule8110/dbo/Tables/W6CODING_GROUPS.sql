﻿CREATE TABLE [dbo].[W6CODING_GROUPS] (
    [W6Key]                INT             NOT NULL,
    [Revision]             INT             NOT NULL,
    [Name]                 NVARCHAR (64)   NULL,
    [Description]          NVARCHAR (1024) NULL,
    [ParentCatalogProfile] INT             NULL,
    [Stamp_TimeModified]   DATETIME        NULL,
    CONSTRAINT [W6PK_10000005] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10000005_4_10000004_] FOREIGN KEY ([ParentCatalogProfile]) REFERENCES [dbo].[W6CATALOG_PROFILES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000005_2]
    ON [dbo].[W6CODING_GROUPS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000005_4_10000004]
    ON [dbo].[W6CODING_GROUPS]([ParentCatalogProfile] ASC) WITH (FILLFACTOR = 50);

