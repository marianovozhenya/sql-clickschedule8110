﻿CREATE TABLE [dbo].[Results] (
    [Region]        NVARCHAR (64) NULL,
    [District]      NVARCHAR (64) NULL,
    [JobType]       NVARCHAR (64) NULL,
    [StartTime]     DATETIME      NULL,
    [TaskDuration]  FLOAT (53)    NULL,
    [TravelTime]    FLOAT (53)    NULL,
    [NumOfCalls]    FLOAT (53)    NULL,
    [TotalDuration] FLOAT (53)    NULL
);

