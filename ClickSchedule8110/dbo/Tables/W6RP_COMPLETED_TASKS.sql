﻿CREATE TABLE [dbo].[W6RP_COMPLETED_TASKS] (
    [W6InstanceID]                 INT            NOT NULL,
    [W6EntryID]                    INT            NOT NULL,
    [TimeResolution_Start]         DATETIME       NULL,
    [TimeResolution_Finish]        DATETIME       NULL,
    [Region_Key]                   INT            NULL,
    [Region_Name]                  NVARCHAR (256) NULL,
    [District_Key]                 INT            NULL,
    [District_Name]                NVARCHAR (256) NULL,
    [TaskType_Key]                 INT            NULL,
    [TaskType_Name]                NVARCHAR (256) NULL,
    [CompletedAssignments_Count]   INT            NULL,
    [UncompletedAssignments_Count] INT            NULL,
    [Task_CallID]                  NVARCHAR (256) NULL,
    [Task_Customer]                NVARCHAR (256) NULL,
    [Task_Duration]                INT            NULL,
    [Task_Key]                     INT            NULL,
    [Task_Number]                  INT            NULL,
    [Task_Priority]                INT            NULL,
    CONSTRAINT [PKRP_COMPLETED_TASKS] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);

