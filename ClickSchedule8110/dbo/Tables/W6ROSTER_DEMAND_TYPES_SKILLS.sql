﻿CREATE TABLE [dbo].[W6ROSTER_DEMAND_TYPES_SKILLS] (
    [W6Key]      INT        NOT NULL,
    [W6SubKey_1] INT        NOT NULL,
    [SkillKey]   INT        NULL,
    [SkillLevel] INT        NULL,
    [Efficiency] FLOAT (53) NULL,
    CONSTRAINT [W6PK_2660000] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK2660000_0_58_] FOREIGN KEY ([SkillKey]) REFERENCES [dbo].[W6SKILLS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2660000_0_58]
    ON [dbo].[W6ROSTER_DEMAND_TYPES_SKILLS]([SkillKey] ASC) WITH (FILLFACTOR = 50);

