﻿CREATE TABLE [dbo].[W6CALENDAR_WEEKLY_INTERVALS] (
    [W6Key]             INT NOT NULL,
    [W6SubKey_1]        INT NOT NULL,
    [Seconds_To_Start]  INT NULL,
    [Seconds_To_Finish] INT NULL,
    [Status]            INT NULL,
    CONSTRAINT [W6PK_30003] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

