﻿CREATE TABLE [dbo].[W6REPORTS_INSTANCES] (
    [W6Key]       INT      NOT NULL,
    [W6SubKey_1]  INT      NOT NULL,
    [InstanceID]  INT      NULL,
    [TimeCreated] DATETIME NULL,
    CONSTRAINT [W6PK_70004] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

