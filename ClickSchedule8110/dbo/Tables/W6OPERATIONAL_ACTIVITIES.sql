﻿CREATE TABLE [dbo].[W6OPERATIONAL_ACTIVITIES] (
    [W6Key]                   INT             NOT NULL,
    [Revision]                INT             NOT NULL,
    [CreatedBy]               NVARCHAR (128)  NOT NULL,
    [TimeCreated]             DATETIME        NOT NULL,
    [CreatingProcess]         INT             NOT NULL,
    [ModifiedBy]              NVARCHAR (128)  NOT NULL,
    [TimeModified]            DATETIME        NOT NULL,
    [ModifyingProcess]        INT             NOT NULL,
    [Name]                    NVARCHAR (64)   NULL,
    [Description]             NVARCHAR (1024) NULL,
    [OperationalActivityType] INT             NULL,
    [Region]                  INT             NULL,
    [District]                INT             NULL,
    [Latitude]                INT             NULL,
    [Longitude]               INT             NULL,
    [GISDataSource]           INT             NULL,
    [Street]                  NVARCHAR (64)   NULL,
    [City]                    NVARCHAR (64)   NULL,
    [State]                   NVARCHAR (64)   NULL,
    [Country]                 INT             NULL,
    [Postcode]                NVARCHAR (64)   NULL,
    [ValidityStart]           DATETIME        NULL,
    [ValidityFinish]          DATETIME        NULL,
    CONSTRAINT [W6PK_208] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK208_14_66_] FOREIGN KEY ([Country]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK208_5_261_] FOREIGN KEY ([OperationalActivityType]) REFERENCES [dbo].[W6OPERATIONAL_ACTIVITY_TYPES] ([W6Key]),
    CONSTRAINT [W6FK208_6_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK208_7_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK208_14_66]
    ON [dbo].[W6OPERATIONAL_ACTIVITIES]([Country] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK208_7_53]
    ON [dbo].[W6OPERATIONAL_ACTIVITIES]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK208_6_50]
    ON [dbo].[W6OPERATIONAL_ACTIVITIES]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK208_5_261]
    ON [dbo].[W6OPERATIONAL_ACTIVITIES]([OperationalActivityType] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_208 ON W6OPERATIONAL_ACTIVITIES FOR DELETE AS  DELETE W6OPERATIONAL_ACTIVS_POS_TPS FROM W6OPERATIONAL_ACTIVS_POS_TPS, deleted WHERE  W6OPERATIONAL_ACTIVS_POS_TPS.W6Key = deleted.W6Key