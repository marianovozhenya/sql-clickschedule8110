﻿CREATE TABLE [dbo].[W6PAIN_TYPES] (
    [W6Key]                 INT           NOT NULL,
    [Revision]              INT           NOT NULL,
    [Name]                  NVARCHAR (64) NULL,
    [PainLevel]             FLOAT (53)    NULL,
    [Calendar]              INT           NULL,
    [MinDuration]           INT           NULL,
    [IncludeStartRequired]  INT           NULL,
    [IncludeFinishRequired] INT           NULL,
    [Stamp_TimeModified]    DATETIME      NULL,
    CONSTRAINT [W6PK_69] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK69_5_3_] FOREIGN KEY ([Calendar]) REFERENCES [dbo].[W6CALENDARS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK69_5_3]
    ON [dbo].[W6PAIN_TYPES]([Calendar] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX69_2]
    ON [dbo].[W6PAIN_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W669_UPDATE_CACHED on W6PAIN_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 69,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W669_DELETE_CACHED on W6PAIN_TYPES for delete as insert into W6OPERATION_LOG select 69, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_69 ON W6PAIN_TYPES FOR DELETE AS  DELETE W6PAIN_TYPES_RELEVANT_AV_TYPES FROM W6PAIN_TYPES_RELEVANT_AV_TYPES, deleted WHERE  W6PAIN_TYPES_RELEVANT_AV_TYPES.W6Key = deleted.W6Key