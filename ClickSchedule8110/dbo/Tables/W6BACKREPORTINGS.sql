﻿CREATE TABLE [dbo].[W6BACKREPORTINGS] (
    [W6Key]             INT             NOT NULL,
    [Revision]          INT             NOT NULL,
    [CreatedBy]         NVARCHAR (128)  NOT NULL,
    [TimeCreated]       DATETIME        NOT NULL,
    [CreatingProcess]   INT             NOT NULL,
    [ModifiedBy]        NVARCHAR (128)  NOT NULL,
    [TimeModified]      DATETIME        NOT NULL,
    [ModifyingProcess]  INT             NOT NULL,
    [Name]              NVARCHAR (256)  NULL,
    [ExternalRefID]     NVARCHAR (128)  NULL,
    [BR_Number]         INT             NULL,
    [Description]       NVARCHAR (1024) NULL,
    [ShortText]         NVARCHAR (64)   NULL,
    [LongText]          NVARCHAR (1024) NULL,
    [Priority]          INT             NULL,
    [MasterIDAsset]     NVARCHAR (128)  NULL,
    [MasterIDTask]      NVARCHAR (128)  NULL,
    [BackReportingType] INT             NULL,
    [CatalogProfile]    INT             NULL,
    [CodingGroup]       INT             NULL,
    [CodingCode]        INT             NULL,
    CONSTRAINT [W6PK_30000003] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000003_12_10000003_] FOREIGN KEY ([BackReportingType]) REFERENCES [dbo].[W6BACKREPORTING_TYPES] ([W6Key]),
    CONSTRAINT [W6FK30000003_13_10000004_] FOREIGN KEY ([CatalogProfile]) REFERENCES [dbo].[W6CATALOG_PROFILES] ([W6Key]),
    CONSTRAINT [W6FK30000003_14_10000005_] FOREIGN KEY ([CodingGroup]) REFERENCES [dbo].[W6CODING_GROUPS] ([W6Key]),
    CONSTRAINT [W6FK30000003_15_10000006_] FOREIGN KEY ([CodingCode]) REFERENCES [dbo].[W6CODING_CODES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000003_2__4_0]
    ON [dbo].[W6BACKREPORTINGS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000003_15_10000006]
    ON [dbo].[W6BACKREPORTINGS]([CodingCode] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000003_14_10000005]
    ON [dbo].[W6BACKREPORTINGS]([CodingGroup] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000003_13_10000004]
    ON [dbo].[W6BACKREPORTINGS]([CatalogProfile] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000003_12_10000003]
    ON [dbo].[W6BACKREPORTINGS]([BackReportingType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000003_4]
    ON [dbo].[W6BACKREPORTINGS]([ExternalRefID] ASC) WITH (FILLFACTOR = 20);

