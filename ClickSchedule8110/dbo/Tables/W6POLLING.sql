﻿CREATE TABLE [dbo].[W6POLLING] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Name]             NVARCHAR (64)  NULL,
    [Progress]         FLOAT (53)     NULL,
    [Status]           INT            NULL,
    [ExtraData]        NVARCHAR (MAX) NULL,
    CONSTRAINT [W6PK_105] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX105_2__4_0]
    ON [dbo].[W6POLLING]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IDX_TimeModified]
    ON [dbo].[W6POLLING]([TimeModified] ASC) WITH (ALLOW_PAGE_LOCKS = OFF);

