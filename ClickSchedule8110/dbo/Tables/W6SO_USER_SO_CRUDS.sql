﻿CREATE TABLE [dbo].[W6SO_USER_SO_CRUDS] (
    [W6Key]           INT NOT NULL,
    [CollectionID]    INT NOT NULL,
    [CanCreate]       INT NULL,
    [CanRead]         INT NULL,
    [CanUpdate]       INT NULL,
    [CanDelete]       INT NULL,
    [SoUserFilterKey] INT NULL,
    CONSTRAINT [W6PK_50000195] PRIMARY KEY CLUSTERED ([W6Key] ASC, [CollectionID] ASC),
    CONSTRAINT [W6CK50000195_5_U] CHECK ([dbo].[W6FN_COUNT_W6SO_USER_SO_CRUDS_PER_SoUserFilterKey]([SoUserFilterKey])=(1) OR [SoUserFilterKey] IS NULL),
    CONSTRAINT [W6FK50000195_5_30000202_] FOREIGN KEY ([SoUserFilterKey]) REFERENCES [dbo].[W6SO_USER_FILTERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50000195_5_30000202]
    ON [dbo].[W6SO_USER_SO_CRUDS]([SoUserFilterKey] ASC) WITH (FILLFACTOR = 50);

