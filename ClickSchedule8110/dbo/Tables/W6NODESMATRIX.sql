﻿CREATE TABLE [dbo].[W6NODESMATRIX] (
    [NodeOrigin]       INT NOT NULL,
    [NodeDestination]  INT NOT NULL,
    [CorrectionFactor] INT NOT NULL,
    CONSTRAINT [W6PK_NODEMATRIX] PRIMARY KEY CLUSTERED ([NodeOrigin] ASC, [NodeDestination] ASC)
);

