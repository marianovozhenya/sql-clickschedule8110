﻿CREATE TABLE [dbo].[W6MEASUREMENT_POINTS] (
    [W6Key]               INT             NOT NULL,
    [Revision]            INT             NOT NULL,
    [CreatedBy]           NVARCHAR (128)  NOT NULL,
    [TimeCreated]         DATETIME        NOT NULL,
    [CreatingProcess]     INT             NOT NULL,
    [ModifiedBy]          NVARCHAR (128)  NOT NULL,
    [TimeModified]        DATETIME        NOT NULL,
    [ModifyingProcess]    INT             NOT NULL,
    [Name]                NVARCHAR (256)  NULL,
    [ExternalRefID]       NVARCHAR (128)  NULL,
    [MeasurementType]     INT             NULL,
    [Description]         NVARCHAR (1024) NULL,
    [Unit]                NVARCHAR (32)   NULL,
    [ExpectedLowValue]    FLOAT (53)      NULL,
    [ExpectedHighValue]   FLOAT (53)      NULL,
    [MeasurementDocument] INT             NULL,
    [MeasurementPosition] NVARCHAR (64)   NULL,
    CONSTRAINT [W6PK_30000005] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000005_10_30000004_] FOREIGN KEY ([MeasurementDocument]) REFERENCES [dbo].[W6MEASUREMENT_DOCUMENTS] ([W6Key]),
    CONSTRAINT [W6FK30000005_5_10000008_] FOREIGN KEY ([MeasurementType]) REFERENCES [dbo].[W6MEASUREMENT_POINT_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000005_2__4_0]
    ON [dbo].[W6MEASUREMENT_POINTS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000005_10_30000004]
    ON [dbo].[W6MEASUREMENT_POINTS]([MeasurementDocument] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000005_5_10000008]
    ON [dbo].[W6MEASUREMENT_POINTS]([MeasurementType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000005_4]
    ON [dbo].[W6MEASUREMENT_POINTS]([ExternalRefID] ASC) WITH (FILLFACTOR = 20);

