﻿CREATE TABLE [dbo].[TeamInfo] (
    [MemberID]  INT          IDENTITY (1, 1) NOT NULL,
    [TeamID]    INT          NULL,
    [FirstName] VARCHAR (50) NULL
);

