﻿CREATE TABLE [dbo].[W6CUSTOMERS] (
    [W6Key]              INT            NOT NULL,
    [Revision]           INT            NOT NULL,
    [Name]               NVARCHAR (64)  NULL,
    [ContactName]        NVARCHAR (512) NULL,
    [Fax]                NVARCHAR (256) NULL,
    [Description]        NVARCHAR (512) NULL,
    [City]               NVARCHAR (256) NULL,
    [State]              NVARCHAR (256) NULL,
    [EMail]              NVARCHAR (256) NULL,
    [Country]            INT            NULL,
    [Calendar]           INT            NULL,
    [ContractType]       INT            NULL,
    [Region]             INT            NULL,
    [District]           INT            NULL,
    [GISDataSource]      INT            NULL,
    [Latitude]           INT            NULL,
    [Longitude]          INT            NULL,
    [ContactPhoneNumber] NVARCHAR (64)  NULL,
    [Street]             NVARCHAR (64)  NULL,
    [Postcode]           NVARCHAR (64)  NULL,
    [Stamp_TimeModified] DATETIME       NULL,
    CONSTRAINT [W6PK_60] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK60_10_3_] FOREIGN KEY ([Calendar]) REFERENCES [dbo].[W6CALENDARS] ([W6Key]),
    CONSTRAINT [W6FK60_11_52_] FOREIGN KEY ([ContractType]) REFERENCES [dbo].[W6CONTRACT_TYPES] ([W6Key]),
    CONSTRAINT [W6FK60_12_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK60_13_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK60_9_66_] FOREIGN KEY ([Country]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX60_2]
    ON [dbo].[W6CUSTOMERS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK60_13_53]
    ON [dbo].[W6CUSTOMERS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK60_12_50]
    ON [dbo].[W6CUSTOMERS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK60_11_52]
    ON [dbo].[W6CUSTOMERS]([ContractType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK60_10_3]
    ON [dbo].[W6CUSTOMERS]([Calendar] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK60_9_66]
    ON [dbo].[W6CUSTOMERS]([Country] ASC) WITH (FILLFACTOR = 50);

