﻿CREATE TABLE [dbo].[W6ACTIVITY_NA_TYPES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_265] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX265_2]
    ON [dbo].[W6ACTIVITY_NA_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W6265_UPDATE_CACHED on W6ACTIVITY_NA_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 265,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W6265_DELETE_CACHED on W6ACTIVITY_NA_TYPES for delete as insert into W6OPERATION_LOG select 265, W6Key, 1, getdate() from deleted