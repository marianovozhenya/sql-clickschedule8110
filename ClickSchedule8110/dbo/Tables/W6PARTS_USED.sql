﻿CREATE TABLE [dbo].[W6PARTS_USED] (
    [W6Key]             INT             NOT NULL,
    [Revision]          INT             NOT NULL,
    [CreatedBy]         NVARCHAR (128)  NOT NULL,
    [TimeCreated]       DATETIME        NOT NULL,
    [CreatingProcess]   INT             NOT NULL,
    [ModifiedBy]        NVARCHAR (128)  NOT NULL,
    [TimeModified]      DATETIME        NOT NULL,
    [ModifyingProcess]  INT             NOT NULL,
    [Name]              NVARCHAR (256)  NULL,
    [ExternalRefID]     NVARCHAR (128)  NULL,
    [PartType]          INT             NULL,
    [Description]       NVARCHAR (1024) NULL,
    [QuantityPlanned]   FLOAT (53)      NULL,
    [ReservationNumber] NVARCHAR (64)   NULL,
    [QuantityUsed]      FLOAT (53)      NULL,
    [QuantityUnit]      NVARCHAR (32)   NULL,
    CONSTRAINT [W6PK_30000002] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000002_5_10000002_] FOREIGN KEY ([PartType]) REFERENCES [dbo].[W6PART_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000002_2__4_0]
    ON [dbo].[W6PARTS_USED]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000002_5_10000002]
    ON [dbo].[W6PARTS_USED]([PartType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000002_4]
    ON [dbo].[W6PARTS_USED]([ExternalRefID] ASC) WITH (FILLFACTOR = 20);

