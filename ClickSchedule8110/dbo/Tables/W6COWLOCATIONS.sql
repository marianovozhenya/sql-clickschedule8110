﻿CREATE TABLE [dbo].[W6COWLOCATIONS] (
    [W6Key]                  INT             NOT NULL,
    [Revision]               INT             NOT NULL,
    [Name]                   NVARCHAR (64)   NULL,
    [EngineerKey]            INT             NULL,
    [CoWDate]                DATETIME        NULL,
    [Longitude]              INT             NULL,
    [Latitude]               INT             NULL,
    [MaxDistance]            INT             NULL,
    [MaxTime]                INT             NULL,
    [Street]                 NVARCHAR (64)   NULL,
    [City]                   NVARCHAR (64)   NULL,
    [State]                  NVARCHAR (64)   NULL,
    [Postcode]               NVARCHAR (64)   NULL,
    [Country]                NVARCHAR (64)   NULL,
    [EnableRadiusLimitation] INT             NULL,
    [EnableAreaLimitation]   INT             NULL,
    [LimitedAreaKeys_old]    NVARCHAR (1500) NULL,
    [Stamp_TimeModified]     DATETIME        NULL,
    CONSTRAINT [W6PK_84] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX84_2]
    ON [dbo].[W6COWLOCATIONS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_84 ON W6COWLOCATIONS FOR DELETE AS  DELETE W6COWLOCATIONS_LIMITEDAREAKEYS FROM W6COWLOCATIONS_LIMITEDAREAKEYS, deleted WHERE  W6COWLOCATIONS_LIMITEDAREAKEYS.W6Key = deleted.W6Key