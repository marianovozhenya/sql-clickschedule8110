﻿CREATE TABLE [dbo].[W6ENGINEER_TYPES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [TravelCost]         FLOAT (53)    NULL,
    [WorkingTimeCost]    FLOAT (53)    NULL,
    [OverTimeCost]       FLOAT (53)    NULL,
    [BaseCostPerTask]    FLOAT (53)    NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX55_2]
    ON [dbo].[W6ENGINEER_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W655_UPDATE_CACHED on W6ENGINEER_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 55,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W655_DELETE_CACHED on W6ENGINEER_TYPES for delete as insert into W6OPERATION_LOG select 55, W6Key, 1, getdate() from deleted