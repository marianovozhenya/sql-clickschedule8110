﻿CREATE TABLE [dbo].[W6ROSTER_ACTION_REQUESTS] (
    [W6Key]             INT            NOT NULL,
    [Revision]          INT            NOT NULL,
    [CreatedBy]         NVARCHAR (128) NOT NULL,
    [TimeCreated]       DATETIME       NOT NULL,
    [CreatingProcess]   INT            NOT NULL,
    [ModifiedBy]        NVARCHAR (128) NOT NULL,
    [TimeModified]      DATETIME       NOT NULL,
    [ModifyingProcess]  INT            NOT NULL,
    [RequestType]       NVARCHAR (64)  NULL,
    [OpenDate]          DATETIME       NULL,
    [CloseDate]         DATETIME       NULL,
    [InitiatorLoginID]  NVARCHAR (128) NULL,
    [SourceShift]       INT            NULL,
    [SourceShiftStart]  DATETIME       NULL,
    [SourceShiftFinish] DATETIME       NULL,
    [SourceResource]    INT            NULL,
    [TargetShift]       INT            NULL,
    [TargetShiftStart]  DATETIME       NULL,
    [TargetShiftFinish] DATETIME       NULL,
    [TargetResource]    INT            NULL,
    [SwapRequestStatus] INT            NULL,
    [CommentText]       NVARCHAR (MAX) NULL,
    [Invalidated]       INT            NULL,
    [InvalidatedReason] NVARCHAR (256) NULL,
    CONSTRAINT [W6PK_30000103] PRIMARY KEY NONCLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000103_10_8_] FOREIGN KEY ([SourceResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK30000103_14_8_] FOREIGN KEY ([TargetResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK30000103_15_10000102_] FOREIGN KEY ([SwapRequestStatus]) REFERENCES [dbo].[W6SHIFTS_SWAP_REQUEST_STATUSES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000103_12_14]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([TargetShiftStart] ASC, [TargetResource] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000103_14_8]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([TargetResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000103_15_10000102]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([SwapRequestStatus] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000103_8_10]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([SourceShiftStart] ASC, [SourceResource] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000103_10_8]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([SourceResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000103_4_0]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([OpenDate] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000103_4_14]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([OpenDate] ASC, [TargetResource] ASC) WITH (FILLFACTOR = 20);


GO
CREATE CLUSTERED INDEX [W6IX30000103_4_10]
    ON [dbo].[W6ROSTER_ACTION_REQUESTS]([OpenDate] ASC, [SourceResource] ASC);

