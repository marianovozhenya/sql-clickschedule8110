﻿CREATE TABLE [dbo].[W6PLANS] (
    [W6Key]                  INT            NOT NULL,
    [Revision]               INT            NOT NULL,
    [CreatedBy]              NVARCHAR (128) NOT NULL,
    [TimeCreated]            DATETIME       NOT NULL,
    [CreatingProcess]        INT            NOT NULL,
    [ModifiedBy]             NVARCHAR (128) NOT NULL,
    [TimeModified]           DATETIME       NOT NULL,
    [ModifyingProcess]       INT            NOT NULL,
    [Name]                   NVARCHAR (64)  NULL,
    [StartTime]              DATETIME       NULL,
    [FinishTime]             DATETIME       NULL,
    [TimeResolution]         NVARCHAR (64)  NULL,
    [Draft]                  INT            NULL,
    [Description]            NVARCHAR (512) NULL,
    [Status]                 INT            NULL,
    [ParentPlan]             INT            NULL,
    [Domain]                 NVARCHAR (MAX) NULL,
    [HistoryStartTime]       DATETIME       NULL,
    [HistoryFinishTime]      DATETIME       NULL,
    [SourceForecast]         INT            NULL,
    [NavigationTree]         NVARCHAR (64)  NULL,
    [DataSource]             NVARCHAR (64)  NULL,
    [PlanType]               INT            NULL,
    [PreviousRoster]         INT            NULL,
    [SourceForecastScenario] INT            NULL,
    [ExportedBy]             NVARCHAR (256) NULL,
    [ExportDate]             DATETIME       NULL,
    [ExportInProgress]       INT            NULL,
    CONSTRAINT [W6PK_100] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK100_10_100_] FOREIGN KEY ([ParentPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK100_14_100_] FOREIGN KEY ([SourceForecast]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK100_19_100_] FOREIGN KEY ([PreviousRoster]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK100_20_101_] FOREIGN KEY ([SourceForecastScenario]) REFERENCES [dbo].[W6PLAN_SCENARIOS] ([W6Key]),
    CONSTRAINT [W6FK100_9_62_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6PLAN_STATUSES] ([W6Key])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX100_3]
    ON [dbo].[W6PLANS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK100_20_101]
    ON [dbo].[W6PLANS]([SourceForecastScenario] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK100_19_100]
    ON [dbo].[W6PLANS]([PreviousRoster] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK100_14_100]
    ON [dbo].[W6PLANS]([SourceForecast] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK100_10_100]
    ON [dbo].[W6PLANS]([ParentPlan] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK100_9_62]
    ON [dbo].[W6PLANS]([Status] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_100 ON W6PLANS FOR DELETE AS  DELETE W6PLAN_MEASURES FROM W6PLAN_MEASURES, deleted WHERE  W6PLAN_MEASURES.W6Key = deleted.W6Key