﻿CREATE TABLE [dbo].[W6PLAN_SCENARIOS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [RelatedPlan]      INT            NULL,
    [Name]             NVARCHAR (64)  NULL,
    [Status]           INT            NULL,
    [Description]      NVARCHAR (512) NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK101_3_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK101_3_100]
    ON [dbo].[W6PLAN_SCENARIOS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);

