﻿CREATE TABLE [dbo].[W6CAPACITYLIMITSDYNAMIC] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [District]           INT           NULL,
    [TaskType]           INT           NULL,
    [TimeResolution]     NVARCHAR (64) NULL,
    [EntryDate]          DATETIME      NULL,
    [CapacityThreshold]  FLOAT (53)    NULL,
    [ActualCapacityUsed] INT           NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_71] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK71_3_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK71_4_56_] FOREIGN KEY ([TaskType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_EntryDate]
    ON [dbo].[W6CAPACITYLIMITSDYNAMIC]([EntryDate] ASC) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK71_4_56]
    ON [dbo].[W6CAPACITYLIMITSDYNAMIC]([TaskType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK71_3_53]
    ON [dbo].[W6CAPACITYLIMITSDYNAMIC]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX71_2]
    ON [dbo].[W6CAPACITYLIMITSDYNAMIC]([Name] ASC) WITH (FILLFACTOR = 20);

