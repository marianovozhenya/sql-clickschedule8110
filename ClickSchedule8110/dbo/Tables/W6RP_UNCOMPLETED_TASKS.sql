﻿CREATE TABLE [dbo].[W6RP_UNCOMPLETED_TASKS] (
    [W6InstanceID]                INT            NOT NULL,
    [W6EntryID]                   INT            NOT NULL,
    [Time_Resolution_Start]       DATETIME       NULL,
    [Time_Resolution_Finish]      DATETIME       NULL,
    [Region_Key]                  INT            NULL,
    [Region_Name]                 NVARCHAR (256) NULL,
    [District_Key]                INT            NULL,
    [District_Name]               NVARCHAR (256) NULL,
    [Resource_Contractor]         INT            NULL,
    [Resource_CrewForExternalUse] INT            NULL,
    [Resource_ID]                 NVARCHAR (256) NULL,
    [Resource_Internal]           INT            NULL,
    [Resource_Key]                INT            NULL,
    [Resource_Name]               NVARCHAR (256) NULL,
    [Assignment_Key]              INT            NULL,
    [Assignment_Task]             INT            NULL,
    [Assignment_Task_CallID]      NVARCHAR (256) NULL,
    [Task_CallID]                 NVARCHAR (256) NULL,
    [Task_Customer]               NVARCHAR (256) NULL,
    [Task_Duration]               INT            NULL,
    [Task_Key]                    INT            NULL,
    [Task_Number]                 INT            NULL,
    [Task_Priority]               INT            NULL,
    [Task_Status]                 INT            NULL,
    [Task_Status_Name]            NVARCHAR (256) NULL,
    [Task_TaskType]               INT            NULL,
    [Task_TaskType_Name]          NVARCHAR (256) NULL,
    CONSTRAINT [PKRP_UNCOMPLETED_TASKS] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);

