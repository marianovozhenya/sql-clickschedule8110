﻿CREATE TABLE [dbo].[W6CONTRACTS] (
    [W6Key]             INT            NOT NULL,
    [Revision]          INT            NOT NULL,
    [CreatedBy]         NVARCHAR (128) NOT NULL,
    [TimeCreated]       DATETIME       NOT NULL,
    [CreatingProcess]   INT            NOT NULL,
    [ModifiedBy]        NVARCHAR (128) NOT NULL,
    [TimeModified]      DATETIME       NOT NULL,
    [ModifyingProcess]  INT            NOT NULL,
    [Name]              NVARCHAR (64)  NULL,
    [AllocatedResource] INT            NULL,
    [Region]            INT            NULL,
    [District]          INT            NULL,
    [Team]              INT            NULL,
    CONSTRAINT [W6PK_112] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK112_4_8_] FOREIGN KEY ([AllocatedResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK112_5_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK112_6_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK112_7_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK112_7_259]
    ON [dbo].[W6CONTRACTS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK112_6_53]
    ON [dbo].[W6CONTRACTS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK112_5_50]
    ON [dbo].[W6CONTRACTS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK112_4_8]
    ON [dbo].[W6CONTRACTS]([AllocatedResource] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_112 ON W6CONTRACTS FOR DELETE AS  DELETE W6CONTRACTS_TIME_PHASED_DATA FROM W6CONTRACTS_TIME_PHASED_DATA, deleted WHERE  W6CONTRACTS_TIME_PHASED_DATA.W6Key = deleted.W6Key