﻿CREATE TABLE [dbo].[W6APPSTATE] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_20000001] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX20000001_2]
    ON [dbo].[W6APPSTATE]([Name] ASC) WITH (FILLFACTOR = 20);

