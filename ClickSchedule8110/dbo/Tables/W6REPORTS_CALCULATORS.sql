﻿CREATE TABLE [dbo].[W6REPORTS_CALCULATORS] (
    [W6Key]       INT             NOT NULL,
    [W6SubKey_1]  INT             NOT NULL,
    [Name]        NVARCHAR (64)   NULL,
    [Description] NVARCHAR (256)  NULL,
    [ProgID]      NVARCHAR (256)  NULL,
    [Enabled]     INT             NULL,
    [GroupBody]   NVARCHAR (4000) NULL,
    [Parameters]  NVARCHAR (MAX)  NULL,
    CONSTRAINT [W6PK_70003] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

