﻿CREATE TABLE [dbo].[W6REGIONS] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    [OrganizationParent] INT           NULL,
    [UsedInMobile]       INT           NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK50_3_88_] FOREIGN KEY ([OrganizationParent]) REFERENCES [dbo].[W6ORGANIZATION] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50_3_88]
    ON [dbo].[W6REGIONS]([OrganizationParent] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX3_3U_IMR]
    ON [dbo].[W6REGIONS]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX8_3_5U_IMR]
    ON [dbo].[W6REGIONS]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [W6IX50_B]
    ON [dbo].[W6REGIONS]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [W6IX50_2]
    ON [dbo].[W6REGIONS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W650_UPDATE_CACHED on W6REGIONS for insert,update as if update(revision) insert into W6OPERATION_LOG select 50,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W650_DELETE_CACHED on W6REGIONS for delete as insert into W6OPERATION_LOG select 50, W6Key, 1, getdate() from deleted