﻿CREATE TABLE [dbo].[W6PERFCOUNTERS] (
    [CounterKey]      INT           NOT NULL,
    [SymbolName]      VARCHAR (64)  NULL,
    [Name]            VARCHAR (64)  NULL,
    [Help]            VARCHAR (255) NULL,
    [ParentObjectKey] INT           NULL,
    PRIMARY KEY CLUSTERED ([CounterKey] ASC)
);

