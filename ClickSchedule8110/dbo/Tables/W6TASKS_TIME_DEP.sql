﻿CREATE TABLE [dbo].[W6TASKS_TIME_DEP] (
    [W6Key]            INT NOT NULL,
    [W6SubKey_1]       INT NOT NULL,
    [TaskKey]          INT NULL,
    [UpperBound]       INT NULL,
    [LowerBound]       INT NULL,
    [RelationType]     INT NULL,
    [RelationOperator] INT NULL,
    [Critical]         INT NULL,
    CONSTRAINT [W6PK_20007] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK20007_0_2_] FOREIGN KEY ([TaskKey]) REFERENCES [dbo].[W6TASKS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK20007_0_2]
    ON [dbo].[W6TASKS_TIME_DEP]([TaskKey] ASC) WITH (FILLFACTOR = 20);

