﻿CREATE TABLE [dbo].[W6CALENDAR_PHASED_INTERVALS] (
    [W6Key]           INT      NOT NULL,
    [SecondsToStart]  INT      NOT NULL,
    [SecondsToFinish] INT      NOT NULL,
    [StartDate]       DATETIME NOT NULL,
    [FinishDate]      DATETIME NOT NULL,
    [Status]          INT      NOT NULL,
    CONSTRAINT [W6PK_30007] PRIMARY KEY CLUSTERED ([W6Key] ASC, [SecondsToStart] ASC, [SecondsToFinish] ASC, [StartDate] ASC, [FinishDate] ASC, [Status] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30007_3_K]
    ON [dbo].[W6CALENDAR_PHASED_INTERVALS]([FinishDate] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);

