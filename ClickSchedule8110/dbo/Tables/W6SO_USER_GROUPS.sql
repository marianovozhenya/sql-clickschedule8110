﻿CREATE TABLE [dbo].[W6SO_USER_GROUPS] (
    [W6Key]                        INT             NOT NULL,
    [Revision]                     INT             NOT NULL,
    [CreatedBy]                    NVARCHAR (128)  NOT NULL,
    [TimeCreated]                  DATETIME        NOT NULL,
    [CreatingProcess]              INT             NOT NULL,
    [ModifiedBy]                   NVARCHAR (128)  NOT NULL,
    [TimeModified]                 DATETIME        NOT NULL,
    [ModifyingProcess]             INT             NOT NULL,
    [Name]                         NVARCHAR (64)   NULL,
    [ClickAnalyzeSetting]          INT             NULL,
    [CSWebClientSetting]           INT             NULL,
    [ClickRosterSetting]           INT             NULL,
    [ClickMobileClassicSetting]    INT             NULL,
    [ClickMobileTouchSetting]      INT             NULL,
    [Description]                  NVARCHAR (1024) NULL,
    [ClickMobileFoundationSetting] INT             NULL,
    [ControlCenterSetting]         INT             NULL,
    CONSTRAINT [W6PK_30000203] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000203_11_17_] FOREIGN KEY ([ClickMobileFoundationSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK30000203_12_17_] FOREIGN KEY ([ControlCenterSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK30000203_4_17_] FOREIGN KEY ([ClickAnalyzeSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK30000203_5_17_] FOREIGN KEY ([CSWebClientSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK30000203_6_17_] FOREIGN KEY ([ClickRosterSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK30000203_7_17_] FOREIGN KEY ([ClickMobileClassicSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK30000203_8_17_] FOREIGN KEY ([ClickMobileTouchSetting]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000203_12_17]
    ON [dbo].[W6SO_USER_GROUPS]([ControlCenterSetting] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000203_11_17]
    ON [dbo].[W6SO_USER_GROUPS]([ClickMobileFoundationSetting] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX30000203_3]
    ON [dbo].[W6SO_USER_GROUPS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000203_6_17]
    ON [dbo].[W6SO_USER_GROUPS]([ClickRosterSetting] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000203_8_17]
    ON [dbo].[W6SO_USER_GROUPS]([ClickMobileTouchSetting] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000203_7_17]
    ON [dbo].[W6SO_USER_GROUPS]([ClickMobileClassicSetting] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000203_4_17]
    ON [dbo].[W6SO_USER_GROUPS]([ClickAnalyzeSetting] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000203_5_17]
    ON [dbo].[W6SO_USER_GROUPS]([CSWebClientSetting] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W630000203_UPDATE_CACHED on W6SO_USER_GROUPS for insert,update as if update(revision) insert into W6OPERATION_LOG select 30000203,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W630000203_DELETE_CACHED on W6SO_USER_GROUPS for delete as insert into W6OPERATION_LOG select 30000203, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_30000203 ON W6SO_USER_GROUPS FOR DELETE AS  DELETE W6SO_USER_GROUPS_CRUDS FROM W6SO_USER_GROUPS_CRUDS, deleted WHERE  W6SO_USER_GROUPS_CRUDS.W6Key = deleted.W6Key