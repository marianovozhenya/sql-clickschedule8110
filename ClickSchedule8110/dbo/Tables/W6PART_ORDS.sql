﻿CREATE TABLE [dbo].[W6PART_ORDS] (
    [W6Key]               INT            NOT NULL,
    [Revision]            INT            NOT NULL,
    [CreatedBy]           NVARCHAR (128) NOT NULL,
    [TimeCreated]         DATETIME       NOT NULL,
    [CreatingProcess]     INT            NOT NULL,
    [ModifiedBy]          NVARCHAR (128) NOT NULL,
    [TimeModified]        DATETIME       NOT NULL,
    [ModifyingProcess]    INT            NOT NULL,
    [PartOrderID]         NVARCHAR (256) NULL,
    [PartsStock]          INT            NULL,
    [Quantity]            INT            NULL,
    [LocationSource]      INT            NULL,
    [LocationDestination] INT            NULL,
    [OrderStatus]         INT            NULL,
    [OrderDate]           DATETIME       NULL,
    [OrderBy]             INT            NULL,
    [MobileKey]           NVARCHAR (128) NULL,
    [OrderCompletionDate] DATETIME       NULL,
    CONSTRAINT [W6PK_30000015] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000015_10_8_] FOREIGN KEY ([OrderBy]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK30000015_4_30000000_] FOREIGN KEY ([PartsStock]) REFERENCES [dbo].[W6PARTS_STOCKS] ([W6Key]),
    CONSTRAINT [W6FK30000015_6_30000014_] FOREIGN KEY ([LocationSource]) REFERENCES [dbo].[W6PART_LOCATIONS] ([W6Key]),
    CONSTRAINT [W6FK30000015_7_30000014_] FOREIGN KEY ([LocationDestination]) REFERENCES [dbo].[W6PART_LOCATIONS] ([W6Key]),
    CONSTRAINT [W6FK30000015_8_10000014_] FOREIGN KEY ([OrderStatus]) REFERENCES [dbo].[W6PART_ORD_STATUSES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000015_4_30000000]
    ON [dbo].[W6PART_ORDS]([PartsStock] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX30000015_3]
    ON [dbo].[W6PART_ORDS]([PartOrderID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000015_8_10000014]
    ON [dbo].[W6PART_ORDS]([OrderStatus] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000015_12_0]
    ON [dbo].[W6PART_ORDS]([OrderCompletionDate] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000015_10_8]
    ON [dbo].[W6PART_ORDS]([OrderBy] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000015_11]
    ON [dbo].[W6PART_ORDS]([MobileKey] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000015_6_30000014]
    ON [dbo].[W6PART_ORDS]([LocationSource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000015_7_30000014]
    ON [dbo].[W6PART_ORDS]([LocationDestination] ASC) WITH (FILLFACTOR = 50);

