﻿CREATE TABLE [dbo].[W6PLAN_COMMENTS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [RelatedPlan]      INT            NULL,
    [PlanScenario]     INT            NULL,
    [Name]             NVARCHAR (64)  NULL,
    [Description]      NVARCHAR (MAX) NULL,
    CONSTRAINT [W6PK_106] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK106_3_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK106_4_101_] FOREIGN KEY ([PlanScenario]) REFERENCES [dbo].[W6PLAN_SCENARIOS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK106_4_101]
    ON [dbo].[W6PLAN_COMMENTS]([PlanScenario] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK106_3_100]
    ON [dbo].[W6PLAN_COMMENTS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);

