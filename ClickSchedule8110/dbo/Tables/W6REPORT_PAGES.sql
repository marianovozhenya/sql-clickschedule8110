﻿CREATE TABLE [dbo].[W6REPORT_PAGES] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [PollingID]        INT            NULL,
    [PageNumber]       INT            NULL,
    [PageContent]      NVARCHAR (MAX) NULL,
    CONSTRAINT [W6PK_30000030] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000030_3_4]
    ON [dbo].[W6REPORT_PAGES]([PollingID] ASC, [PageNumber] ASC) WITH (FILLFACTOR = 20);

