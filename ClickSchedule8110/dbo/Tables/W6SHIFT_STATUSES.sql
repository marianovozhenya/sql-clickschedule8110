﻿CREATE TABLE [dbo].[W6SHIFT_STATUSES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_260] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX260_2]
    ON [dbo].[W6SHIFT_STATUSES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W6260_UPDATE_CACHED on W6SHIFT_STATUSES for insert,update as if update(revision) insert into W6OPERATION_LOG select 260,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W6260_DELETE_CACHED on W6SHIFT_STATUSES for delete as insert into W6OPERATION_LOG select 260, W6Key, 1, getdate() from deleted