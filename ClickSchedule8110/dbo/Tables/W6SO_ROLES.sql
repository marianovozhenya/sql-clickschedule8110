﻿CREATE TABLE [dbo].[W6SO_ROLES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_10000204] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX10000204_2]
    ON [dbo].[W6SO_ROLES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W610000204_UPDATE_CACHED on W6SO_ROLES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000204,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000204_DELETE_CACHED on W6SO_ROLES for delete as insert into W6OPERATION_LOG select 10000204, W6Key, 1, getdate() from deleted