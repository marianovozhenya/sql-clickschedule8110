﻿CREATE TABLE [dbo].[W6ENG_DYNAMIC_DATA] (
    [W6Key]                   INT            NOT NULL,
    [Revision]                INT            NOT NULL,
    [CreatedBy]               NVARCHAR (128) NOT NULL,
    [TimeCreated]             DATETIME       NOT NULL,
    [CreatingProcess]         INT            NOT NULL,
    [ModifiedBy]              NVARCHAR (128) NOT NULL,
    [TimeModified]            DATETIME       NOT NULL,
    [ModifyingProcess]        INT            NOT NULL,
    [Engineer]                INT            NULL,
    [HasLocationData]         INT            NULL,
    [IsLate]                  INT            NULL,
    [CurrentInterval]         INT            NULL,
    [LogicExecutionTimeStamp] DATETIME       NULL,
    CONSTRAINT [W6PK_24] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK24_3_8_] FOREIGN KEY ([Engineer]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX24_3_0_1]
    ON [dbo].[W6ENG_DYNAMIC_DATA]([Engineer] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK24_3_8]
    ON [dbo].[W6ENG_DYNAMIC_DATA]([Engineer] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_24 ON W6ENG_DYNAMIC_DATA FOR DELETE AS  DELETE W6ENG_DYNAMIC_DATA_CONNECTED FROM W6ENG_DYNAMIC_DATA_CONNECTED, deleted WHERE  W6ENG_DYNAMIC_DATA_CONNECTED.W6Key = deleted.W6Key  DELETE W6ENG_DYNAMIC_DATA_LOCATION FROM W6ENG_DYNAMIC_DATA_LOCATION, deleted WHERE  W6ENG_DYNAMIC_DATA_LOCATION.W6Key = deleted.W6Key