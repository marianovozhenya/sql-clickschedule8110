﻿CREATE TABLE [dbo].[W6ROSTER_DEMANDS] (
    [W6Key]                     INT             NOT NULL,
    [Revision]                  INT             NOT NULL,
    [CreatedBy]                 NVARCHAR (128)  NOT NULL,
    [TimeCreated]               DATETIME        NOT NULL,
    [CreatingProcess]           INT             NOT NULL,
    [ModifiedBy]                NVARCHAR (128)  NOT NULL,
    [TimeModified]              DATETIME        NOT NULL,
    [ModifyingProcess]          INT             NOT NULL,
    [Name]                      NVARCHAR (64)   NULL,
    [StartTime]                 DATETIME        NULL,
    [FinishTime]                DATETIME        NULL,
    [NumberOfRequiredResources] INT             NULL,
    [Region]                    INT             NULL,
    [District]                  INT             NULL,
    [Description]               NVARCHAR (1024) NULL,
    [Team]                      INT             NULL,
    [RosterDemandType]          INT             NULL,
    [DemandCategory]            INT             NULL,
    [MaxReqResources]           INT             NULL,
    [OrigReqResources]          INT             NULL,
    [OrigMaxReqResources]       INT             NULL,
    [Priority]                  INT             NULL,
    [DisplayColor]              INT             NULL,
    [RosterCrewType]            INT             NULL,
    [RosterCrewID]              NVARCHAR (64)   NULL,
    CONSTRAINT [W6PK_202] PRIMARY KEY NONCLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK202_11_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key]),
    CONSTRAINT [W6FK202_12_266_] FOREIGN KEY ([RosterDemandType]) REFERENCES [dbo].[W6ROSTER_DEMAND_TYPES] ([W6Key]),
    CONSTRAINT [W6FK202_19_10000103_] FOREIGN KEY ([RosterCrewType]) REFERENCES [dbo].[W6ROSTER_CREW_TYPES] ([W6Key]),
    CONSTRAINT [W6FK202_7_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK202_8_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK202_19_10000103]
    ON [dbo].[W6ROSTER_DEMANDS]([RosterCrewType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX202_12_13_4_5]
    ON [dbo].[W6ROSTER_DEMANDS]([RosterDemandType] ASC, [DemandCategory] ASC, [StartTime] ASC, [FinishTime] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK202_12_266]
    ON [dbo].[W6ROSTER_DEMANDS]([RosterDemandType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK202_11_259]
    ON [dbo].[W6ROSTER_DEMANDS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX202_5_0]
    ON [dbo].[W6ROSTER_DEMANDS]([FinishTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK202_8_53]
    ON [dbo].[W6ROSTER_DEMANDS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK202_7_50]
    ON [dbo].[W6ROSTER_DEMANDS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE CLUSTERED INDEX [W6IX202_4_8]
    ON [dbo].[W6ROSTER_DEMANDS]([StartTime] ASC, [District] ASC);


GO
 CREATE TRIGGER W6TRIGGER_202 ON W6ROSTER_DEMANDS FOR DELETE AS  DELETE W6ROSTER_DEMANDS_REQ_SKILLS FROM W6ROSTER_DEMANDS_REQ_SKILLS, deleted WHERE  W6ROSTER_DEMANDS_REQ_SKILLS.W6Key = deleted.W6Key