﻿CREATE TABLE [dbo].[W6GENERIC_EVENT_DEFS_USERS] (
    [W6Key]      INT            NOT NULL,
    [W6SubKey_1] INT            NOT NULL,
    [Name]       NVARCHAR (256) NULL,
    [UserType]   NVARCHAR (32)  NULL,
    CONSTRAINT [W6PK_50000163] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

