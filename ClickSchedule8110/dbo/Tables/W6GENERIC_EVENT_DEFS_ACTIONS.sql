﻿CREATE TABLE [dbo].[W6GENERIC_EVENT_DEFS_ACTIONS] (
    [W6Key]             INT             NOT NULL,
    [W6SubKey_1]        INT             NOT NULL,
    [ActionType]        NVARCHAR (128)  NULL,
    [Description]       NVARCHAR (1024) NULL,
    [SourceProperty]    NVARCHAR (256)  NULL,
    [TargetProperty]    NVARCHAR (256)  NULL,
    [ValueLong]         INT             NULL,
    [ValueBoolean]      INT             NULL,
    [ValueString]       NVARCHAR (MAX)  NULL,
    [ValueDate]         DATETIME        NULL,
    [ValueDouble]       FLOAT (53)      NULL,
    [ValueDuration]     INT             NULL,
    [ValueKey]          NVARCHAR (256)  NULL,
    [CustomData]        NVARCHAR (MAX)  NULL,
    [ValueRelativeTime] INT             NULL,
    [AuxiliaryProperty] NVARCHAR (256)  NULL,
    CONSTRAINT [W6PK_50000164] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

