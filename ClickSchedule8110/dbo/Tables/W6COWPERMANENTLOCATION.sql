﻿CREATE TABLE [dbo].[W6COWPERMANENTLOCATION] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    [Longitude]          INT           NULL,
    [Latitude]           INT           NULL,
    [Street]             NVARCHAR (64) NULL,
    [City]               NVARCHAR (64) NULL,
    [State]              NVARCHAR (64) NULL,
    [Postcode]           NVARCHAR (64) NULL,
    [Country]            NVARCHAR (64) NULL,
    CONSTRAINT [W6PK_87] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX87_2]
    ON [dbo].[W6COWPERMANENTLOCATION]([Name] ASC) WITH (FILLFACTOR = 20);

