﻿CREATE TABLE [dbo].[W6PART_TYPES] (
    [W6Key]              INT             NOT NULL,
    [Revision]           INT             NOT NULL,
    [Name]               NVARCHAR (64)   NULL,
    [Description]        NVARCHAR (1024) NULL,
    [QuantityUnit]       NVARCHAR (32)   NULL,
    [Stamp_TimeModified] DATETIME        NULL,
    CONSTRAINT [W6PK_10000002] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000002_2]
    ON [dbo].[W6PART_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);

