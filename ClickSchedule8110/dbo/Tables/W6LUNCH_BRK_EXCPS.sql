﻿CREATE TABLE [dbo].[W6LUNCH_BRK_EXCPS] (
    [W6Key]                       INT            NOT NULL,
    [Revision]                    INT            NOT NULL,
    [CreatedBy]                   NVARCHAR (128) NOT NULL,
    [TimeCreated]                 DATETIME       NOT NULL,
    [CreatingProcess]             INT            NOT NULL,
    [ModifiedBy]                  NVARCHAR (128) NOT NULL,
    [TimeModified]                DATETIME       NOT NULL,
    [ModifyingProcess]            INT            NOT NULL,
    [Engineer]                    INT            NULL,
    [StartTime]                   DATETIME       NULL,
    [IsActual]                    INT            NULL,
    [BreakDuration]               INT            NULL,
    [AssignmentIncludingBreakKey] INT            NULL,
    [ActualStartTime]             DATETIME       NULL,
    [ActualFinishTime]            DATETIME       NULL,
    [MobileKey]                   NVARCHAR (64)  NULL,
    CONSTRAINT [W6PK_30000034] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000034_3_8_] FOREIGN KEY ([Engineer]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000034_4_0]
    ON [dbo].[W6LUNCH_BRK_EXCPS]([StartTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000034_3_4]
    ON [dbo].[W6LUNCH_BRK_EXCPS]([Engineer] ASC, [StartTime] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000034_3_8]
    ON [dbo].[W6LUNCH_BRK_EXCPS]([Engineer] ASC) WITH (FILLFACTOR = 50);

