﻿CREATE TABLE [dbo].[W6APP_COMPLIESWITHSERVERVERSIONS] (
    [W6Key]         INT           NOT NULL,
    [W6SubKey_1]    INT           NOT NULL,
    [VersionString] NVARCHAR (64) NULL,
    [BuildNumber]   INT           NULL,
    [PatchNumber]   INT           NULL,
    CONSTRAINT [W6PK_50000251] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

