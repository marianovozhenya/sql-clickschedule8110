﻿CREATE TABLE [dbo].[W6SERVICECODE] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_89] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX89_2]
    ON [dbo].[W6SERVICECODE]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W689_UPDATE_CACHED on W6SERVICECODE for insert,update as if update(revision) insert into W6OPERATION_LOG select 89,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W689_DELETE_CACHED on W6SERVICECODE for delete as insert into W6OPERATION_LOG select 89, W6Key, 1, getdate() from deleted