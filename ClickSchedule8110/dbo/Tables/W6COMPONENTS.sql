﻿CREATE TABLE [dbo].[W6COMPONENTS] (
    [Component_Handle]  INT           NOT NULL,
    [Component_Prog_ID] VARCHAR (128) NOT NULL,
    [Is_Product]        INT           NULL,
    PRIMARY KEY CLUSTERED ([Component_Handle] ASC)
);

