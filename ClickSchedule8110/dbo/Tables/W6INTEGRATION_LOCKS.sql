﻿CREATE TABLE [dbo].[W6INTEGRATION_LOCKS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Name]             NVARCHAR (64)  NOT NULL,
    [TransactionID]    INT            NOT NULL,
    CONSTRAINT [W6PK_20] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX20_4]
    ON [dbo].[W6INTEGRATION_LOCKS]([TransactionID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX20_3]
    ON [dbo].[W6INTEGRATION_LOCKS]([Name] ASC) WITH (FILLFACTOR = 20);

