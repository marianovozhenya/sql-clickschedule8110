﻿CREATE TABLE [dbo].[W6AUDIT] (
    [W6Key]                     INT             NOT NULL,
    [Revision]                  INT             NOT NULL,
    [Name]                      NVARCHAR (64)   NULL,
    [TaskCallID]                NVARCHAR (64)   NULL,
    [AuditTimeStamp]            DATETIME        NULL,
    [ObjectType]                INT             NULL,
    [AuditAction]               INT             NULL,
    [TaskStatus]                NVARCHAR (64)   NULL,
    [Region]                    NVARCHAR (64)   NULL,
    [District]                  NVARCHAR (64)   NULL,
    [StartTime]                 DATETIME        NULL,
    [FinishTime]                DATETIME        NULL,
    [Engineers]                 NVARCHAR (640)  NULL,
    [TaskKey]                   INT             NULL,
    [LocationVerificationError] NVARCHAR (128)  NULL,
    [LastNotificationSent]      NVARCHAR (64)   NULL,
    [NotifyLongTerm]            INT             NULL,
    [NotifyMidTerm]             INT             NULL,
    [NotifyShortTerm]           INT             NULL,
    [NotifySMS]                 INT             NULL,
    [NotifyEmail]               INT             NULL,
    [NotifyIVR]                 INT             NULL,
    [AppointmentStart]          DATETIME        NULL,
    [DispatcherComment]         NVARCHAR (4000) NULL,
    [Comment]                   NVARCHAR (1000) NULL,
    [ReceiverLine]              NVARCHAR (55)   NULL,
    [AppointmentFinish]         DATETIME        NULL,
    [RuleViolations]            NVARCHAR (1000) NULL,
    [JeopardyState]             NVARCHAR (64)   NULL,
    [Stamp_TimeModified]        DATETIME        NULL,
    [IsRuleViolating]           INT             NULL,
    [RuleViolationText]         NVARCHAR (4000) NULL,
    [ChangeDescription]         NVARCHAR (64)   NULL,
    [TaskNumber]                INT             NULL,
    [TaskDuration]              INT             NULL,
    [FSPro_User]                NVARCHAR (100)  NULL,
    CONSTRAINT [W6PK_79] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX79_2]
    ON [dbo].[W6AUDIT]([Name] ASC) WITH (FILLFACTOR = 20);

