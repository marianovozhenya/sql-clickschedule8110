﻿CREATE TABLE [dbo].[W6JEOPARDY_STATE] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_254] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX254_2]
    ON [dbo].[W6JEOPARDY_STATE]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W6254_UPDATE_CACHED on W6JEOPARDY_STATE for insert,update as if update(revision) insert into W6OPERATION_LOG select 254,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W6254_DELETE_CACHED on W6JEOPARDY_STATE for delete as insert into W6OPERATION_LOG select 254, W6Key, 1, getdate() from deleted