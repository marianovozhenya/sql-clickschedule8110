﻿CREATE TABLE [dbo].[W6ROSTER_CORRECTIVE_ACTIONS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (64)   NULL,
    [TypeName]         NVARCHAR (64)   NULL,
    [ProgID]           NVARCHAR (256)  NULL,
    [UIProgID]         NVARCHAR (256)  NULL,
    [Body]             NVARCHAR (MAX)  NULL,
    [ExecutionText]    NVARCHAR (2000) NULL,
    CONSTRAINT [W6PK_30000104] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);

