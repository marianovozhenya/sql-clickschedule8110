﻿CREATE TABLE [dbo].[W6IM_INCOMING_LOG] (
    [SERVERNAME]            NVARCHAR (128)  NOT NULL,
    [USERNAME]              NVARCHAR (128)  NOT NULL,
    [LOGTIME]               DATETIME        NULL,
    [MESSAGEIN]             VARCHAR (MAX)   NULL,
    [MESSAGEOUT]            VARCHAR (MAX)   NULL,
    [EntryID]               INT             NULL,
    [PageID]                INT             NULL,
    [AgentID]               INT             NULL,
    [MessageIn_Translated]  NVARCHAR (3584) NULL,
    [MessageOut_Translated] NVARCHAR (3584) NULL,
    [MessageIn_Time]        DATETIME        NULL,
    [MessageOut_Time]       DATETIME        NULL,
    [Translator]            NVARCHAR (128)  NULL,
    [PK]                    INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    PRIMARY KEY CLUSTERED ([PK] ASC)
);

