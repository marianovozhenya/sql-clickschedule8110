﻿CREATE TABLE [dbo].[W6MESSAGES] (
    [MsgKey]      INT            NOT NULL,
    [MsgType]     NVARCHAR (30)  NULL,
    [SubType]     NVARCHAR (30)  NULL,
    [Source]      NVARCHAR (128) NULL,
    [Destination] NVARCHAR (128) NULL,
    [DateCreated] DATETIME       NOT NULL,
    [Importance]  INT            NOT NULL,
    [UserField1]  NVARCHAR (128) NULL,
    [UserField2]  NVARCHAR (128) NULL,
    [UserField3]  NVARCHAR (128) NULL,
    [UserField4]  NVARCHAR (128) NULL,
    PRIMARY KEY CLUSTERED ([MsgKey] ASC),
    UNIQUE NONCLUSTERED ([MsgKey] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX_MESSAGE_DEST_TYPE]
    ON [dbo].[W6MESSAGES]([Destination] ASC, [MsgType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_MESSAGE_DEST_SRC]
    ON [dbo].[W6MESSAGES]([Destination] ASC, [Source] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_MESSAGE_DATECREATED]
    ON [dbo].[W6MESSAGES]([DateCreated] ASC);

