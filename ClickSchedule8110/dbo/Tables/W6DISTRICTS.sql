﻿CREATE TABLE [dbo].[W6DISTRICTS] (
    [W6Key]                  INT           NOT NULL,
    [Revision]               INT           NOT NULL,
    [Name]                   NVARCHAR (64) NULL,
    [RegionParent]           INT           NULL,
    [TimeZone]               NVARCHAR (64) NULL,
    [SameSiteTimeRadius]     INT           NULL,
    [SameSiteDistanceRadius] INT           NULL,
    [City]                   NVARCHAR (64) NULL,
    [Postcode]               NVARCHAR (64) NULL,
    [Street]                 NVARCHAR (64) NULL,
    [State]                  NVARCHAR (64) NULL,
    [Longitude]              INT           NULL,
    [Latitude]               INT           NULL,
    [GISDataSource]          INT           NULL,
    [CountryID]              INT           NULL,
    [AverageTravelTime]      INT           NULL,
    [Stamp_TimeModified]     DATETIME      NULL,
    [UsedInMobile]           INT           NULL,
    [LunchBreakDurations]    INT           NULL,
    CONSTRAINT [W6PK_53] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK53_14_66_] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK53_3_50_] FOREIGN KEY ([RegionParent]) REFERENCES [dbo].[W6REGIONS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX53_2]
    ON [dbo].[W6DISTRICTS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK53_14_66]
    ON [dbo].[W6DISTRICTS]([CountryID] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK53_3_50]
    ON [dbo].[W6DISTRICTS]([RegionParent] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W653_UPDATE_CACHED on W6DISTRICTS for insert,update as if update(revision) insert into W6OPERATION_LOG select 53,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W653_DELETE_CACHED on W6DISTRICTS for delete as insert into W6OPERATION_LOG select 53, W6Key, 1, getdate() from deleted
GO
 CREATE TRIGGER W6TRIGGER_53 ON W6DISTRICTS FOR DELETE AS  DELETE W6DISTRICTS_ADJACENTDISTRICTS FROM W6DISTRICTS_ADJACENTDISTRICTS, deleted WHERE  W6DISTRICTS_ADJACENTDISTRICTS.W6Key = deleted.W6Key