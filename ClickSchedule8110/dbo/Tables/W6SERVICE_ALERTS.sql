﻿CREATE TABLE [dbo].[W6SERVICE_ALERTS] (
    [W6Key]                      INT             NOT NULL,
    [Revision]                   INT             NOT NULL,
    [CreatedBy]                  NVARCHAR (128)  NOT NULL,
    [TimeCreated]                DATETIME        NOT NULL,
    [CreatingProcess]            INT             NOT NULL,
    [ModifiedBy]                 NVARCHAR (128)  NOT NULL,
    [TimeModified]               DATETIME        NOT NULL,
    [ModifyingProcess]           INT             NOT NULL,
    [UserNames]                  NVARCHAR (4000) NULL,
    [CustomDestination]          NVARCHAR (1024) NULL,
    [AlertSource]                NVARCHAR (1024) NULL,
    [RelatedTask]                INT             NULL,
    [RelatedEngineer]            INT             NULL,
    [RelatedAssignment]          INT             NULL,
    [ServiceAlertStatus]         INT             NULL,
    [ServiceAlertType]           INT             NULL,
    [CreationTime]               DATETIME        NULL,
    [ExpectedDueDate]            DATETIME        NULL,
    [Importance]                 INT             NULL,
    [Description]                NVARCHAR (MAX)  NULL,
    [Latitude]                   INT             NULL,
    [Longitude]                  INT             NULL,
    [GISDataSource]              INT             NULL,
    [CountryID]                  INT             NULL,
    [RelatedOperationalActivity] INT             NULL,
    [RelatedRosterShift]         INT             NULL,
    [RelatedRosterDemand]        INT             NULL,
    [SnoozeTime]                 DATETIME        NULL,
    [Region]                     INT             NULL,
    [District]                   INT             NULL,
    [Team]                       INT             NULL,
    [RelatedVacationCalendar]    INT             NULL,
    [InnerType]                  INT             NULL,
    [MobileKey]                  NVARCHAR (128)  NULL,
    [Urgency]                    INT             NULL,
    [ReferencedTask]             INT             NULL,
    [ReferencedAssignment]       INT             NULL,
    [Color]                      NVARCHAR (64)   NULL,
    [Priority]                   INT             NULL,
    [Title]                      NVARCHAR (64)   NULL,
    [ImageName]                  NVARCHAR (64)   NULL,
    [ActionKey]                  INT             NULL,
    [ActionMobileKey]            NVARCHAR (64)   NULL,
    [ActionObjectType]           INT             NULL,
    [ActionScript]               NVARCHAR (128)  NULL,
    [ActionSettings]             NVARCHAR (MAX)  NULL,
    [ActionType]                 INT             NULL,
    [ActionURL]                  NVARCHAR (256)  NULL,
    [ActionViewName]             NVARCHAR (64)   NULL,
    [EnableActionButton]         INT             NULL,
    [EnableDismissButton]        INT             NULL,
    [EnableSnoozeButton]         INT             NULL,
    [OpenUI]                     INT             NULL,
    [ActionText]                 NVARCHAR (64)   NULL,
    [WidgetScript]               NVARCHAR (128)  NULL,
    [WidgetSettings]             NVARCHAR (MAX)  NULL,
    [ButlerStatus]               INT             NULL,
    CONSTRAINT [W6PK_28] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK28_10_263_] FOREIGN KEY ([ServiceAlertStatus]) REFERENCES [dbo].[W6SERVICE_ALERT_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK28_11_264_] FOREIGN KEY ([ServiceAlertType]) REFERENCES [dbo].[W6SERVICE_ALERT_TYPES] ([W6Key]),
    CONSTRAINT [W6FK28_19_66_] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK28_20_208_] FOREIGN KEY ([RelatedOperationalActivity]) REFERENCES [dbo].[W6OPERATIONAL_ACTIVITIES] ([W6Key]),
    CONSTRAINT [W6FK28_24_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK28_25_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK28_26_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key]),
    CONSTRAINT [W6FK28_8_8_] FOREIGN KEY ([RelatedEngineer]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX28_32]
    ON [dbo].[W6SERVICE_ALERTS]([ReferencedAssignment] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX28_31]
    ON [dbo].[W6SERVICE_ALERTS]([ReferencedTask] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX28_27]
    ON [dbo].[W6SERVICE_ALERTS]([RelatedVacationCalendar] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_26_259]
    ON [dbo].[W6SERVICE_ALERTS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_25_53]
    ON [dbo].[W6SERVICE_ALERTS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_24_50]
    ON [dbo].[W6SERVICE_ALERTS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX28_22]
    ON [dbo].[W6SERVICE_ALERTS]([RelatedRosterDemand] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX28_21]
    ON [dbo].[W6SERVICE_ALERTS]([RelatedRosterShift] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX28_13_0]
    ON [dbo].[W6SERVICE_ALERTS]([ExpectedDueDate] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_20_208]
    ON [dbo].[W6SERVICE_ALERTS]([RelatedOperationalActivity] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_19_66]
    ON [dbo].[W6SERVICE_ALERTS]([CountryID] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_11_264]
    ON [dbo].[W6SERVICE_ALERTS]([ServiceAlertType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_10_263]
    ON [dbo].[W6SERVICE_ALERTS]([ServiceAlertStatus] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK28_8_8]
    ON [dbo].[W6SERVICE_ALERTS]([RelatedEngineer] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_28 ON W6SERVICE_ALERTS FOR DELETE AS  DELETE W6SERVICE_ALERTS_DESTINATIONS FROM W6SERVICE_ALERTS_DESTINATIONS, deleted WHERE  W6SERVICE_ALERTS_DESTINATIONS.W6Key = deleted.W6Key