﻿CREATE TABLE [dbo].[W6ABSENCE_TYPES] (
    [W6Key]               INT           NOT NULL,
    [Revision]            INT           NOT NULL,
    [Name]                NVARCHAR (64) NULL,
    [NonAvailabilityType] INT           NULL,
    [Stamp_TimeModified]  DATETIME      NULL,
    CONSTRAINT [W6PK_10000032] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10000032_3_252_] FOREIGN KEY ([NonAvailabilityType]) REFERENCES [dbo].[W6NONAVAILABILITY_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000032_3_252]
    ON [dbo].[W6ABSENCE_TYPES]([NonAvailabilityType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX10000032_2]
    ON [dbo].[W6ABSENCE_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W610000032_UPDATE_CACHED on W6ABSENCE_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000032,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000032_DELETE_CACHED on W6ABSENCE_TYPES for delete as insert into W6OPERATION_LOG select 10000032, W6Key, 1, getdate() from deleted