﻿CREATE TABLE [dbo].[W6ROSTER_DEMAND_PATTERNS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (64)   NULL,
    [Description]      NVARCHAR (1024) NULL,
    [Region]           INT             NULL,
    [District]         INT             NULL,
    [Team]             INT             NULL,
    [Active]           INT             NULL,
    [PatternLength]    INT             NULL,
    CONSTRAINT [W6PK_30000101] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000101_5_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK30000101_6_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK30000101_7_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000101_7_259]
    ON [dbo].[W6ROSTER_DEMAND_PATTERNS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000101_5_50]
    ON [dbo].[W6ROSTER_DEMAND_PATTERNS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000101_6_53]
    ON [dbo].[W6ROSTER_DEMAND_PATTERNS]([District] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_30000101 ON W6ROSTER_DEMAND_PATTERNS FOR DELETE AS  DELETE W6ROSTER_DMND_PTRNS_TEMPLATES FROM W6ROSTER_DMND_PTRNS_TEMPLATES, deleted WHERE  W6ROSTER_DMND_PTRNS_TEMPLATES.W6Key = deleted.W6Key