﻿CREATE TABLE [dbo].[W6PARTS_STOCKS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (256)  NULL,
    [Active]           INT             NULL,
    [MobileKey]        NVARCHAR (128)  NULL,
    [Description]      NVARCHAR (1024) NULL,
    CONSTRAINT [W6PK_30000000] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000000_5]
    ON [dbo].[W6PARTS_STOCKS]([MobileKey] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000000_2__4_0]
    ON [dbo].[W6PARTS_STOCKS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000000_3]
    ON [dbo].[W6PARTS_STOCKS]([Name] ASC) WITH (FILLFACTOR = 20);

