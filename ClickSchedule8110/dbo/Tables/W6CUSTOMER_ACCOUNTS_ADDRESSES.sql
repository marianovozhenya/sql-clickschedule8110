﻿CREATE TABLE [dbo].[W6CUSTOMER_ACCOUNTS_ADDRESSES] (
    [W6Key]      INT NOT NULL,
    [W6SubKey_1] INT NOT NULL,
    [AddressKey] INT NULL,
    CONSTRAINT [W6PK_50000032] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK50000032_0_30000007_] FOREIGN KEY ([AddressKey]) REFERENCES [dbo].[W6ADDRESSES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50000032_0_30000007]
    ON [dbo].[W6CUSTOMER_ACCOUNTS_ADDRESSES]([AddressKey] ASC) WITH (FILLFACTOR = 50);

