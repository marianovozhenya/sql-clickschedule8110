﻿CREATE TABLE [dbo].[MissingIndexes] (
    [tablename]          NVARCHAR (4000) NULL,
    [equality_columns]   NVARCHAR (4000) NULL,
    [inequality_columns] NVARCHAR (4000) NULL,
    [included_columns]   NVARCHAR (4000) NULL
);

