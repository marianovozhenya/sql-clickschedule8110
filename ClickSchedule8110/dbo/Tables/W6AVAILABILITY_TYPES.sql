﻿CREATE TABLE [dbo].[W6AVAILABILITY_TYPES] (
    [W6Key]                INT           NOT NULL,
    [Revision]             INT           NOT NULL,
    [Name]                 NVARCHAR (64) NULL,
    [ContributeToCoverage] INT           NULL,
    [OffDuty]              INT           NULL,
    [Stamp_TimeModified]   DATETIME      NULL,
    CONSTRAINT [W6PK_68] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX68_2]
    ON [dbo].[W6AVAILABILITY_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W668_UPDATE_CACHED on W6AVAILABILITY_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 68,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W668_DELETE_CACHED on W6AVAILABILITY_TYPES for delete as insert into W6OPERATION_LOG select 68, W6Key, 1, getdate() from deleted