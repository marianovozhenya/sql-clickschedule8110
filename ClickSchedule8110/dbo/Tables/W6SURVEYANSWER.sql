﻿CREATE TABLE [dbo].[W6SURVEYANSWER] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_76] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX76_2]
    ON [dbo].[W6SURVEYANSWER]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W676_UPDATE_CACHED on W6SURVEYANSWER for insert,update as if update(revision) insert into W6OPERATION_LOG select 76,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W676_DELETE_CACHED on W6SURVEYANSWER for delete as insert into W6OPERATION_LOG select 76, W6Key, 1, getdate() from deleted