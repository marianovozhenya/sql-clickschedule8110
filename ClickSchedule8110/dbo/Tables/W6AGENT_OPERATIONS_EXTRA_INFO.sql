﻿CREATE TABLE [dbo].[W6AGENT_OPERATIONS_EXTRA_INFO] (
    [W6Key]            INT             NOT NULL,
    [InformationChunk] NVARCHAR (1024) NOT NULL,
    CONSTRAINT [W6PK_210001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [InformationChunk] ASC)
);

