﻿CREATE TABLE [dbo].[W6SETTINGS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Owner]            NVARCHAR (128) NULL,
    [Category]         NVARCHAR (128) NULL,
    [SubCategory]      NVARCHAR (128) NULL,
    [Name]             NVARCHAR (128) NULL,
    [Body]             NVARCHAR (MAX) NULL,
    CONSTRAINT [W6PK_15] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX15_3_4_5_6_0_1]
    ON [dbo].[W6SETTINGS]([Owner] ASC, [Category] ASC, [SubCategory] ASC, [Name] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W615_UPDATE_CACHED on W6SETTINGS for insert,update as if update(revision) insert into W6OPERATION_LOG select 15,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W615_DELETE_CACHED on W6SETTINGS for delete as insert into W6OPERATION_LOG select 15, W6Key, 1, getdate() from deleted