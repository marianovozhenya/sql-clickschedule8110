﻿CREATE TABLE [dbo].[W6CODING_CODES] (
    [W6Key]              INT             NOT NULL,
    [Revision]           INT             NOT NULL,
    [Name]               NVARCHAR (64)   NULL,
    [Description]        NVARCHAR (1024) NULL,
    [ParentCodingGroup]  INT             NULL,
    [Stamp_TimeModified] DATETIME        NULL,
    CONSTRAINT [W6PK_10000006] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK10000006_4_10000005_] FOREIGN KEY ([ParentCodingGroup]) REFERENCES [dbo].[W6CODING_GROUPS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000006_2]
    ON [dbo].[W6CODING_CODES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK10000006_4_10000005]
    ON [dbo].[W6CODING_CODES]([ParentCodingGroup] ASC) WITH (FILLFACTOR = 50);

