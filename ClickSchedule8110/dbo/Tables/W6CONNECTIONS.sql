﻿CREATE TABLE [dbo].[W6CONNECTIONS] (
    [W6Key]              INT             NOT NULL,
    [Revision]           INT             NOT NULL,
    [CreatedBy]          NVARCHAR (128)  NOT NULL,
    [TimeCreated]        DATETIME        NOT NULL,
    [CreatingProcess]    INT             NOT NULL,
    [ModifiedBy]         NVARCHAR (128)  NOT NULL,
    [TimeModified]       DATETIME        NOT NULL,
    [ModifyingProcess]   INT             NOT NULL,
    [Name]               NVARCHAR (64)   NULL,
    [Description]        NVARCHAR (256)  NULL,
    [ConnectionType]     INT             NULL,
    [ConnectionString]   NVARCHAR (1024) NULL,
    [OriginalConnection] NVARCHAR (1024) NULL,
    [Password]           NVARCHAR (256)  NULL,
    CONSTRAINT [W6PK_11] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
create trigger W611_UPDATE_CACHED on W6CONNECTIONS for insert,update as if update(revision) insert into W6OPERATION_LOG select 11,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W611_DELETE_CACHED on W6CONNECTIONS for delete as insert into W6OPERATION_LOG select 11, W6Key, 1, getdate() from deleted